# coding: utf-8
from django.forms.models import model_to_dict
from django.shortcuts import render
from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from django.http import HttpResponse, HttpResponseServerError
from django.http import HttpResponseRedirect
from django.conf import settings
from django.core import serializers
from django.shortcuts import render_to_response
from django.shortcuts import redirect
from django.core.servers.basehttp import FileWrapper
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout as auth_logout
from django.contrib.messages.api import get_messages
from contextlib import closing
from zipfile import ZipFile, ZIP_DEFLATED
import urllib
import urllib2
import tempfile
import shutil
from urlparse import urlparse
import datetime, random, sha
from datetime import timedelta
from django.core.mail import send_mail
from django.template import RequestContext
from md5 import md5
from string import whitespace
import time
from django.utils import timezone
import subprocess
import os
import ftplib
from os import listdir
import traceback
from agglobal.models import *
from agglobal.forms import *
import json
import datetime
import logging
import sys
from dateutil.parser import parse
import ntpath
import mysite.settings

logger = logging.getLogger(__name__)


############  utility functions ###############

def make_fn( filename):
    return   ntpath.basename(filename).replace(' ','_')  

def make_ufn( email, filename):
    return   email + '_' + ntpath.basename(filename).replace(' ','_') 

def make_url( email, filename):
    return  os.path.join( settings.MEDIA_URL, email +  '_' +  ntpath.basename(filename).replace(' ','_') )

def rename_file( file1, file2):
     os.rename( os.path.join(settings.MEDIA_ROOT, make_fn(file1)), os.path.join(settings.MEDIA_ROOT,make_fn(file2)) )

############  views  ############



# view to save user type to session data and call social auth

def login_linkedin(request):

    #mydata = json.loads(str(request.body))
    #mydata = json.loads( '{ "userType": "e"}')
    #print mydata  
    print "entering login_linkedin"   

    if request.GET.get('userType'):
         userType = request.GET['userType']
    else:
         userType = 'None'
    print "user type is: " + userType

    request.session['userType'] = userType

    return  HttpResponseRedirect('/login/linkedin')


# Where the user is redirected after successful authentication
@login_required
def complete(request): 
    ### need to change username to email for linkedin created user
    print "entering linkedin complete routine" 

    userType = request.session.get('userType', 'Not found')
    user = request.user
    user.username=user.email
    user.save()

    print "user email: " + user.email
    print "first name: " + user.first_name
    print "last name: " + user.last_name
    print "user type: " + userType 

   
    response =  HttpResponseRedirect('/test/Layout.html')

    u_user = user.userprofile_set.all()
    u_inv = user.investorprofile_set.all()
   
    exptime = datetime.datetime.now() + timedelta(seconds=15) 
    if u_inv.exists():
        i_obj = InvestorProfile.objects.get( user = user )
        response.set_cookie('from','',  max_age=15, expires=exptime)
        response.set_cookie('redirect','N')    
        response.set_cookie('keyagg','successful_login')
        response.set_cookie('profile_img', i_obj.profile_img.name)
        response.set_cookie('userType','i')
        print "image file name: " +  i_obj.profile_img.name
    elif u_user.exists():
        u_obj = UserProfile.objects.get( user = user )
        response.set_cookie('from','',  max_age=15, expires=exptime)
        response.set_cookie('redirect','N')
        response.set_cookie('keyagg','successful_login')
        response.set_cookie('profile_img',u_obj.profile_img.name)
        response.set_cookie('userType','e')
        print "image file name: " + u_obj.profile_img.name
    else: 
        response.set_cookie('from','linkedin',  max_age=15, expires=exptime)
        response.set_cookie('redirect','Y')
        print "this user needs to complete detailed profile"

    #response.cookies['from']['expires'] = datetime.datetime.now() + timedelta(seconds=5)
    response.set_cookie('userType',userType)
    response.set_cookie('email',user.email)
    response.set_cookie('first_name',user.first_name)
    response.set_cookie('last_name',user.last_name)

    return response

# Since the logged in user is a normal Django user instance, we logout the user the natural Django way:
def logout(request):
    """Logs out user"""
    auth_logout(request)
    return HttpResponseRedirect('/')

def error(request):
    """Error view"""
    messages = get_messages(request)
    print messages
    return render_to_response('registration/error.html', {'messages': messages}, RequestContext(request))




def register_user_test(request):
    print "in register user loop"
    print request.method
    # if  request.method == 'POST':
    print "hello world"
    print request.POST.dict()
    print request.POST.items()
    print request.body

@csrf_exempt
def list(request):
    # Handle file upload

    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            newdoc = Document(
                docfile = request.FILES['docfile'],
                email = 'me@me.mindcraft.com',
                #filename= 'testfile.txt',
                created = datetime.datetime.now()
            )

            newdoc.filename = make_ufn(newdoc.email, newdoc.docfile.name )
            newdoc.save()
            rename_file( newdoc.docfile.name, newdoc.filename )
            # Redirect to the document list after POST
            return HttpResponseRedirect(reverse('agglobal.views.list'))
    else:
        form = DocumentForm() # A empty, unbound form

    # Load documents for the list page
    documents = Document.objects.all()

    # Render list page with the documents and the form
    return render_to_response(
        'agglobal/list.html',
        {'documents': documents, 'form': form},
        context_instance=RequestContext(request)
    )

@csrf_exempt
def file_upload(request):
    # Handle file upload

    success = True
    #print request.body
    print request.POST
    #mydata = json.loads(str(request.body))
    print "in file_upload"
    print request.POST['email']

    for filename, file in request.FILES.items():
        print request.FILES[filename].name
 
    if request.method == 'POST':

        for filename, file in request.FILES.items():
            print request.FILES[filename].name

        newdoc = Document(
           docfile = request.FILES['file'],
           email = request.POST.get('email').replace('"',''), 
           seq = request.POST.get('seq'),
           created = datetime.datetime.now() 
        )
        #newdoc.filename = newdoc.email + '_' + ntpath.basename(newdoc.docfile.name).replace(' ','_')
        newdoc.filename = make_ufn(newdoc.email, newdoc.docfile.name) 
        newdoc.save()
        print newdoc.email
        print newdoc.filename
        #os.rename(os.path.join(settings.MEDIA_ROOT, ntpath.basename(newdoc.docfile.name).replace(' ','_')), os.path.join(settings.MEDIA_ROOT, newdoc.filename) )
        rename_file(newdoc.docfile.name, newdoc.filename)
    else:
         message = " request was not post type"
         success = False
    
    return_json = {}
    if (success):
        message = "please redirect to user reigstration confirmation page" 
        return_json['file_name']= os.path.join(settings.MEDIA_URL,  newdoc.filename)
    return_json['success'] = success    
    return_json['message'] = message
    json_str = json.dumps(return_json)
    return HttpResponse(json_str, content_type='application/json')

            
@csrf_exempt
def login_user_test(request):
    logger.info("user logging to angie global website")
    error = ''
    username = password = ''
    print request.body

    print "entering user login page"

    print request.method

    success = True
        
    #mydata = json.loads(str(request.body))

    #username ='whereis@sandiego.com'
    #password ='world
    username = 'e@angelsglobal.com'
    password = '123456'

    #username = mydata['email']
    #password = mydata['password']
    print username
    print password
    usertype = ''

    user = authenticate(username=username, password=password)

    if user is not None:
        if user.is_active:
            print "user is active"
            login(request, user)
            message =   'Your have succesfully logged in.'
            u_usr = user.userprofile_set.all()
            u_inv = user.investorprofile_set.all()

            if u_inv.exists():
               usertype = 'i'
               print "this user is investor"

            if u_usr.exists():
               usertype = 'e'
               print "this user is normal user"

        else:
            print 'Your account is not active, please contact the site'
            message = 'Your account is not active, please contact the site'
            success = False
            auth_logout(request)
    else:
        message = 'Your username and/or password are incorrect.'
        print  'Your username and/or password are incorrect.'
        success = False
        auth_logout(request)


    return_json = {}
    return_json['success'] = success
    return_json['message'] = message
    if (success):
       return_json['userType'] = usertype

    json_str = json.dumps(return_json)
    print json_str
    response =  HttpResponse(json_str, content_type='application/json')
    if (success):
           response.set_cookie('keyagg','successful_login')
    return response
 
@csrf_exempt
def logout_user(request):
    auth_logout(request)
    return HttpResponseRedirect('/agglobal/home')


@csrf_exempt
def home(request):
    return render_to_response('agglobal/layout.html', {}, context_instance=RequestContext(request))


@csrf_exempt
@login_required
def events(request):
    return render_to_response('agglobal/events.html', {}, context_instance=RequestContext(request))


# TO-DO
@csrf_exempt
@login_required
def user_profile(request):
    pass


# TO-DO
@csrf_exempt
@login_required
def investor_profile(request):
    pass


# TO-DO
@csrf_exempt
@login_required
def edit_user_profile(request):
    pass


# TO-DO
@csrf_exempt
@login_required
def edit_investor_profile(request):
    pass


# TO-DO
@csrf_exempt
@login_required
def submit_bp(request):
    pass


# TO-DO
@csrf_exempt
@login_required
def create_startup_step1(request):
    pass


# TO-DO
@csrf_exempt
@login_required
def create_startup_step2(request):
    pass


# TO-DO
@csrf_exempt
@login_required
def search_startups(request):
    pass


# TO-DO
@csrf_exempt
@login_required
def search_investors(request):
    pass


# TO-DO
@csrf_exempt
def register_investor1(request):
    pass


# TO-DO
@csrf_exempt
def register_investor2(request):
    pass


# TO-DO
def register_user1(request):
    return render_to_response('agglobal/SignUpUser.html', {}, context_instance=RequestContext(request))


@csrf_exempt
def register_user(request):
    print "in register user loop"
    print request.body

    mydata = json.loads(str(request.body))
    success = True

    uemail = mydata['email'].replace('"', '')
    try:
        # create a new user in the database
        if request.user.is_authenticated():
           print "already authenticated, not creating new user"
           user = request.user
        else:
           user = User.objects.create_user(uemail, uemail, mydata['password'])
           user.is_active = True 
           user.is_staff = False
           user.is_superuser = False
           user.save()

        u_user = user.userprofile_set.all()
        if u_user.exists():
            new_user = UserProfile.objects.get( user = user )

        # build activation key
        salt = sha.new(str(random.random())).hexdigest()[:5]
        activation_key = sha.new(salt + uemail).hexdigest()
        key_expires = datetime.datetime.today() + datetime.timedelta(2)

        #sfilename = mydata['profile_img'] if 'profile_img' in mydata else ''
        #doc = Document.objects.filter(email=mydata['email'], seq = 0).order_by('-created').first()

        new_user = UserProfile(
            first_name=mydata['first_name'] if 'first_name' in mydata else '',
            last_name=mydata['last_name'] if 'last_name' in mydata else '',
            email=uemail,
            company_name=mydata['company_name'] if 'company_name' in mydata else '',
            domain=mydata['domain'] if 'domain' in mydata else '',
            introduction=mydata['introduction'] if 'introduction' in mydata else '',
            job_title=mydata['job_title'] if 'job_title' in mydata else '',
            personal_profile=mydata['personal_profile'] if 'personal_profile' in mydata else '',
            city=mydata['city'] if 'city' in mydata else '',
            country=mydata['country'] if 'country' in mydata else '',
            industry=mydata['industry'] if 'industry' in mydata else '',
            industryofconcern=mydata['industryofconcern'] if 'industryofconcern' in mydata else '',
            profession=mydata['profession'] if 'profession' in mydata else '',
            activation_key = activation_key,
            key_expires = key_expires,
            user = user,
        )
        #print os.path.join( settings.MEDIA_URL , mydata['profile_img'])
        if 'profile_img' in mydata:
            if mydata['profile_img'] <> '':
              new_user.profile_img.name =   mydata['profile_img'].replace(' ','_')  
              print new_user.profile_img.name

        new_user.save()
        # nu = User.objects.latest()

        for exp in mydata['business_experiences']:
            #json_parsed_startime = None
            #if exp['start_time']:
            #    json_parsed_startime = parse(exp['start_time'])

            #json_parsed_endtime = None
            #if exp['end_time']:
            #    json_parsed_endtime = parse(exp['end_time'])

            #sfilename = exp['attached_file'] if 'attached_file' in exp else ''
            doc = Document.objects.filter(email=uemail, seq = exp['id']).order_by('-created').first()

            new_exp = User_exp(
                company_name=exp['company_name'] if 'company_name' in exp else '',
                job_title=exp['job_title'] if 'job_title' in exp else '',
                description=exp['description'] if 'description' in exp else '',
                location=exp['location'] if 'location' in exp else '',
                #start_time=json_parsed_startime,
                #end_time=json_parsed_endtime,
                start_year=exp['start_year'] if 'start_year' in exp else '',
                start_month=exp['start_month'] if 'start_month' in exp else '',
                end_year=exp['end_year'] if 'end_year' in exp else '',
                end_month=exp['end_month'] if 'end_month' in exp else '',
                user=new_user,
            )
            #print  os.path.join( settings.MEDIA_URL ,exp['attached_file'] )
            if 'attached_file' in exp:
               if exp['attached_file'] <> '':
                 #new_exp.attached_file.name = os.path.join( settings.MEDIA_URL ,new_user.email + '_' + exp['attached_file'].replace(' ','_') )
                 new_exp.attached_file.name = make_url( new_user.email, exp['attached_file'] )
                 print new_exp.attached_file.name 

            new_exp.save()
    except:
        success = False
        print sys.exc_info()
        message = "error occurred " + str(sys.exc_info()[0])

    return_json = {}
    if (success):
       message = "please redirect to user reigstration confirmation page" 
       return_json['userType'] = 'e'
       return_json['first_name'] = new_user.first_name
       return_json['last_name'] = new_user.last_name
       return_json['profile_img'] = new_user.profile_img.name

    return_json['success'] = success
    return_json['message'] = message
    json_str = json.dumps(return_json)
    response =  HttpResponse(json_str, content_type='application/json')
    if (success):
        print uemail
        print new_user.first_name
        print new_user.last_name
        print new_user.profile_img.name
        response.set_cookie('keyagg','successful_login')
        response.set_cookie('userType','e')
        response.set_cookie('email',uemail)
        response.set_cookie('first_name',new_user.first_name)
        response.set_cookie('last_name',new_user.last_name)
        response.set_cookie('profile_img',new_user.profile_img.name)

    return response
 
    
@csrf_exempt
def register_investor(request):
    print "in register investor loop"
    print request.POST.dict()
    print request.POST.items()
    print request.body

    mydata = json.loads(str(request.body))
    #mydata = json.loads( '{ "last_name": "a", "first_name": "a", "country": "??", "city": "a", "interested_industry": "GAMBLING", "company_name": "a", "job_title":"CEO", "personal_profile":"Guided world leading company", "introduction": "a", "business_plans": [{ "investment_field": "social media", "investment_location": "San Francisco", "investment_budget": "million dollars", "investment_status": "beginning" }, { "investment_field": "biogenetics", "investment_budget": "500K", "investment_location": "Los Angeles", "investment_status": "established" }]}')

    print mydata
    if 'email' in mydata:
        print mydata['email']
        iemail =  mydata['email'].replace('"','')

    success = True
    try:
        # create a new user in the database
        if request.user.is_authenticated():
           print "already authenticated, not creating new user"
           user = request.user
        else:
           user = User.objects.create_user(iemail,iemail, mydata['password'])
           user.is_active = True 
           user.is_staff = False
           user.is_superuser = False
           user.save()


        u_inv = user.investorprofile_set.all()
        if u_inv.exists():
            new_investor = InvestorProfile.objects.get( user = user ) 

        # build activation keya

        salt = sha.new(str(random.random())).hexdigest()[:5]
        activation_key = sha.new(salt + iemail).hexdigest()
        key_expires = datetime.datetime.today() + datetime.timedelta(2)

        #sfilename = mydata['profile_img'] if 'profile_img' in mydata else ''
        doc = Document.objects.filter(email=iemail, seq = 0).order_by('-created').first()

        new_investor = InvestorProfile(
            first_name=mydata['first_name'] if 'first_name' in mydata else '',
            last_name=mydata['last_name'] if 'last_name' in mydata else '',
            email = iemail, 
            company_name=mydata['company_name'] if 'company_name' in mydata else '',
            introduction=mydata['introduction'] if 'introduction' in mydata else '',
            job_title=mydata['job_title'] if 'job_title' in mydata else '',
            personal_profile=mydata['personal_profile'] if 'personal_profile' in mydata else '',
            city=mydata['city'] if 'city' in mydata else '',
            country=mydata['country'] if 'country' in mydata else '',
            profession=mydata['profession'] if 'profession' in mydata else '',
            industryofconcern=mydata['industryofconcern'] if 'industryofconcern' in mydata else '',
            industry=mydata['industry'] if 'industry' in mydata else '',
            investment_field = mydata['investment_field'] if 'investment_field' in mydata else '',
            investment_location = mydata['investment_location']  if 'investment_location' in mydata else '',
            investment_budget = mydata['investment_budget'] if 'investment_budget' in mydata else '',
            investment_angel_status = mydata['investment_angel_status'] if 'investment_angel_status' in mydata else '',
            investment_a_status = mydata['investment_a_status'] if 'investment_a_status' in mydata else '',
            investment_b_status = mydata['investment_b_status'] if 'investment_b_status' in mydata else '',
            investment_pre_a_status = mydata['investment_pre_a_status'] if 'investment_pre_a_status' in mydata else '',
            investment_b_after_status = mydata['investment_b_after_status'] if 'investment_b_after_status' in mydata else '',
            activation_key = activation_key,
            key_expires = key_expires,
            user = user,
        )
        #print settings.MEDIA_URL + mydata['profile_img']
        if 'profile_img' in mydata:
           if mydata['profile_img'] <> '':
             new_investor.profile_img.name =   mydata['profile_img'].replace(' ','_')
             print new_investor.profile_img.name

        new_investor.save()

        for iexp in  mydata['investment_experiences']:

          #json_parsed_investdate = None
          #if iexp['invest_date']:
          #     json_parsed_investdate = parse(iexp['invest_date'])


          #sfilename = iexp['attached_project_file'] if 'attached_project_file' in iexp else ''
          doc = Document.objects.filter(email=iemail, seq = iexp['id'] ).order_by('-created').first()

          new_iexp = Investment_experiences(
             project_name = iexp['project_name'] if 'project_name' in iexp else '',
             #invest_date = json_parsed_investdate,
             invest_year=iexp['invest_year'] if 'invest_year' in iexp else '',
             invest_month=iexp['invest_month'] if 'invest_month' in iexp else '',

             invested_status = iexp['invested_status'] if 'invested_status' in iexp else '',
             project_detail = iexp['project_detail'] if 'project_detail' in iexp else '',
             investor = new_investor,
          )
          #print settings.MEDIA_URL + iexp['attached_project_file']
          #if (doc is not None):
          if 'attached_project_file' in iexp:
             if  iexp['attached_project_file'] <> '': 
               #new_iexp.attached_project_file.name = os.path.join( settings.MEDIA_URL ,new_investor.email + '_' + iexp['attached_project_file'].replace(' ','_') ) 
               new_iexp.attached_project_file.name = make_url( new_investor.email, iexp['attached_project_file'])
               print new_iexp.attached_project_file.name

          new_iexp.save()

    except:
        success = False
        print sys.exc_info()
        message = "error occurred " + str(sys.exc_info()[0])

    return_json = {}
    if (success):
       message = "please redirect to investor reigstration confirmation page" 
       return_json['userType'] = 'i'
       return_json['first_name'] = new_investor.first_name
       return_json['last_name'] = new_investor.last_name
       return_json['profile_img'] = new_investor.profile_img.name

    return_json['success'] = success    
    return_json['message'] = message
    json_str = json.dumps(return_json)
    response =  HttpResponse(json_str, content_type='application/json')
    if (success):
        print iemail
        print new_investor.first_name
        print new_investor.last_name
        print new_investor.profile_img.name
        response.set_cookie('keyagg','successful_login')
        response.set_cookie('userType','i')
        response.set_cookie('email',iemail)
        response.set_cookie('first_name',new_investor.first_name)
        response.set_cookie('last_name',new_investor.last_name)
        response.set_cookie('profile_img',new_investor.profile_img.name)

    return response


@csrf_exempt
def query_investor(request):
    print "in query investor loop"

    try:
     print request.POST.dict()
     print request.POST.items()
     print request.body

     mydata = json.loads(str(request.body))
     emailadd =  mydata['email'] if 'email' in mydata else '' 

     #emailadd = 'jj@jjj.com'
     print mydata
     print mydata['email']
     success = True
    
     if request.user.is_authenticated(): 
        
       inv = InvestorProfile.objects.filter( email = emailadd )
       if inv.exists():
          inv = InvestorProfile.objects.get( email = emailadd )
          print inv
          inv_dict = model_to_dict(inv)
          inv_dict["key_expires"] = inv_dict["key_expires"].isoformat()
          inv_dict["profile_img"] = inv_dict["profile_img"].name
          inv_dict["investment_experiences"]=[]
          print inv_dict

          inv_exp = inv.investment_experiences_set.all()
          for exp in inv_exp:
             print exp
             inv_exp_dict = model_to_dict(exp) 
             inv_exp_dict["attached_project_file"]=inv_exp_dict["attached_project_file"].name

             print inv_exp_dict
             inv_dict["investment_experiences"].append( inv_exp_dict )

          print inv_dict
       else:
          success=False
          message = "There is no investor with given email address"
     else:
        success=False
        message = "user has not logged in yet"
 
    except:
        success = False
        print sys.exc_info()
        message = "error occurred " + str(sys.exc_info()[0])
 
    return_json = {}
    return_json['success'] = success

    if (success):
        message = "investor profile has been successfully retrieved" 
        return_json['data'] = inv_dict

    return_json['message'] = message
    json_str = json.dumps(return_json, ensure_ascii=False)
    return HttpResponse(json_str, content_type='application/json')

@csrf_exempt
def query_user(request):
    print "in query user loop"
    try:

     print request.POST.dict()
     print request.POST.items()
     print request.body

     mydata = json.loads(str(request.body))
     emailadd =  mydata['email']
     print mydata
     print mydata['email']
     #emailadd = 'whereis@sandiego.com'
     success = True


     if request.user.is_authenticated():

        usr = UserProfile.objects.filter( email = emailadd )
        if usr.exists():
          usr = UserProfile.objects.get( email = emailadd )
          print usr
          usr_dict = model_to_dict(usr)
          usr_dict["key_expires"] = usr_dict["key_expires"].isoformat()
          usr_dict["profile_img"] = usr_dict["profile_img"].name
          usr_dict["business_experiences"]=[]
          print usr_dict

          usr_exp = usr.user_exp_set.all()
          for exp in usr_exp:
             print exp
             usr_exp_dict = model_to_dict(exp)
             usr_exp_dict["attached_file"]=usr_exp_dict["attached_file"].name
             print usr_exp_dict
             usr_dict["business_experiences"].append( usr_exp_dict )

          print usr_dict
        else:
          success=False
          message = "There are no user with give email address"

     else:
        success=False
        message = "user has not logged in yet"
     
    except:
        success = False
        print sys.exc_info()
        message = "error occurred " + str(sys.exc_info()[0])

    return_json = {}
    return_json['success'] = success

    if (success):
        message = "user profile has been successfully retrieved" 
        return_json['data'] = usr_dict

    return_json['message'] = message
    json_str = json.dumps(return_json, ensure_ascii=False)
    return HttpResponse(json_str, content_type='application/json')


@csrf_exempt
def queryall_investor(request):
   print "in query investor loop"

   try:
     print request.POST.dict()
     print request.POST.items()
     print request.body

     #mydata = json.loads(str(request.body))
     #emailadd =  mydata['email'] if 'email' in mydata else ''

     #print mydata
     #print mydata['email']
     success = True
     inv_results = []

     #if request.user.is_authenticated():
     if success:

       #inv_list = InvestorProfile.objects.filter( profession  = 'investing', job_title='manager' )
       inv_list = InvestorProfile.objects.all()	   
       if inv_list.exists():
          for inv in inv_list:
             print inv
             inv_dict = model_to_dict(inv)
             inv_dict["key_expires"] = inv_dict["key_expires"].isoformat()
             inv_dict["profile_img"] = inv_dict["profile_img"].name
             inv_dict["investment_experiences"]=[]
             print inv_dict

             inv_exp = inv.investment_experiences_set.all()
             for exp in inv_exp:
                print exp
                inv_exp_dict = model_to_dict(exp)
                inv_exp_dict["attached_project_file"]=inv_exp_dict["attached_project_file"].name

                print inv_exp_dict
                inv_dict["investment_experiences"].append( inv_exp_dict )

             print inv_dict
             inv_results.append( inv_dict )

       else:
          success=False
          message = "There is no investor with given search conditions"
     #else:
     #   success=False
     #   message = "user has not logged in yet"

   except:
        success = False
        print sys.exc_info()
        message = "error occurred " + str(sys.exc_info()[0])

   return_json = {}
   return_json['success'] = success

   if (success):
        message = "investor profile has been successfully retrieved"
        return_json['data'] = inv_results

   return_json['message'] = message
   json_str = json.dumps(return_json, ensure_ascii=False)
   return HttpResponse(json_str, content_type='application/json')





@csrf_exempt
def queryall_user(request):
   print "in query all user loop"

   try:
     print request.POST.dict()
     print request.POST.items()
     print request.body

     #mydata = json.loads(str(request.body))
     #emailadd =  mydata['email'] if 'email' in mydata else ''

     #print mydata
     #print mydata['email']
     success = True
     usr_results = []

     #if request.user.is_authenticated():
     if success:
       #usr_list = userProfile.objects.filter( profession  = 'usresting', job_title='manager' )
       usr_list = UserProfile.objects.all()	   
       if usr_list.exists():
          for usr in usr_list:
             print usr
             usr_dict = model_to_dict(usr)
             usr_dict["key_expires"] = usr_dict["key_expires"].isoformat()
             usr_dict["profile_img"] = usr_dict["profile_img"].name
             usr_dict["business_experiences"]=[]
             print usr_dict

             usr_exp = usr.user_exp_set.all()
             for exp in usr_exp:
                print exp
                usr_exp_dict = model_to_dict(exp)
                usr_exp_dict["attached_file"]=usr_exp_dict["attached_file"].name

                print usr_exp_dict
                usr_dict["business_experiences"].append( usr_exp_dict )

             print usr_dict
             usr_results.append( usr_dict )

       else:
          success=False
          message = "There is no usrestor with given search conditions"
     #else:
     #   success=False
     #   message = "user has not logged in yet"

   except:
        success = False
        print sys.exc_info()
        message = "error occurred " + str(sys.exc_info()[0])

   return_json = {}
   return_json['success'] = success

   if (success):
        message = "user profiles has been successfully retrieved"
        return_json['data'] = usr_results

   return_json['message'] = message
   json_str = json.dumps(return_json, ensure_ascii=False)
   return HttpResponse(json_str, content_type='application/json')


@csrf_exempt
def register_company(request):
    print "in register company loop"
    #mydata = json.loads(' {  "project_name":"New project", "company_logo":"images (1).jpg", "industry":"..", "location":"GuangZhou", "invested_status":"A .", "investment_budget":"<= .500.", "company_site":"http://www.designs88.us/agweb/creart_startup_2.html", "introduction":"test", "product_img1":"baby-unicorn-small.jpg", "product_img2":"a.png", "product_img3":"images (2).jpg", "product_img4":"33.256.gif", "business_plan":"Hoteles_cerca.pdf", "description":"company introduction" } ')

    #mydata = json.loads(' {"product_img1":"Joris.jpg","product_img2":"small.png","product_img3":"small.png","product_img4":"Joris.jpg","business_plan":"Hoteles_cerca.pdf","description":"testd dsfsd","project_name":"dfdsf","industry":"金融","location":"dsfsd","invested_status":"B 轮以后","investment_budget":"<= ￥200万","company_site":"sdfdsf","introduction":"sdfdsf","company_logo":"poster_rodents_small.jpg"} ')

    success = True

    try:
      print request.POST.dict()
      print request.POST.items()
      print request.body
      mydata = json.loads(str(request.body))
      print mydata

      if request.user.is_authenticated():
 
        new_company = Company(
            invested_status=mydata['invested_status'] if 'invested_status' in mydata else '',                
            description=mydata['description'] if 'description' in mydata else '',                            
            location=mydata['location'] if 'location' in mydata else '',                                     
            industry=mydata['industry'] if 'industry' in mydata else '',                                     
            project_name=mydata['project_name'] if 'project_name' in mydata else '',                         
            investment_budget=mydata['investment_budget'] if 'investment_budget' in mydata else '',
            company_site=mydata['company_site'] if 'company_site' in mydata else '',                         
            introduction=mydata['introduction'] if 'introduction' in mydata else '',                         
            user = request.user,
        )

        if 'company_logo' in mydata:
            if  mydata['company_logo'] <> '':
              #new_company.company_logo.name =  os.path.join( settings.MEDIA_URL , new_company.user.email + '_' +  mydata['company_logo'].replace(' ','_'))
              new_company.company_logo.name = make_url( new_company.user.email, mydata['company_logo'])
              print new_company.company_logo.name

        if 'business_plan' in mydata:
            if  mydata['business_plan'] <> '':
              #new_company.business_plan.name =  os.path.join( settings.MEDIA_URL , new_company.user.email + '_' +  mydata['business_plan'].replace(' ','_'))
              new_company.business_plan.name = make_url( new_company.user.email, mydata['business_plan'])
              print new_company.business_plan.name

        if 'product_img1' in mydata:
            if  mydata['product_img1'] <> '':
              #new_company.product_img1.name =  os.path.join( settings.MEDIA_URL , inew_company.user.email + '_' +  mydata['product_img1'].replace(' ','_'))
              new_company.product_img1.name = make_url( inew_company.user.email,  mydata['product_img1'])
              print new_company.product_img1.name
            
        if 'product_img2' in mydata:
            if  mydata['product_img2'] <> '':
              #new_company.product_img2.name =  os.path.join( settings.MEDIA_URL , new_company.user.email + '_' +  mydata['product_img2'].replace(' ','_'))
              new_company.product_img2.name = make_url( new_company.user.email, mydata['product_img2'])
              print new_company.product_img2.name

        if 'product_img3' in mydata:
            if  mydata['product_img3'] <> '':
              #new_company.product_img3.name =  os.path.join( settings.MEDIA_URL , new_company.user.email + '_' +  mydata['product_img3'].replace(' ','_'))
              new_company.product_img3.name = make_url( new_company.user.email, mydata['product_img3'])
              print new_company.product_img3.name

        if 'product_img4' in mydata:
            if  mydata['product_img4'] <> '':
              #new_company.product_img4.name =  os.path.join( settings.MEDIA_URL , new_company.user.email + '_' +  mydata['product_img4'].replace(' ','_'))
              new_company.product_img4.name = make_url( new_company.user.email, mydata['product_img4'])
              print new_company.product_img4.name
            
            
        new_company.save()

      else:
        success=False
        message = "user has not logged in yet"

    except:
        success = False
        print sys.exc_info()
        message = "error occurred " + str(sys.exc_info()[0])


    if (success):
        message = "please redirect to com;pany reigstration confirmation page"
    return_json = {}
    return_json['success'] = success
    return_json['message'] = message
    json_str = json.dumps(return_json)
    response =  HttpResponse(json_str, content_type='application/json')
    if (success):
        response.set_cookie('keyagg','successful_company_registration')
    return response



@csrf_exempt
def queryall_company(request):
   print "in query all company loop"

   try:
     print request.POST.dict()
     print request.POST.items()
     print request.body

     #mydata = json.loads(str(request.body))
     #emailadd =  mydata['email'] if 'email' in mydata else ''

     #print mydata
     #print mydata['email']
     success = True
     comp_results = []

     #if request.user.is_authenticated():
     if success:
       #comp_list = userProfile.objects.filter( profession  = 'compesting', job_title='manager' )
       comp_list = Company.objects.all()
       if comp_list.exists():
          for comp in comp_list:
             print comp
             comp_dict = model_to_dict(comp)
             comp_dict["company_logo"] = comp_dict["company_logo"].name
             comp_dict["business_plan"] = comp_dict["business_plan"].name
             comp_dict["product_img1"] = comp_dict["product_img1"].name
             comp_dict["product_img2"] = comp_dict["product_img2"].name
             comp_dict["product_img3"] = comp_dict["product_img3"].name
             comp_dict["product_img4"] = comp_dict["product_img4"].name
			 
             print comp_dict

             comp_results.append( comp_dict )

       else:
          success=False
          message = "There is no company with given search conditions"
     #else:
     #   success=False
     #   message = "user has not logged in yet"

   except:
        success = False
        print sys.exc_info()
        message = "error occurred " + str(sys.exc_info()[0])

   return_json = {}
   return_json['success'] = success

   if (success):
        message = "company profiles has been successfully retrieved"
        return_json['data'] = comp_results

   return_json['message'] = message
   json_str = json.dumps(return_json, ensure_ascii=False)
   return HttpResponse(json_str, content_type='application/json')














##################  TEST ROUTINES  #####################



def query_test(request):
    print "in query test loop"
    print request.POST.dict()
    print request.POST.items()
    print request.body

    #mydata = json.loads(str(request.body))
    #emailadd =  mydata['email']
    #print mydata
    #print mydata['email']
    emailadd = 'whereis@sandiego.com'
    success = True


    try:

     #if request.user.is_authenticated():

        usr = UserProfile.objects.get( email = emailadd )
        print usr
        usr_dict = model_to_dict(usr)
        usr_dict["key_expires"] = usr_dict["key_expires"].isoformat()
        usr_dict["profile_img"] = usr_dict["profile_img"].name
        usr_dict["business_experience"]=[]
        print usr_dict

        usr_exp = usr.user_exp_set.all()
        for exp in usr_exp:
           print exp
           usr_exp_dict = model_to_dict(exp)
           usr_exp_dict["attached_file"]=usr_exp_dict["attached_file"].name
           print usr_exp_dict
           usr_dict["business_experience"].append( usr_exp_dict )

        print usr_dict

     #else:
     #   success=False
     #   message = "user has not logged in yet"

    except:
        success = False
        print sys.exc_info()
        message = "error occurred " + str(sys.exc_info()[0])

    if (success):
        message = "user profile has been successfully retrieved"
    return_json = {}
    return_json['success'] = success
    return_json['message'] = message
    return_json['data'] = usr_dict
    json_str = json.dumps(return_json, ensure_ascii=False)
    return HttpResponse(json_str, content_type='application/json')



@csrf_exempt
def login_user(request):
    logger.info("user logging to angie global website")
    error = ''
    username = password = ''
    print request.body

    print "entering user login page"

    print request.method

    success = True
        
    #username ='whereis@sandiego.com'
    #password ='mrqwerty'

    mydata = json.loads(str(request.body))
    username = mydata['email']
    password = mydata['password']
    print username
    print password
    usertype = ''

    user = authenticate(username=username, password=password)

    if user is not None:
        if user.is_active:
            print "user is active"
            login(request, user)
            message =   'Your have succesfully logged in.'
            u_usr = user.userprofile_set.all()
            u_inv = user.investorprofile_set.all()

            if u_inv.exists():
               usertype = 'i'
               firstname = u_inv[0].first_name
               lastname = u_inv[0].last_name
               profileimg = u_inv[0].profile_img.name
               print "this user is investor"
 
            if u_usr.exists():
               usertype = 'e'
               firstname = u_usr[0].first_name
               lastname = u_usr[0].last_name
               profileimg = u_usr[0].profile_img.name
               print "this user is normal user"

        else:
            print 'Your account is not active, please contact the site'
            message = 'Your account is not active, please contact the site'
            success = False
            auth_logout(request)
    else:
        message = 'Your username and/or password are incorrect.'
        print  'Your username and/or password are incorrect.'
        success = False
        auth_logout(request)


    return_json = {}
    return_json['success'] = success
    return_json['message'] = message
    if (success):
       return_json['userType'] = usertype
       return_json['first_name'] = firstname
       return_json['last_name'] = lastname
       return_json['profile_img'] = profileimg
       response.set_cookie('userType',usertype)
       response.set_cookie('email',user.email)
       response.set_cookie('first_name',firstname)
       response.set_cookie('last_name',lastname)
       response.set_cookie('profile_img',profileimg)

    json_str = json.dumps(return_json)
    print json_str
    response =  HttpResponse(json_str, content_type='application/json')
    if (success):
           response.set_cookie('keyagg','successful_login')
    return response



@csrf_exempt
def multiquery_test(request):
    print "in query investor loop"

    try:
     print request.POST.dict()
     print request.POST.items()
     print request.body

     #mydata = json.loads(str(request.body))
     #emailadd =  mydata['email'] if 'email' in mydata else ''

     #emailadd = 'jj@jjj.com'
     #print mydata
     #print mydata['email']
     success = True
     inv_results = []
	 
     #if request.user.is_authenticated():

     inv_list = InvestorProfile.objects.filter( profession  = 'investing', job_title='manager' )
     if inv_list.exists():
	  for inv in inv_list:
             print inv
             inv_dict = model_to_dict(inv)
             inv_dict["key_expires"] = inv_dict["key_expires"].isoformat()
             inv_dict["profile_img"] = inv_dict["profile_img"].name
             inv_dict["investment_experiences"]=[]
             print inv_dict

             inv_exp = inv.investment_experiences_set.all()
             for exp in inv_exp:
                print exp
                inv_exp_dict = model_to_dict(exp)
                inv_exp_dict["attached_project_file"]=inv_exp_dict["attached_project_file"].name

                print inv_exp_dict
                inv_dict["investment_experiences"].append( inv_exp_dict )

             print inv_dict
	     inv_results.append( inv_dict )
			 
     else:
        success=False
        message = "There is no investor with given search conditions"
    #else:
    #    success=False
    #    message = "user has not logged in yet"

    except:
        success = False
        print sys.exc_info()
        message = "error occurred " + str(sys.exc_info()[0])

    return_json = {}
    return_json['success'] = success

    if (success):
        message = "investor profile has been successfully retrieved"
        return_json['data'] = inv_results

    return_json['message'] = message
    json_str = json.dumps(return_json, ensure_ascii=False)
    return HttpResponse(json_str, content_type='application/json')


@csrf_exempt
def register_investor_test(request):
    print "in register investor test loop"
    #print request.POST.dict()
    #print request.POST.items()
    #print request.body

    #mydata = json.loads(str(request.body))
    #mydata = json.loads( ' {  "last_name":"a", "first_name":"adf", "country":"US", "city":"ad", "industry":"互联网", "industryofconcern":"互联网", "company_name":"saf", "profession":"df", "introduction":"a", "investment_location":"adf", "investment_budget":"50???", "investment_pre_a_status":"Y", "investment_a_status":"Y", "investment_b_status":"Y", "profile_img":"Joris.jpg", "email":"ccab@a.com", "password":"a", "confirmPassword":"a", "investment_experiences":[  {  "id":1, "project_name":"adf", "invest_year":"2014", "invest_month":"3", "invested_status":"A ?", "project_detail":"adsf", "files":"", "attached_project_file":"images.jpg" }, {  "id":2, "project_name":"adsf", "invested_status":"Pre A", "invest_year":"2014", "invest_month":"3", "project_detail":"adsf", "files":"", "attached_project_file":"question.jpg" } ] }')


    mydata = json.loads( ' {"profile_img":"33.256.gif","last_name":"dsfds","industryofconcern":"金融","country":"美国","city":"sdf","company_name":"sdf","profession":"sdf","introduction":"sdf","investment_location":"sdf","investment_budget":"100-500万","investment_angel_status":"Y","investment_pre_a_status":"Y","email":"kkk@first_national.com","password":"kkkkkk","confsrmPassword":"aaaaaa","investment_experiences":[{"id":1,"project_name":"dsf","invest_year":"2014","invest_month":"3","invested_status":"Pre A","project_detail":"sdfds","files":"","attached_project_file":"baby-unicorn-small.jpg"},{"id":2,"project_name":"sdf","invested_status":"A 轮","invest_year":"2014","invest_month":"2","project_detail":"dsf","files":"","attached_project_file":"ddsaa.png"}]} ')

    print mydata
    if 'email' in mydata:
        print mydata['email']

    success = True
    if success:
    #try:
        # create a new user in the database
        user = User.objects.create_user(mydata['email'], mydata['email'], mydata['password'])
        user.is_active = True
        user.is_staff = False
        user.is_superuser = False
        user.save()
        # build activation keya

        salt = sha.new(str(random.random())).hexdigest()[:5]
        activation_key = sha.new(salt + mydata['email']).hexdigest()
        key_expires = datetime.datetime.today() + datetime.timedelta(2)

        #sfilename = mydata['profile_img'] if 'profile_img' in mydata else ''
        doc = Document.objects.filter(email=mydata['email'], seq = 0).order_by('-created').first()

        new_investor = InvestorProfile(
            first_name=mydata['first_name'] if 'first_name' in mydata else '',
            last_name=mydata['last_name'] if 'last_name' in mydata else '',
            email = mydata['email'],
            company_name=mydata['company_name'] if 'company_name' in mydata else '',
            introduction=mydata['introduction'] if 'introduction' in mydata else '',
            job_title=mydata['job_title'] if 'job_title' in mydata else '',
            personal_profile=mydata['personal_profile'] if 'personal_profile' in mydata else '',
            city=mydata['city'] if 'city' in mydata else '',
            country=mydata['country'] if 'country' in mydata else '',
            profession=mydata['profession'] if 'profession' in mydata else '',
            industryofconcern=mydata['industryofconcern'] if 'industryofconcern' in mydata else '',
            industry=mydata['industry'] if 'industry' in mydata else '',
            investment_field = mydata['investment_field'] if 'investment_field' in mydata else '',
            investment_location = mydata['investment_location']  if 'investment_location' in mydata else '',
            investment_budget = mydata['investment_budget'] if 'investment_budget' in mydata else '',
            investment_angel_status = mydata['investment_angel_status'] if 'investment_angel_status' in mydata else '',
            investment_a_status = mydata['investment_a_status'] if 'investment_a_status' in mydata else '',
            investment_b_status = mydata['investment_b_status'] if 'investment_b_status' in mydata else '',
            investment_pre_a_status = mydata['investment_pre_a_status'] if 'investment_pre_a_status' in mydata else '',
            investment_b_after_status = mydata['investment_b_after_status'] if 'investment_b_after_status' in mydata else '',
            activation_key = activation_key,
            key_expires = key_expires,
            user = user,
        )
        #print settings.MEDIA_URL + mydata['profile_img']
        if 'profile_img' in mydata:
           #new_investor.profile_img.name =  os.path.join( settings.MEDIA_URL , mydata['profile_img'].replace(' ','_'))
           new_investor.profile_img.name = make_url( new_investor.email, mydata['profile_img'])
           print new_investor.profile_img.name

        new_investor.save()

        for iexp in  mydata['investment_experiences']:

          #json_parsed_investdate = None
          #if iexp['invest_date']:
          #     json_parsed_investdate = parse(iexp['invest_date'])


          #sfilename = iexp['attached_project_file'] if 'attached_project_file' in iexp else ''
          doc = Document.objects.filter(email=mydata['email'], seq = iexp['id'] ).order_by('-created').first()

          new_iexp = Investment_experiences(
             project_name = iexp['project_name'] if 'project_name' in iexp else '',
             #invest_date = json_parsed_investdate,
             invest_year=iexp['invest_year'] if 'invest_year' in iexp else '',
             invest_month=iexp['invest_month'] if 'invest_month' in iexp else '',

             invested_status = iexp['invested_status'] if 'invested_status' in iexp else '',
             project_detail = iexp['project_detail'] if 'project_detail' in iexp else '',
             investor = new_investor,
          )
          #print settings.MEDIA_URL + iexp['attached_project_file']
          #if (doc is not None):
          if 'attached_project_file' in iexp:
             #new_iexp.attached_project_file.name = os.path.join( settings.MEDIA_URL ,iexp['attached_project_file'].replace(' ','_') )
             new_iexp.attached_project_file.name = make_url( new_investor.email, iexp['attached_project_file'])
             print new_iexp.attached_project_file.name

          new_iexp.save()

    #except:
    #    success = False
    #    print sys.exc_info()
    #    message = "error occurred " + str(sys.exc_info()[0])

    if (success):
        message = "please redirect to user reigstration confirmation page"
    return_json = {}
    return_json['success'] = success
    return_json['message'] = message
    json_str = json.dumps(return_json)
    response =  HttpResponse(json_str, content_type='application/json')
    if (success):
        response.set_cookie('keyagg','successful_login')
    return response

    
@csrf_exempt
def register_check(request):
    print "in register check loop"
    print request.body

    mydata = json.loads(str(request.body))
    email_addr = mydata["email"]
    #email_addr = 'whereis@sandiego.com'
    success = True

    try:
        if User.objects.filter(email=email_addr).exists():
           success=False
           message="Email address has already been used" 
        else:
           success=True
           message="Email address is available for registration"
    except:
        success = False
        print sys.exc_info()
        message = "error occurred " + str(sys.exc_info()[0])

    return_json = {} 
    return_json['availble'] = success
    return_json['message'] = message
    json_str = json.dumps(return_json)
    response =  HttpResponse(json_str, content_type='application/json')
    return response


   
@csrf_exempt
def register_check_test(request):
    print "in register check loop"
    print request.body

    #mydata = json.loads(str(request.body))
    #email_addr = mydata["email"]
    email_addr = 'herei@om'
    success = True

    print email_addr
    if success:
    #try:
        if User.objects.filter(email=email_addr).exists():
           success=False
           message="Email address has already been used"
        else:
           success=True
           message="Email address is available for registration"
    #except:
    #    success = False
    #    print sys.exc_info()
    #    message = "error occurred " + str(sys.exc_info()[0])

    return_json = {}
    return_json['availble'] = success
    return_json['message'] = message
    json_str = json.dumps(return_json)
    response =  HttpResponse(json_str, content_type='application/json')
    return response


@csrf_exempt
def password_reset(request):
    print "in password reset loop"
    print request.body

    mydata = json.loads(str(request.body))
    email_addr = mydata["email"]
    #email_addr = 'whereis@sandiego.com'
    success = True

    try:
        if User.objects.filter(email=email_addr).exists():
           success=True
           message="Password reset is successful"
        else:
           success=False
           message="Email does not exist.  Password reset failed"
    except:
        success = False
        print sys.exc_info()
        message = "error occurred " + str(sys.exc_info()[0])

    return_json = {}
    return_json['success'] = success
    return_json['message'] = message
    json_str = json.dumps(return_json)
    response =  HttpResponse(json_str, content_type='application/json')
    return response

 
