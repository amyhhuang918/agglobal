// JavaScript Document

var total_q = 0;

$(document).ready(function(){


 loadInfo();



 $("#search_investor").click(function (e) {
        e.preventDefault();
        investment_status = $('#investment_status').val();
        investment_angel_status = "";
        investment_pre_a_status = "";
        investment_a_status = "";
        investment_b_status = "";
        investment_b_after_status = "";
        switch (investment_status) {
            case "investment_angel_status":
                investment_angel_status = "Y";
                break;
            case "investment_a_status":
                investment_a_status = "Y";
                break;
            case "investment_pre_a_status":
                investment_pre_a_status = "Y";
                break;
            case "investment_b_status":
                investment_b_status = "Y";    
            case "investment_b_after_status":
                investment_b_after_status = "Y";
                break;
        }


        var searchi = {
            investment_field: $('#investment_field').val(),
            investment_angel_status: investment_angel_status,
            investment_pre_a_status: investment_pre_a_status,
            investment_a_status: investment_a_status,
            investment_b_status: investment_b_status,
            investment_b_after_status: investment_b_after_status,
            investment_budget: $('#investment_budget').val(),
            investment_location: $('#investment_location').val(),
            certified: $("#ag_approve").prop("checked")   //  true or false
        }
        $.ajax({
            type: "POST",
            async: false,
            dataType: "json",
            url: "/agglobal/queryfilter_investor/",
            data: JSON.stringify(searchi),
            success: function (data) {


                $("#result").html("");
                if (data.data) {
                    $.each(data.data, function (i, item) {

                        var investment_status = "全部";

                        if (item.investment_angel_status == "Y") {
                            investment_status = "天使轮";
                        }
                        ;
                        if (item.investment_pre_a_status == "Y") {
                            investment_status = "Pre A";
                        }
                        ;
                        if (item.investment_a_status == "Y") {
                            investment_status = "A 轮";
                        }
                        ;
                        if (item.investment_b_status == "Y") {
                            investment_status = "B 轮";
                        }
                        if (item.investment_b_after_status == "Y") {
                            investment_status = "B 轮以后";
                        }
                        ;


                        if (item.ag_approve == "true") {

                            $("#result").append(
                                    "<tr><td><a href='/agglobal/viewInvProfile/" + item.user + "'<div class='pull-left'><img class='list-img circle' src='" + item.profile_img +
                                    "' alt='photo'></div><div class='pull-left'><h5>" + item.last_name +
                                    " <span class='badge badge-sm'>认证</span></h5>" + item.introduction +
                                    "</a></td><td>" + item.investment_field +
                                    "</td><td>" + investment_status +
                                    "</td><td>" + item.investment_budget +
                                    "</td><td>" + item.investment_location + "</td></tr>");
                        } else {

                            $("#result").append(
                                    "<tr><td><a href='/agglobal/viewInvProfile/" + item.user + "'<div class='pull-left'><img class='list-img circle' src='" + item.profile_img +
                                    "' alt='photo'></div><div class='pull-left'><h5>" + item.last_name +
                                    "</h5>" + item.introduction +
                                    "</a></td><td>" + item.investment_field +
                                    "</td><td>" + investment_status +
                                    "</td><td>" + item.investment_budget +
                                    "</td><td>" + item.investment_location + "</td></tr>");


                        }

                        total_q = i;


                        pagiNation();


                    })
                }
            }});
    });








});









function loadInfo() {
    $.getJSON("/agglobal/queryall_investor/", function(data) {
        $("#result").html("");
        $.each(data.data, function(i, item) {

                var investment_status = "全部";

                 if (item.investment_angel_status == "Y"){
                      investment_status = "天使轮";
                 };
                 if (item.investment_pre_a_status == "Y"){
                      investment_status = "Pre A";
                 };
                 if (item.investment_a_status == "Y"){
                      investment_status = "A 轮";
                 };
                 if (item.investment_b_status == "Y"){
                      investment_status = "B 轮";
                 }
                 if (item.investment_b_after_status == "Y"){
                      investment_status = "B 轮以后";
                 };




           
           if (item.ag_approve == "true") {

                $("#result").append(

                    "<tr><td>><a href='/agglobal/viewInvProfile/" + item.user + "'<div class='pull-left'><img class='list-img circle' src='" + item.profile_img +
                    "' alt='photo'></div><div class='pull-left'><h5>" + item.last_name  +
                    " <span class='badge badge-sm'>认证</span></h5>" + item.introduction + 
                    "</a></td><td>" + item.investment_field +
                    "</td><td>" + investment_status +
                    "</td><td>" + item.investment_budget +
                    "</td><td>" + item.investment_location + "</td></tr>");
            } else {

                $("#result").append(

                    "<tr><td><a href='/agglobal/viewInvProfile/" + item.user + "'><div class='pull-left'><img class='list-img circle' src='" + item.profile_img +
                    "' alt='photo'></div><div class='pull-left'><h5>" + item.last_name  +
                    "</h5>" + item.introduction + 
                    "</a></td><td>" + item.investment_field +
                    "</td><td>" + investment_status +
                    "</td><td>" + item.investment_budget +
                    "</td><td>" + item.investment_location + "</td></tr>");


            }

          total_q = total_q+1;

          

         pagiNation();


        });

    }).error(function() { alert("连接数据库失败！"); });

}


function pagiNation(){

    
  $("#result tr:gt(14)").hide();
   

    var current_page=15;
    var current_num=1;
   // var total_page= Math.round(total_q/current_page);
    var total_page= Math.ceil(parseFloat(total_q)/parseFloat(current_page));

    var next=$(".next");
    var prev=$(".prev");
    $(".total").text(total_page);
    $(".current_page").text(current_num);
    
    $(".next").click(function(){
          if(current_num==total_page){
              return false;
          }
          else{
              $(".current_page").text(++current_num);
              $.each($('#result tr'),function(index,item){
                  var start = current_page* (current_num-1);
                  var end = current_page * current_num;
                          if(index >= start && index < end){
                             $(this).show();
                          }else {
                             $(this).hide();
                          }
                });
          }
    });


    $(".prev").click(function(){
        if(current_num==1){
            return false;
        }else{
            $(".current_page").text(--current_num);
            $.each($('#result tr'),function(index,item){
                var start = current_page* (current_num-1);
                var end = current_page * current_num;
                 if(index >= start && index < end){
                    $(this).show();
                 }else {
                     $(this).hide();
                 }
            });
        }
    });

}

