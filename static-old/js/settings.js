	$(document).ready(function() {
        $('.alert').hide();
		$("#passwordForm").validate({
			rules: {
				cpassword: {
					required: true,
					minlength: 5
				},
				password1: {
					required: true,
					minlength: 5
				},
				password2: {
					required: true,
					minlength: 5,
                    equalTo: "#password1"
				}
			},
			messages: {
				cpassword: {
                    required: "请输入你的目前密码",
                    minlength: "你的目前密码必须是5位字母"
                },
				password1: {
                    required: "请输入你的新密码",
                    minlength: "你的新密码必须是5位字母"
                },
				password2: {
                    required: "请输入你的重复密码",
                    minlength: "你的重复密码必须是5位字母",
					equalTo: "必须输入同样的密码"
                }
			}
        });
        $('#submitPassword').click(function(){
           event.preventDefault();

            data = {
                cpassword: $('#cpassword').val(),
                password1: $('#password1').val(),
                password2: $('#password2').val()
            }


           $.ajax({
                type: "POST",
                async: true,
                cache: false,
                dataType: "json",
                url: '/agglobal/password_reset/',
                data: JSON.stringify(data),
                error: function(request, status, error) {
                    console.error(request.responseText);
                    $('.alert').show();
                },
                success: function(result) {
                   console.log(result);
                   if (result.success == false) {
                       $('.alert').show();
                   }
                }
           });
        });
	});