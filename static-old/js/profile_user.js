$('#message-submit').on('click',function (event){
	event.preventDefault();

	var msgToSend = {
	    "subject": "test",
		"sender": $(this).data("sender"),
		"receiver": $(this).data("receiver"),
		"body": $('#my-msg').val(),
		"parentid": null
	};
	$.ajax({
	    url: '/agglobal/newMessage/',
	    data: JSON.stringify(msgToSend),
	    dataType: 'json',
	    type: 'POST',
	    crossDomain: true,
	    success: function(msgs) {
	    	$('#sendMessage').modal('toggle')
	    },
	    error: function(err) {
	    	console.log(err);
	    }
	});
	
});