function getUnread(userId){
	$.ajax({
	    url: '/agglobal/allMessages/' + userId,
	    //data: session,
	    type: 'GET',
	    success: function(msgs) {
	    	var unreadMsg = [];
	    	var msgCount = 0;
	    	$.each(msgs,function (index,msg){
	    		if(msg.read == 'F'){
	    			msgCount++;
	    			msg.id = index;
	    			unreadMsg.push(msg);
	    		}
	    	});
	    	if(msgCount!=0){
	    		$('#unread-count').text(msgCount);
	    		unreadMsg.reverse();
	    		$.each(unreadMsg, function (index,msg){
	    			$('#unread-msgs-list').append("<tr><td class='down-email-td'><img class='signIn-img' alt='' src='images/user_example.jpg'></td><td><strong><a id='"+index+"' class='unread-msg' href='#'>"+msg.body+"</a></strong></td></tr>");
	    		});
	    		$("#unread-msgs-list tr:gt(3)").hide();
	    		$('#unread-msgs-list').append("<tr><td></td><td class='text-right'><a class='unread-msg' href=''><i class='fa fa-envelope'></i> 查看所有邮件>> </a></td></tr>");
	    		$('.unread-msg').on('click',function (event){

	    		});
	    	}

	    	
	    	// $('#msg-wrapper').html('');
	    	// $('#msg-wrapper').append("");
	    	// var isNewEmail; //class for new email
	    	// $.each(unreadMsg, function (index,msg){
	    	// 	if(msg.read == 'F')
	    	// 		//add class to message if unread
	    	// 		isNewEmail = 'new-email';
	    	// 	else
	    	// 		isNewEmail = '';
	    	// 	$('#inbox').append("<tr id='"+msg.id+"' class='"+isNewEmail+"'><td><div class='pull-left'><a href='＃'><img class='email-img' src='http://ai22.mooo.com:8080"+msg.sender.profile_img+"' alt='photo'></a></div> <div class='pull-left'><b>"+msg.sender.display_name+"</b><br><span class='post-date'>"+moment(msg.senttime).format('MM/DD/YYYY hh:mm')+"</span></div></td><td><a href='' class='enter-msg'>"+msg.body+"</a></td><td><a href='#' class='btn-delete'><i class='del-msg fa fa-trash-o'></i></a></td></tr>");
	    	// });
	    },
	    //error: function() { alert('Failed!'); }
	});
}

//show all inbox messages
function showInboxMsg(userId){
	$.ajax({
	    url: '/agglobal/allMessages/' + userId,
	    //data: session,
	    type: 'GET',
	    success: function(msgs) {
	    	total_q= 0;
	    	displayMsg = [];
	    	var msgCount = 0;
	    	$.each(msgs,function (index,msg){
	    		//if(msg.parentid == null){
	    			msgCount++;
	    			msg.id = index;
	    			displayMsg.push(msg);
	    		//}
	    	});
	    	displayMsg.reverse();
	    	//sessionStorage.setItem('agInboxMsgs', JSON.stringify(displayMsg));
	    	$('#msg-wrapper').html('');
	    	$('#msg-wrapper').append("<table class='table table-hover email'><tbody id='inbox'></tbody></table><div class='clearfix'></div><div class='page_btn'><span class='page_box'><a href='#' class='prev'>上一页</a><span class='num'><span class='current_page'>1</span><span style='padding:0 3px;'>/</span><span class='total'></span></span><a href='#' class='next'>下一页</a></span><div>");
	    	var isNewEmail; //class for new email
	    	$.each(displayMsg, function (index,msg){
	    		if(msg.read == 'F')
	    			//add class to message if unread
	    			isNewEmail = 'new-email';
	    		else
	    			isNewEmail = '';
	    		$('#inbox').append("<tr id='"+msg.id+"' class='"+isNewEmail+"'><td><div class='pull-left'><a><img class='email-img' src='" + msg.sender.profile_img+"' alt='photo'></a></div> <div class='pull-left'><b>"+msg.sender.display_name+"</b><br><span class='post-date'>"+moment(msg.senttime).format('MM/DD/YYYY hh:mm')+"</span></div></td><td><a href='' class='enter-msg'>"+msg.body+"</a></td><td><a href='#' class='btn-delete'><i class='del-msg fa fa-trash-o'></i></a></td></tr>");
	    		total_q = total_q+1;
	    	});
	    	
	    	pagiNation();
	    },
	    //error: function() { alert('Failed!'); }
	});
}

function showSentMsg(userId){
	$.ajax({
	    url: '/agglobal/allSentMessages/' + userId,
	    //data: session,
	    type: 'GET',
	    success: function(msgs) {
	    	sent_total_q= 0;
	    	displaySentMsg = [];
	    	var msgCount = 0;
	    	$.each(msgs,function (index,msg){
	    		//if(msg.parentid == null){
	    			msgCount++;
	    			msg.id = index;
	    			displaySentMsg.push(msg);
	    		//}
	    	});
	    	displaySentMsg.reverse();
	    	//sessionStorage.setItem('agInboxMsgs', JSON.stringify(displaySentMsg));
	    	$('#sent-msg-wrapper').html('');
	    	$('#sent-msg-wrapper').append("<table class='table table-hover email'><tbody id='sent-msgs'></tbody></table><div class='clearfix'></div><div class='page_btn'><span class='page_box'><a href='#' class='prev1'>上一页</a><span class='num'><span class='current_page1'>1</span><span style='padding:0 3px;'>/</span><span class='total1'></span></span><a href='#' class='next1'>下一页</a></span><div>");

	    	$.each(displaySentMsg, function (index,msg){
	    		$('#sent-msgs').append("<tr id='"+msg.id+"'><td><div class='pull-left'><a><img class='email-img' src='"+msg.sender.profile_img+"' alt='photo'></a></div> <div class='pull-left'><b>"+msg.sender.display_name+"</b><br><span class='post-date'>"+moment(msg.senttime).format('MM/DD/YYYY hh:mm')+"</span></div></td><td><a href='' class='enter-msg'>"+msg.body+"</a></td><td><a href='#' class='btn-delete'><i class='del-msg fa fa-trash-o'></i></a></td></tr>");
	    		sent_total_q = sent_total_q+1;
	    	});
	    	
	    	sentPagiNation();
	    },
	    //error: function() { alert('Failed!'); }
	});
}

function markRead(msgId){
	$.ajax({
	    url: '/agglobal/readMessage/'+msgId,
	    //data: session,
	    type: 'GET'
	    // crossDomain: true,
	});
}

function pagiNation(){

    var msgNumberPerPage = 6;
  	$("#inbox tr:gt("+(msgNumberPerPage-1)+")").hide();
   

    var current_page=msgNumberPerPage;
    var current_num=1;
   // var total_page= Math.round(total_q/current_page);
    var total_page= Math.ceil(parseFloat(total_q)/parseFloat(current_page));

    var next=$(".next");
    var prev=$(".prev");
    $(".total").text(total_page);
    $(".current_page").text(current_num);
    
    $(".next").click(function(){
          if(current_num==total_page){
              return false;
          }
          else{
              $(".current_page").text(++current_num);
              $.each($('#inbox tr'),function(index,item){
                  var start = current_page* (current_num-1);
                  var end = current_page * current_num;
                          if(index >= start && index < end){
                             $(this).show();
                          }else {
                             $(this).hide();
                          }
                });
          }
    });


    $(".prev").click(function(){
        if(current_num==1){
            return false;
        }else{
            $(".current_page").text(--current_num);
            $.each($('#inbox tr'),function(index,item){
                var start = current_page* (current_num-1);
                var end = current_page * current_num;
                 if(index >= start && index < end){
                    $(this).show();
                 }else {
                     $(this).hide();
                 }
            });
        }
    });

}

function sentPagiNation(){

    var msgNumberPerPage = 6;
  	$("#sent tr:gt("+(msgNumberPerPage-1)+")").hide();
   

    var current_page=msgNumberPerPage;
    var current_num=1;
   // var total_page= Math.round(total_q/current_page);
    var total_page= Math.ceil(parseFloat(sent_total_q)/parseFloat(current_page));

    var next=$(".next1");
    var prev=$(".prev1");
    $(".total1").text(total_page);
    $(".current_page1").text(current_num);
    
    $(".next1").click(function(){
          if(current_num==total_page){
              return false;
          }
          else{
              $(".current_page1").text(++current_num);
              $.each($('#sent tr'),function(index,item){
                  var start = current_page* (current_num-1);
                  var end = current_page * current_num;
                          if(index >= start && index < end){
                             $(this).show();
                          }else {
                             $(this).hide();
                          }
                });
          }
    });


    $(".prev1").click(function(){
        if(current_num==1){
            return false;
        }else{
            $(".current_page1").text(--current_num);
            $.each($('#inbox tr'),function(index,item){
                var start = current_page* (current_num-1);
                var end = current_page * current_num;
                 if(index >= start && index < end){
                    $(this).show();
                 }else {
                     $(this).hide();
                 }
            });
        }
    });

}

$(document).ready(function(){
        var id = $("#userid").val();
	showInboxMsg(id);
	showSentMsg(id);
	getUnread(id);
});

function showOneMessage(msgId, msgType){
	$.ajax({
	    url: '/agglobal/messageChain/'+msgId,
	    type: 'GET',
	    success: function(msgs) {
	    	if(msgType=='INBOX'){
	    		var wrapper = '#msg-wrapper';
	    		var oneMsg = 'thisMsg';
	    		var backBtn = '收件箱';
	    		var backId = 'back-to-inbox';
	    		var report = "｜<a href='#'>举报垃圾邮箱</a>";
	    		var submitBtn = 'message-submit';
	    	}else{
	    		var wrapper = '#sent-msg-wrapper';
	    		var oneMsg = 'thisSentMsg';
	    		var backBtn = '发件箱';
	    		var backId = 'back-to-sent';
	    		var report = '';
	    		var submitBtn = 'sent-message-submit';
	    	}
	    	$(wrapper).html('');
	    		$(wrapper).append("<div class='email-title text-center><h3>您和"+msgs[msgId].sender.display_name+"的私信</h3></div><div class='text-right'><a id='"+backId+"' href=''>"+backBtn+"</a> ｜ <a href=''>删除</a> "+report+"</div><table class='table email vtop'><tbody id='"+oneMsg+"'></tbody></table>");
	    	$.each(msgs,function(index,msg){
	    		$('#'+oneMsg).append("<tr><td width='25%'><div class='pull-left'><a href='#'><img class='email-img' src='"+msg.sender.profile_img+"' alt='photo'></a></div><div class='pull-left'><b>"+msg.sender.display_name+"</b><br><span class='post-date'>"+""+"</span></div> </td><td>"+msg.body+"</td></tr>");
	    	});
               $('#'+oneMsg).append("<tr><td><div class='pull-left'><a href=''><img class='email-img' src='" + $('#userid').data('lprofimg') + "' alt='photo'></a></div><div class=' pull-left'><b>"+$('#userid').data('lusrname') +"</b><br><span class='post-date'>"+moment().format('MM/DD/YYYY hh:mm')+"</span></div>" +
                                " </td><td><form><div" +
                                " class='form-group'><textarea id='my-reply' class='form-control' rows='8' autofocus></textarea></div><button id='"+submitBtn+"' type='submit' class='btn btn-default'>发送</button></form></td></tr>");
	    	markRead(msgId);
	   		$('#message-submit').on('click',function (event){
				event.preventDefault();
				replyMessage(msgId, msgs[msgId].sender.id);
			});
			$('#sent-message-submit').on('click',function (event){
				event.preventDefault();
				replyMessage(msgId, msgs[msgId].reciever.id);
			});
	    },
	    error: function(err) {
	    	console.log(err);
	    }
	});
}


	// $.each(displayMsg,function (index,msg){
	//    	if(msg.id == msgId){
	//    		if(msg.read="F")
	//    			markRead(msg.id);
	//    		//sessionStorage.setItem('agOneMsg', JSON.stringify(msg));
	//    		$('#msg-wrapper').html('');
	//    		$('#msg-wrapper').append("<div class='email-title text-center'><h3>您和"+msg.sender.display_name+"的私信</h3></div><div class='text-right'><a id='back-to-inbox' href=''>收件箱</a> ｜ <a href=''>删除</a> ｜ <a href='#'>举报垃圾邮箱</a></div> <table class='table email vtop'><tbody id='thisMsg'><tr><td width='25%'><div class='pull-left'><a href=''><img class='email-img' src='"+msg.sender.profile_img+"' alt='photo'></a></div><div class='pull-left'><b>"+msg.sender.display_name+"</b><br><span class='post-date'>"+moment(msg.senttime).format('MM/DD/YYYY hh:mm')+"</span></div> </td><td>"+msg.body+"</td></tr><tr><td><div class='pull-left'><a href='＃'><img class='email-img' src='images/user_example.jpg' alt='photo'></a></div><div class=' pull-left'><b>"+"MyName"+"</b><br><span class='post-date'>2015-06-05 13:31</span></div> </td><td><form><div class='form-group'><textarea id='my-reply' class='form-control' rows='8' autofocus></textarea></div><button id='message-submit' type='submit' class='btn btn-default'>发送</button></form></td></tr></tbody></table>");
	//    		$('#message-submit').on('click',function (event){
	// 			event.preventDefault();
	// 			replyMessage(msg.id);
	// 		});
 //    	}
 //    });

function replyMessage(msgId, receiverid){
	//Get last message id and reply it
	$.ajax({
	    url: '/agglobal/lastMessage/'+msgId+'/',
	    type: 'GET',
	    //crossDomain: true,
	    success: function(msg) {
	    	$.each(msg,function (index){
	    		var msgToSend = {
				    "subject": "test",
					"sender": $('#userid').val(),
					"receiver": receiverid,
					"body": $('#my-reply').val(),
					"parentid": index
				};
				$.ajax({
				    url: '/agglobal/newMessage/',
				    data: JSON.stringify(msgToSend),
				    dataType: 'json',
				    type: 'POST',
				    //crossDomain: true,
				    success: function() {
				    		
						if($("a[href=#h2tab1]").parent().hasClass('active'))
							showOneMessage(msgId,'INBOX');
						else
							showOneMessage(msgId,'SENT');
				    },
				    error: function(err) {
				    	console.log(err);
				    }
				});
	    	});
	    },
	    error: function(err) {
	    	console.log(err);
	    }
	});
	
}

//
$('#msg-wrapper').on('click','.enter-msg',function (event){
	event.preventDefault();
	var msgId = $(this).parent().parent().attr('id');
	if($("a[href=#h2tab1]").parent().hasClass('active'))
		showOneMessage(msgId,'INBOX');
	else
		showOneMessage(msgId,'SENT');
});

$('#sent-msg-wrapper').on('click','.enter-msg',function (event){
	event.preventDefault();
	var msgId = $(this).parent().parent().attr('id');
	if($("a[href=#h2tab1]").parent().hasClass('active'))
		showOneMessage(msgId,'INBOX');
	else
		showOneMessage(msgId,'SENT');
});
$('#msg-wrapper').on('click','.del-msg',function (event){
	$.ajax({
	    url: '/agglobal/deleteMessage/'+$(this).parent().parent().parent().attr('id'),
	    //data: session,
	    type: 'GET',
	    // crossDomain: true,
	    success: function(msgs) {
	    	showInboxMsg($('#userid').val());
	    },
	    error: function(err) {
	    	//console.log(err);
	    }
	});
})

//from message detail to inbox
$('#msg-wrapper').on('click','#back-to-inbox',function (event){
	event.preventDefault();
	showInboxMsg($('#userid').val());
});

$('#sent-msg-wrapper').on('click','#back-to-sent',function (event){
	event.preventDefault();
	showSentMsg($('#userid').val());
});
