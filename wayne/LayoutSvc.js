﻿(function () {
    'use strict';

    var serviceId = 'LayoutSvc';

    function layoutSvc(common) {

        var service = {
            signIn: signIn
        };
        return service;
        
        function signIn(request) {
            console.log(JSON.stringify(request));
            return common.sendHttpMessage('POST', '/agglobal/login/', request);
        };
    };

    layoutSvc.$inject = ['common'];

    angular.module('ag-content').factory(serviceId, layoutSvc);
})();