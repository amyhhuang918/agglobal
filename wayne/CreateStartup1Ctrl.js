﻿(function () {
    'use strict';
    var controllerId = 'CreateStartup1Ctrl';

    function createStartup1Ctrl($scope, $cookies, common, acsCommonSvc, createStartup1Svc, upload, $timeout) {
        var vm = this;
        vm.controllerId = controllerId;
        vm.model = {};
        vm.init = function() {            
            vm.model.product_img1 = 'images/team-member-1.jpg';
            vm.model.product_img2 = 'images/team-member-1.jpg';
            vm.model.product_img3 = 'images/team-member-1.jpg';
            vm.model.product_img4 = 'images/team-member-1.jpg';

            var request = {};
            var promise = createStartup1Svc.getStartup(request);
            promise.then(
                function(response) {
                    if (response.success == false) {
                        vm.model['id'] = 0;
                        return;
                    } else {
                        vm.model = response.data;
                        var email = $cookies.get('email').replace(/\"/g, "");                  
                        vm.model.business_plan = vm.model.business_plan.substring(vm.model.business_plan.lastIndexOf(email + '_') + email.length + 1);
                        vm.model.company_logo = vm.model.company_logo.substring(vm.model.company_logo.lastIndexOf(email + '_') + email.length + 1);
                        console.log(JSON.stringify(vm.model));
                    }
                },
                function(reason) {
                    alert('failed to Get data');

                });
        }

        vm.countOf = function (text) {
            return text ? text.length : '0';
        };

     

        vm.uploadFile = function (files) {
            if (files && files.length) {
                if (acsCommonSvc.userInfo.data.email == undefined) {
                    acsCommonSvc.userInfo.data.email = $cookies.get('email');
                }
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    return upload.upload({
                        url: '/agglobal/file_upload/',
                        fields: {
                            'email': acsCommonSvc.userInfo.data.email
                        },
                        file: file
                    });
                }
            }

        };

        vm.pdfFileSelected = function ($files, imageSrc) {
            if ($files[0] != undefined) {
                if ($files[0].type.indexOf('pdf') == -1) {
                    $('#pdfTypeError').modal('show');
                    return false;
                }
                
                var promise = vm.uploadFile($files);
                promise.progress(function (evt) {

                }).success(function (data, status, headers, config) {
                    $timeout(function () {                       
                       //vm.model.business_plan = data.file_name;               
                    });
                });
            }
        }

        vm.fileSelected = function ($files, imageSrc) {
            if ($files[0] != undefined) {
                if ($files[0].type.indexOf('image') == -1) {
                    $('#imageTypeError').modal('show');
                    return false;
                }
                if ($files[0].size > 1000000) {
                    $('#imageSizeError').modal('show');
                    return false;
                }

                var promise = vm.uploadFile($files);
                promise.progress(function (evt) {

                }).success(function (data, status, headers, config) {
                    $timeout(function () {
                        if (imageSrc != null) {
                            $('#' + imageSrc).attr('src', '');
                            $('#' + imageSrc).attr('src', data.file_name);
                        } 
                    });
                });
            }
        }


        vm.next = function (isValid) {
            if (isValid) {

                var email = $cookies.get('email').replace(/\"/g, "");
                
                if (typeof (vm.model.company_logo) == 'string')
                    vm.model['company_logo'] = vm.model.company_logo.substring(vm.model.company_logo.lastIndexOf(email + '_') + email.length + 1);
                else if (typeof (vm.model.company_logo) == 'object')
                    vm.model['company_logo'] = vm.model.company_logo[0].name;


                if (typeof (vm.model.product_img1) == 'string')
                    vm.model['product_img1'] = vm.model.product_img1.substring(vm.model.product_img1.lastIndexOf(email + '_') + email.length + 1);
                else if (typeof (vm.model.product_img1) == 'object')
                    vm.model['product_img1'] = vm.model.product_img1[0].name;

                if (typeof (vm.model.product_img2) == 'string')
                    vm.model['product_img2'] = vm.model.product_img2.substring(vm.model.product_img2.lastIndexOf(email + '_') + email.length + 1);
                else if (typeof (vm.model.product_img2) == 'object')
                    vm.model['product_img2'] = vm.model.product_img2[0].name;
                

                if (typeof (vm.model.product_img3) == 'string')
                    vm.model['product_img3'] = vm.model.product_img3.substring(vm.model.product_img3.lastIndexOf(email + '_') + email.length + 1);
                else if (typeof (vm.model.product_img3) == 'object')
                    vm.model['product_img3'] = vm.model.product_img3[0].name;

                if (typeof (vm.model.product_img4) == 'string')
                    vm.model['product_img4'] = vm.model.product_img4.substring(vm.model.product_img4.lastIndexOf(email + '_') + email.length + 1);
                else if (typeof (vm.model.product_img4) == 'object')
                    vm.model['product_img4'] = vm.model.product_img4[0].name;
                

                if (typeof (vm.model.business_plan) == 'string')
                    vm.model['business_plan'] = vm.model.business_plan.substring(vm.model.business_plan.lastIndexOf(email + '_') + email.length + 1);
                else if (typeof (vm.model.business_plan) == 'object')
                    vm.model['business_plan'] = vm.model.business_plan[0].name;
                
                
                vm.model['email'] = $cookies.get('email'),
                acsCommonSvc.userInfo.data = vm.model;
                console.log(JSON.stringify(vm.model));
                $scope.$parent.vm.template.url = 'CreateStartup2.html';
          
            }
        }

    }

    createStartup1Ctrl.$inject = ['$scope', '$cookies', 'common', 'acsCommonSvc', 'CreateStartup1Svc', 'Upload', '$timeout'];

    angular.module('ag-content').controller(controllerId, createStartup1Ctrl);
})();