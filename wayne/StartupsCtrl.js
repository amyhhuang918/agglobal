﻿(function () {
    'use strict';
    var controllerId = 'StartupsCtrl';

    function startupsCtrl($scope, $cookies, common, acsCommonSvc, startupsSvc) {
        var vm = this;
        vm.controllerId = controllerId;
        vm.userType = acsCommonSvc.userType;
        vm.init = function () {
            startupsSvc.getAllStartups();
        }

       

    }

    startupsCtrl.$inject = ['$scope', '$cookies',  'common', 'acsCommonSvc', 'StartupsSvc'];

    angular.module('ag-content').controller(controllerId, startupsCtrl);
})();