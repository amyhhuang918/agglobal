﻿(function () {
    'use strict';

    var serviceId = 'ForgetPassword2Svc';

    function forgetPassword2Svc(common) {

        var service = {
            getPassword: getPassword
        };
        return service;
        
        function getPassword(request) {
            return common.sendHttpMessage('POST', '/Info/GetActionRef', request);
        };
    };

    forgetPassword2Svc.$inject = ['common'];

    angular.module('ag-content').factory(serviceId, forgetPassword2Svc);
})();