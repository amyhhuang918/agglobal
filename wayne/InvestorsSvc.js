﻿(function () {
    'use strict';

    var serviceId = 'InvestorsSvc';

    function investorsSvc(common) {

        var service = {
            getAllInvestors: getAllInvestors
        };
        return service;
        
        function getAllInvestors(request) {
            return common.sendHttpMessage('POST', '/agglobal/queryall_investor/', request);
        };
    };

    investorsSvc.$inject = ['common'];

    angular.module('ag-content').factory(serviceId, investorsSvc);
})();