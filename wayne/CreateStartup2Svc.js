﻿(function () {
    'use strict';

    var serviceId = 'CreateStartup2Svc';

    function createStartup2Svc(common) {

        var service = {
            registerCompany: registerCompany
        };
        return service;
        
        function registerCompany(request) {
            return common.sendHttpMessage('POST', '/agglobal/register_company/', request);
        };
    };

    createStartup2Svc.$inject = ['common'];

    angular.module('ag-content').factory(serviceId, createStartup2Svc);
})();