﻿(function () {
    'use strict';
    var controllerId = 'SignUpUserCtrl';

    function signUpUserCtrl($scope, $cookies, common, acsCommonSvc, signUpUserSvc) {
        var vm = this;
        vm.controllerId = controllerId;
        // vm.userType = $cookies.get('userType');
        vm.userType = acsCommonSvc.userType;
        $('#inputEmail').focus();
        vm.model = {
            email: '',
            password: ''
        }
        vm.registerUser = function (isValid) {
            if (isValid) {
                $.extend(acsCommonSvc.userInfo.data, vm.model);
                $cookies.put('email', vm.model.email);
                $scope.$parent.vm.template.url = 'SignUpUser2.html';
                $scope.$parent.vm.showHeader = true;
            }

        }

        vm.weiXin = function() {
            // alert('weiXin');
        }

        vm.home = function () {
            $scope.$parent.vm.showHeader = true;
            $scope.$parent.vm.home();
        }

        vm.linkedIn = function () {
            // $cookies.put('usertype', vm.userType);
            // var url = "/agglobal/login_linkedin/";
            var url = "/agglobal/login_linkedin/?userType=" + vm.userType;
            //$scope.$parent.vm.template.url = url;
            location.href = url;

        }

        vm.emailAvailable = true;
        vm.checkEmail = function () {
            if (vm.model.email == '' || vm.model.email == undefined) {
                return;
            }
            var request = {
                email: vm.model.email
            }
            var promise = signUpUserSvc.checkEmail(request);
          
            promise.then(
                function (response) {
                    if (response.availble) {                      
                        vm.emailAvailable = true;
                    } else {
                        vm.emailAvailable = false;
                        $('#inputEmail').focus();
                    }
                },
                function (reason) {
                    alert('failed to Get data');
                }
            );
        }

    }

    signUpUserCtrl.$inject = ['$scope', '$cookies', 'common', 'acsCommonSvc', 'SignUpUserSvc'];

    angular.module('ag-content').controller(controllerId, signUpUserCtrl);
})();