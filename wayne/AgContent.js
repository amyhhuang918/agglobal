﻿(function () {
    'use strict';
    angular.module('ag-content', [
        'common',
        'commonDirective',
        'ngMessages',
        'ag-content',
        'ngFileUpload',
        'ngCookies',
        'angularUtils.directives.dirPagination'
    ]);

})();

