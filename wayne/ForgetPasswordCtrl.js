﻿(function () {
    'use strict';
    var controllerId = 'ForgetPasswordCtrl';

    function forgetPasswordCtrl($scope, $cookies, common, acsCommonSvc, forgetPasswordSvc) {
        var vm = this;
        vm.controllerId = controllerId;


        vm.invalidEmail = false;
        vm.resetPassword = function () {
            var request =
            {
                email: vm.model.email
            };
            var promise = forgetPasswordSvc.getPassword(request);

            promise.then(
                function(response) {
                    if (response.success) {
                        $scope.$parent.vm.template.url = 'ForgetPassword2.html';
                    } else {
                        vm.invalidEmail = true;
                    }
                    
                },
                function(reason) {
                    alert('failed to Get data');
                }
            );
            
            
        }

    }

    forgetPasswordCtrl.$inject = ['$scope', '$cookies',  'common', 'acsCommonSvc', 'ForgetPasswordSvc'];

    angular.module('ag-content').controller(controllerId, forgetPasswordCtrl);
})();