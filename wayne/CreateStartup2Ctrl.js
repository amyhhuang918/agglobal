﻿(function () {
    'use strict';
    var controllerId = 'CreateStartup2Ctrl';

    function createStartup2Ctrl($scope, $cookies, common, acsCommonSvc, createStartup2Svc, upload, $timeout) {
        var vm = this;
        vm.controllerId = controllerId;
        
        vm.init = function () {
            vm.model = {};
          
            vm.members = [
                {
                    id: 0,
                    name: '',
                    position: '',
                    introduction: '',
                    file: [],
                    file_name: 'images/team-member-1.jpg'
                }
            ];

            vm.investors = [
                {
                    id: 0,
                    name: '',
                    position: '',
                    introduction: '',
                    file: [],
                    file_name: 'images/team-member-1.jpg'

                }
            ];

            vm.financing_experiences = [
                {
                    id: 0,
                    financing_title: '',
                    start_year: '',
                    start_month: '',
                    end_year: '',
                    end_month: '',
                    introduction: '',
                }
            ];
            if (acsCommonSvc.userInfo.data.financing_experiences) {
                vm.financing_experiences = acsCommonSvc.userInfo.data.financing_experiences;
            }
            if (acsCommonSvc.userInfo.data.investors) {
                vm.investors = acsCommonSvc.userInfo.data.investors;
            }
            if (acsCommonSvc.userInfo.data.members) {
                vm.members = acsCommonSvc.userInfo.data.members;
            }
        }


        vm.uploadFile = function (files) {
            if (files && files.length) {
                if (acsCommonSvc.userInfo.data.email == undefined) {
                    acsCommonSvc.userInfo.data.email = $cookies.get('email');
                }
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    return upload.upload({
                        url: '/agglobal/file_upload/',
                        fields: {
                            'email': acsCommonSvc.userInfo.data.email
                        },
                        file: file
                    });
                }
            }

        };

        vm.finish = function (isValid) {
            
            if (isValid) {               
                vm.model['financing_experiences'] = vm.financing_experiences;
                var email = $cookies.get('email').replace(/\"/g, "");
                $.each(vm.investors, function (index, value) {                   
                    if (!$.isEmptyObject(value.file)) {                        
                        value.file_name = '/media/' +  value.file[0].name;
                        value.file = '';
                    }
                    //else if (value.file_name) {
                    //    var email = $cookies.get('email').replace(/\"/g, "");
                    //    value.file_name = value.file_name.substring(value.file_name.lastIndexOf(email + '_') + email.length + 1);
                    //}
                });

                vm.model['investors'] = vm.investors;
                $.each(vm.members, function (index, value) {
                    if (!$.isEmptyObject(value.file)) {                        
                        value.file_name = '/media/'+ value.file[0].name;
                        value.file = '';
                    } 
                    // else if (value.file_name) {
                    //    var email = $cookies.get('email').replace(/\"/g, "");
                    //    value.file_name = value.file_name.substring(value.file_name.lastIndexOf(email + '_') + email.length + 1);
                    //}
                });
                vm.model['members'] = vm.members;
                $.extend(acsCommonSvc.userInfo.data, vm.model);
                console.log(JSON.stringify(acsCommonSvc.userInfo.data));
                var promise = createStartup2Svc.registerCompany(acsCommonSvc.userInfo.data);
                promise.then(
                function (response) {
                   if (response.success) {
                       //$scope.$parent.vm.template.url = 'Index.html';
                       location.href = "/agglobal/home"
                   } else {
                       alert(response.message);
                   }
                   },
                   function (reason) {
                       alert('failed to Get data');
                   }
                );
               
            }

        }


        vm.moreMembers = function () {
            var i = vm.members.length + 1;
            vm.members.push({ 'id': +i, 'file': '', file_name: 'images/team-member-1.jpg' });
           
        }

    

        vm.moreFinancingExperiences = function () {
            var i = vm.financing_experiences.length + 1;
            vm.financing_experiences.push({ 'id': +i, 'file': '' });                 
        }


        vm.fileSelected = function ($files, imageSrc) {
            if ($files[0] != undefined) {
                if ($files[0].type.indexOf('image') == -1) {
                    $('#imageTypeError').modal('show');
                    return false;
                }
                if ($files[0].size > 1000000) {
                    $('#imageSizeError').modal('show');
                    return false;
                }

                var promise = vm.uploadFile($files);
                promise.progress(function (evt) {

                }).success(function (data, status, headers, config) {
                    $timeout(function () {
                        if (imageSrc != null) {
                            $('#' + imageSrc).attr('src', '');
                            $('#' + imageSrc).attr('src', data.file_name);
                        }
                    });
                });
            }
        }



        vm.moreInvestors = function () {
            var i = vm.investors.length + 1;
            vm.investors.push({
                'id': +i, 'file': '', file_name: 'images/team-member-1.jpg'
            });
          
        }

    }

    createStartup2Ctrl.$inject = ['$scope', '$cookies', 'common', 'acsCommonSvc', 'CreateStartup2Svc', 'Upload', '$timeout'];

    angular.module('ag-content').controller(controllerId, createStartup2Ctrl);
})();