﻿(function () {
    'use strict';
    var controllerId = 'ProfileCtrl';

    function profileCtrl($scope, $cookies, common, acsCommonSvc, profileSvc) {
        var vm = this;
        vm.controllerId = controllerId;
        
        vm.home = function () {
            $scope.$parent.vm.showHeader = true;
            $scope.$parent.vm.home();
        }

        vm.init = function() {
            // vm.userType = acsCommonSvc.userInfo.userType;
            vm.userType = $cookies.get('userType');
            var request = {
                // email: acsCommonSvc.userInfo.data.email
                email: $cookies.get('email').replace(/['"]+/g, '')
        };

            // var test = JSON.parse('{"message": "user profile has been successfully retrieved", "data": {"user": 45, "city": "Concord", "first_name": "dough", "last_name": "John", "personal_profile": "", "key_expires": "2015-08-04T05:34:59.032432+00:00", "introduction": "intorduction to the john dough", "country": "ä¸­å›½", "industry": "æ•™è‚²", "profession": "United States", "domain": "", "business_experience": [{"start_month": null, "start_year": null, "company_name": "allphabet factory", "user": 9, "end_month": null, "location": "United States", "end_year": null, "attached_file": "/media/apple_green.jpg", "id": 8, "job_title": "programmer"}], "user_profile_id": 9, "profile_img": "/media/abluebird.jpg", "company_name": "the company doorman", "activation_key": "7d867079b54e1e96c2c0ff1a1df1be79e0c6ee16", "industryofconcern": "äº’è”ç½‘", "email": "whereis@sandiego.com", "job_title": ""}, "success": true}');

            // vm.model = $.extend(vm.model, test.data);
            var promise;
            if (vm.userType == 'i')
                promise = profileSvc.getInvestorProfile(request);
            else if (vm.userType == 'e')
                promise = profileSvc.getUserProfile(request);

            promise.then(
                function(response) {
                    if (response.success == false) {
                        alert(response.message);
                        return;
                    }

                    vm.model = $.extend(vm.model, response.data);

                    console.log(JSON.stringify(vm.model));
                },
                function(reason) {
                    alert('failed to Get data');
                }
             );


        }

       

    


      
    }

    profileCtrl.$inject = ['$scope', '$cookies',  'common', 'acsCommonSvc', 'ProfileSvc'];
    angular.module('ag-content').controller(controllerId, profileCtrl);
})();