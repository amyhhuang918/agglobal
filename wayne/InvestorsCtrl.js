﻿(function () {
    'use strict';
    var controllerId = 'InvestorsCtrl';

    function investorsCtrl($scope, $cookies, common, acsCommonSvc, investorsSvc) {
        var vm = this;
        vm.controllerId = controllerId;
        vm.userType = acsCommonSvc.userType;

        vm.currentPage = 1;
        vm.pageSize = 5;
        vm.init = function () {
            var promise = investorsSvc.getAllInvestors();
            promise.then(
                             function (response) {
                                 if (response.success == false) {
                                     alert(response.message);
                                     return;
                                 }

                                 vm.investors = response.data;
                                 JSON.stringify(vm.investors);
                             },
                             function (reason) {
                                 alert('failed to Get data');
                             }
                         );
        }

        vm.pageChangeHandler = function (num) {

            console.log('Change to ' + num);

        };

       

    }

    investorsCtrl.$inject = ['$scope', '$cookies', 'common', 'acsCommonSvc', 'InvestorsSvc'];

    angular.module('ag-content').controller(controllerId, investorsCtrl);
})();