﻿(function () {
    'use strict';

    var serviceId = 'ForgetPasswordSvc';

    function forgetPasswordSvc(common) {

        var service = {
            getPassword: getPassword
        };
        return service;
        
        function getPassword(request) {
            return common.sendHttpMessage('POST', '/agglobal/password_reset/', request);
        };
    };

    forgetPasswordSvc.$inject = ['common'];

    angular.module('ag-content').factory(serviceId, forgetPasswordSvc);
})();