﻿(function () {
    'use strict';

    var serviceId = 'IndexSvc';

    function indexSvc(common) {

        var service = {
            signIn: signIn
        };
        return service;
        
        function signIn(request) {
            console.log(JSON.stringify(request));
            return common.sendHttpMessage('POST', '/agglobal/login/', request);
        };
    };

    indexSvc.$inject = ['common'];

    angular.module('ag-content').factory(serviceId, indexSvc);
})();