﻿(function () {
    'use strict';

    var serviceId = 'acsCommonSvc';
    function acsCommonSvc(common) {
        var service = {
            userInfo: {
                data: {},
                userType: ''
            },
        };
        
        //function getHolidays(request) {
        //    return common.sendHttpMessage('POST', '/Info/GetHolidays', request);
        //};



        return service;
    }

    acsCommonSvc.$inject = ['common'];

    angular.module('app-ag').factory(serviceId, acsCommonSvc);
})();