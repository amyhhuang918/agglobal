﻿(function () {
    'use strict';

    var serviceId = 'CreateStartup1Svc';

    function createStartup1Svc(common) {

        var service = {
            createStartup: createStartup,
            getStartup: getStartup
        };
        return service;
        
        function createStartup(request) {
            return common.sendHttpMessage('POST', '/agglobal/register_company/', request);
        };

        function getStartup(request) {
            return common.sendHttpMessage('POST', '/agglobal/query_company/', request);
        }
    };

    createStartup1Svc.$inject = ['common'];

    angular.module('ag-content').factory(serviceId, createStartup1Svc);
})();