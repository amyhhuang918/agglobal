﻿(function () {
    'use strict';

    var serviceId = 'SignUpUserSvc';

    function signUpUserSvc(common) {

        var service = {
            checkEmail: checkEmail
        };
        return service;
        
        function checkEmail(request) {
            return common.sendHttpMessage('POST', '/agglobal/register_check/', request);
        };
    };

    signUpUserSvc.$inject = ['common'];

    angular.module('ag-content').factory(serviceId, signUpUserSvc);
})();