﻿(function () {
    'use strict';

    var serviceId = 'StartupsSvc';

    function startupsSvc(common) {

        var service = {
            getAllStartups: getAllStartups
        };
        return service;
        
        function getAllStartups(request) {
            return common.sendHttpMessage('POST', '/agglobal/queryall_user/', request);
        };
    };

    startupsSvc.$inject = ['common'];

    angular.module('ag-content').factory(serviceId, startupsSvc);
})();