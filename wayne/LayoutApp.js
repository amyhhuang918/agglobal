﻿(function () {
    'use strict';
    angular.module('app-ag', [
        'common',
        'commonDirective',
        'ngMessages',
        'ag-content',
        'ngFileUpload',
        'ngCookies',
        'angularUtils.directives.dirPagination'
    ]);

})();

