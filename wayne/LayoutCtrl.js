﻿(function () {
    'use strict';
    var controllerId = 'LayoutCtrl';
    function layoutCtrl($scope, $cookies,  common, acsCommonSvc, layoutSvc) {
        var vm = this;
        vm.controllerId = controllerId;


        vm.createStartup = function() {
            vm.template.url = 'CreateStartup1.html';
        }

        vm.english = function () {
            alert('English');
        }

        vm.home = function () {
            vm.template.url = 'Index.html';
          // vm.template.url = 'Index_login.html';
        }


        vm.ventureProject = function () {
            if (vm.isLogin()) {
                alert('You are login');

            } else {
                $('#loginModal').modal('show').attr('aria-hidden', true);
            }
        }

        vm.forgetPassword = function() {
            vm.template.url = 'ForgetPassword.html';
        }

        vm.investor = function () {
            vm.template.url = 'ProfileInvestor.html';
        }

        vm.init = function () {
            vm.showHeader = true;
            vm.template = {
               'url': 'Index.html'
               // 'url': 'Index_login.html'
            };

            vm.numOfEmails = $cookies.get('totalemails');

            var from = $cookies.get('from');

            //alert(from);
            if (from == 'linkedin') {
                vm.template.url = 'SignUpUser2.html';
            }
            if (from == 'CreateStartup1') {
                vm.template.url = 'CreateStartup1.html';
                //vm.controller.url = '/Components/CreateStartup1Ctrl.js';
            }
            
            if (vm.isLogin()) {
                
                vm.first_name = vm.parseUTF($cookies.get('first_name').replace(/['"]+/g, '').replace(/\\\\/g, '\\'));
                vm.last_name = vm.parseUTF($cookies.get('last_name').replace(/['"]+/g, '').replace(/\\\\/g, '\\'));
                vm.profile_img = $cookies.get('profile_img').replace(/['"]+/g, '');
                //location.href="/agglobal/home"
            }
            if (window.location.search.indexOf('promptlogin=true')>=0) {
                $('#login').modal('toggle');
            }
            if (window.location.search.indexOf('promptregister=true')>=0) {
                $('#loginModal').modal('toggle');
            }
        }

        vm.parseUTF = function(myString) {
            // var x = "\\xE4\\xBA\\x8E\\xE5\\xB0\\x8F\\xE4\\xBC\\x9F";
            var r = /\\x([\d\w]{2})/gi;
            myString = myString.replace(r, function (match, grp) {
                return String.fromCharCode(parseInt(grp, 16));
            });
            return utf8.decode(myString);            
        }

        vm.weiXin = function () {
            // alert('weiXin');
        }

       
        vm.linkedIn = function () {            
            var url = "/agglobal/login_linkedin/";
            location.href = url;
        }

        vm.registerInvestor = function () {
            //$('#loginModal').modal('hide').attr('aria-hidden', false);            
            vm.template.url = 'SignUpUser.html';            
            acsCommonSvc.userType = 'i';
            vm.showHeader = false;
            
        }

        vm.registerEntrepreneur = function() {
            //$('#loginModal').modal('hide').attr('aria-hidden', false);
            acsCommonSvc.userType = 'e';
            vm.template.url = 'SignUpUser.html';
            vm.showHeader = false;
            $('#slider-banner').revpause();


        }

        vm.ventureProject = function() {
            //if (vm.isLogin()) {
                vm.template.url = 'Startups.html';
            //} else {
            //    $('#loginModal').modal('show').attr('aria-hidden', true);
            //}
        }

        vm.investor = function() {
            //if (vm.isLogin()) {
                vm.template.url = 'Investors.html';
            //} else {
            //    $('#loginModal').modal('show').attr('aria-hidden', true);
            //}
        }

        vm.profile= function() {
            if ($cookies.get('userType') == "i") {
                location.href = "/agglobal/viewInvProfile/" + $cookies.get('userid');
            } else {
                location.href = '/agglobal/viewUserProfile/' + $cookies.get('userid');
            }
        }

        vm.stopSlide = function() {
            $(".slider-banner-container").finish();
        }

        vm.signIn = function(isValid) {
            if (isValid) {

                var promise = layoutSvc.signIn(vm.model);

                promise.then(
                    function (response) {
                        if (response.success == false) {                        
                            vm.loginErrorMessage = response.message;
                            return;
                        } else {
                            $('#login').modal('hide');
                            $(".modal-backdrop").remove();                            
                            vm.first_name = response.first_name;
                            vm.last_name = response.last_name;
                            vm.profile_img = response.profile_img;
                            console.log(acsCommonSvc.userType);
                            location.href = "/agglobal/home"                            
                        }
                    },
                    function (reason) {
                        alert('failed to Get data');

                    }
                );
            }
        }

        vm.logout = function () {
            location.href = '/agglobal/logout/';
        };

        vm.isLogin = function () {
            if ($cookies.get('keyagg') == 'successful_login') {
                return true;
            }                        
            return false;
        };
    
        
      
    }

    layoutCtrl.$inject = ['$scope', '$cookies',  'common', 'acsCommonSvc', 'LayoutSvc'];

    angular.module('app-ag').controller(controllerId, layoutCtrl);
})();
