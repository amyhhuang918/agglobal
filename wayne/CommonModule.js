﻿(function () {
    'use strict';

    var commonModule = angular.module('common', ['ngCookies']);

    commonModule.provider('commonConfig', function() {
        this.config = {
             withCredentials: true
        };

        this.$get = function() {
            return {
                config: this.config
            };
        };
    });

    commonModule.factory('common',
        ['$q', '$http', '$rootScope', '$timeout', '$location', '$window', 'commonConfig',  '$cookies', common]
    );
    
    function common($q, $http, $rootScope, $timeout, $location, $window, commonConfig,  $cookies) {
        var service = {
            $broadcast: $broadcast,
            $q: $q,
            $http: $http,
            $timeout: $timeout,
            sendHttpMessage: sendHttpMessage,
            logout: logout,
            isNumber: isNumber,
            textComtains: textContains,
            ngGridDoubleClick: ngGridDoubleClick
        };

        return service;
        //DoubleClick row plugin
        function ngGridDoubleClick() {
            var self = this;
            self.$scope = null;
            self.myGrid = null;

            // The init method gets called during the ng-grid directive execution.
            self.init = function (scope, grid, services) {
                // The directive passes in the grid scope and the grid object which
                // we will want to save for manipulation later.
                self.$scope = scope;
                self.myGrid = grid;
                // In this example we want to assign grid events.
                self.assignEvents();
            };
            self.assignEvents = function () {
                // Here we set the double-click event handler to the header container.
                self.myGrid.$viewport.on('dblclick', self.onDoubleClick);
            };
            // double-click function
            self.onDoubleClick = function (event) {
                self.myGrid.config.dblClickFn(self.$scope.selectedItems[0]);
            };
        }

        function sendHttpMessage(method, url, payload) {
            var deferred = $q.defer();
            
            if(typeof payload === 'undefined') {
                $http({
                    method: method,
                    url: url,
                    withCredentials: true
                    //headers: headers
                })
                .success(function (data, status, headers, config) {                                        
                         deferred.resolve(data);
                 })
                .error(function(data) {
                     deferred.reject(data);
                });
            } else {
                $http({
                    method: method,
                    url: url,
                    data: payload,
                    withCredentials: true
                    //headers: headers
                })
                .success(function (data, status, headers, config) {                    
                    deferred.resolve(data);                    
                    })
                .error(function(data) {
                    deferred.reject(data);
                    handleErrors(data);
                });
            }

            return deferred.promise;
        }

        function updateErrors(errors) {
            //$scope.errors.formErrors = {};
            //$scope.errors.pageError = "";

            if (errors) {
                for (var i = 0; i < errors.length; i++) {
                    //$scope.errors.formErrors[errors[i].Key] = errors[i].Message;
                }
            }
        }

        function handleErrors(data) {
            if (data.Errors) {
                updateErrors(data.Errors);
            } else if (data.message) {
                //$scope.errors.pageError = data.message;
            } else if (data) {
                //$scope.errors.pageError = data;
            } else {
                //$scope.errors.pageError = "An unexpected error has occurred, please try again later.";
            }
        };
        
        function logout() {
            
            var host = $location.host(),
                port = $location.port(),
                logoutPath = host + ':' + port;
            
            location.href = '/login';
        }

        function $broadcast() {
            return $rootScope.$broadcast.apply($rootScope, arguments);
        }
        
        function isNumber(val) {
            // negative or positive
            return /^[-]?\d+$/.test(val);
        }

        function textContains(text, searchText) {
            return text && -1 !== text.toLowerCase().indexOf(searchText.toLowerCase());
        }


    };
})();