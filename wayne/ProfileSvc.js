﻿(function () {
    'use strict';

    var serviceId = 'ProfileSvc';

    function profileSvc(common) {

        var service = {
            getInvestorProfile: getInvestorProfile,
            getUserProfile: getUserProfile,
  
        };
        return service;
        
        function getInvestorProfile(request) {
            console.log(JSON.stringify(request));
            return common.sendHttpMessage('POST', '/agglobal/query_investor/', request);
        };

        function getUserProfile(request) {
            console.log(JSON.stringify(request));
            return common.sendHttpMessage('POST', '/agglobal/query_user/', request);
        };

    };

    profileSvc.$inject = ['common'];

    angular.module('ag-content').factory(serviceId, profileSvc);
})();