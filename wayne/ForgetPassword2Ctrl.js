﻿(function () {
    'use strict';
    var controllerId = 'ForgetPassword2Ctrl';

    function forgetPassword2Ctrl($scope, $cookies,  common, acsCommonSvc, forgetPassword2Svc) {
        var vm = this;
        vm.controllerId = controllerId;
       
      

        vm.getPassword = function () {
            var request =
            {
                email: vm.model.email
            };
            var promise = forgetPassword2Svc.getPassword(request);
            $scope.$parent.vm.template.url = 'ForgetPassword22.html';
        }

    }

    forgetPassword2Ctrl.$inject = ['$scope', '$cookies',  'common', 'acsCommonSvc', 'ForgetPassword2Svc'];

    angular.module('ag-content').controller(controllerId, forgetPassword2Ctrl);
})();