﻿(function () {
    'use strict';

    var serviceId = 'SignUpUser2Svc';

    function signUpUser2Svc(common) {

        var service = {
            registerUser: registerUser,
            registerInvestor: registerInvestor
        };
        return service;
        
        function registerUser(request) {            
            console.log(JSON.stringify(request));
            return common.sendHttpMessage('POST', '/agglobal/register_user/', request);
        };

        function registerInvestor(request) {
            console.log(JSON.stringify(request));
            return common.sendHttpMessage('POST', '/agglobal/register_investor/', request);
        };
    };

    signUpUser2Svc.$inject = ['common'];

    angular.module('ag-content').factory(serviceId, signUpUser2Svc);
})();