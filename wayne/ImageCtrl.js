﻿(function () {
    'use strict';
    var controllerId = 'ImageCtrl';

    function imageCtrl($scope, acsCommonSvc) {
        var ic = this;
        ic.controllerId = controllerId;

        $scope.$watch('ic.investment_experience', function () {
            if (ic.investment_experience != 'undefined' && ic.investment_experience != null && ic.investment_experience.files != null)
                ic.uploadFile(ic.investment_experience.files);
        });

        $scope.$watch('$scope.business_experience', function () {
            if (ic.business_experience != 'undefined' && $scope.business_experience != null && $scope.business_experience.files != null)
                ic.uploadFile($scope.business_experience.files);

        });
        
        ic.uploadFile = function (files) {
            if (files && files.length) {
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    return upload.upload({
                        url: 'http://ai22.mooo.com:8000/agglobal/file_upload/',
                        fields: {
                            'email': acsCommonSvc.userInfo.data.email
                        },
                        file: file
                    }).progress(function (evt) {
                        // var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                        // ic.log = 'progress: ' + progressPercentage + '% ';                       
                    }).success(function (data, status, headers, config) {
                      
                    });
                }

            }

        };
        
    }


    imageCtrl.$inject = ['$scope', 'acsCommonSvc'];

    angular.module('ag-content').controller(controllerId, imageCtrl);
})();
