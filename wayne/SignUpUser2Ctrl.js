﻿(function () {
    'use strict';
    var controllerId = 'SignUpUser2Ctrl';

    function signUpUser2Ctrl($scope, $cookies, $timeout, common, acsCommonSvc, signUpUser2Svc, upload) {
        var vm = this;
        vm.controllerId = controllerId;

        vm.init = function () {
            vm.userType = acsCommonSvc.userType;
            if (vm.userType == undefined) {
                vm.userType = $cookies.get('userType');
            }
            vm.userImage = 'images/team-member-1.jpg';
            vm.business_experiences = [
                {
                    id: 1,
                    company_name: '',
                    job_title: '',
                    location: '',
                    start_year: '',
                    start_month: '',
                    end_year_: '',
                    end_month: '',
                    description: '',
                    files: {},
                    attached_file: ''
                }
            ];

            vm.investment_experiences = [
                {
                    id: 1,
                    project_name: '',
                    invest_year: '',
                    invest_month: '',
                    invested_status: '',
                    project_detail: '',
                    files: {},
                    attached_project_file: ''
                }
            ];
        }


        vm.findError = false;
        vm.registerUser = function (isValid) {
            if (isValid) {
                $.extend(vm.model, acsCommonSvc.userInfo.data);
                var promise;
                if (vm.userType == 'e') {
                    vm.model['business_experiences'] = vm.business_experiences;
                    vm.model['profile_img'] = vm.userImage;
                    $.each(vm.model.business_experiences, function(index, value) {
                        if (!$.isEmptyObject(value.files)) {
                            value.attached_file = value.files[0].name;
                            value.files = '';
                        }
                    });
                    console.log(JSON.stringify(vm.model));
                    promise = signUpUser2Svc.registerUser(vm.model);
                }

                if (vm.userType == 'i') {
                    vm.model['investment_experiences'] = vm.investment_experiences;
                    vm.model['profile_img'] = vm.userImage;
                    $.each(vm.model.investment_experiences, function(index, value) {
                        if (!$.isEmptyObject(value.files)) {
                            //var iPromise = vm.uploadFile(value.files, false, value.id);
                            //iPromise.progress(function(evt) {
                            //}).success(function(data, status, headers, config) {
                            //    $timeout(function() {
                            //        // value.attached_project_file = data.file_name;
                            //    });
                            //});
                            value.attached_project_file = value.files[0].name;
                            value.files = '';
                        }

                    });
                    console.log(JSON.stringify(vm.model));
                    promise = signUpUser2Svc.registerInvestor(vm.model);
                }


                //console.log(vm.model);

                promise.then(
                    function(response) {
                        if (response.success == false) {
                            alert(response.message);
                            return;
                        }

                        $scope.$parent.vm.template.url = 'SignupSuccess.html';
                    },
                    function(reason) {
                        alert('failed to Get data');
                    }
                );
            } else {
                vm.findError = true;
            }
        }
        vm.fileSelected = function ($files, $file, $event, $rejectedFiles) {
            if ($files[0] != undefined) {
                if ($files[0].type.indexOf('image') == -1) {
                    $('#imageTypeError').modal('show');                    
                    return false;
                }
                if ($files[0].size > 1000000) {
                    $('#imageSizeError').modal('show');
                    return false;
                }

                var iPromise = vm.uploadFile($files);
                iPromise.progress(function(evt) {
                }).success(function(data, status, headers, config) {
                    $timeout(function() {
                        
                    });
                });
            }
        }

        vm.profileSelected = function ($files, $file, $event, $rejectedFiles) {
            if ($files[0] != undefined) {
                if ($files[0].type.indexOf('image') == -1) {
                    $('#imageTypeError').modal('show');
                    $files = {};
                    return false;
                }
                if ($files[0].size > 1000000) {
                    $('#imageSizeError').modal('show');
                    $files = {};
                    return false;
                }

                var promise = vm.uploadFile($files);
                promise.progress(function (evt) {

                }).success(function (data, status, headers, config) {
                    $timeout(function () {
                        vm.userImage = '';
                        vm.userImage = data.file_name;
                    });
                });
            }
        }

        //$scope.$watch('vm.model.profile_img', function () {
        //    if (vm.model != null && vm.model.profile_img != null) {

        //        var promise = vm.uploadFile(vm.model.profile_img, true, 0);        
        //        promise.progress(function (evt) {
                    
        //        }).success(function (data, status, headers, config) {
        //            $timeout(function () {
        //                vm.userImage = '';
        //                vm.userImage = data.file_name;
        //            });
        //        });
        //    }
        //});

        vm.uploadFile = function (files) {
            if (files && files.length) {
                if (acsCommonSvc.userInfo.data.email == undefined) {
                    acsCommonSvc.userInfo.data.email = $cookies.get('email');
                }
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    return upload.upload({
                        url: '/agglobal/file_upload/',
                        fields: {
                            'email': acsCommonSvc.userInfo.data.email
                        },
                        file: file
                    });
                }
            }

        };

        vm.moreBusinessExperience = function () {
            var newItemNo = vm.business_experiences.length + 1;
            vm.business_experiences.push({ 'id': +newItemNo });
        }

        vm.moreInvestmentExperience = function () {
            var newItemNo = vm.investment_experiences.length + 1;
            vm.investment_experiences.push({ 'id': +newItemNo });
        }
    }

    signUpUser2Ctrl.$inject = ['$scope', '$cookies', '$timeout', 'common', 'acsCommonSvc', 'SignUpUser2Svc', 'Upload'];
    angular.module('ag-content').controller(controllerId, signUpUser2Ctrl);
})();