# coding: utf-8
from django.forms.models import model_to_dict
from django.shortcuts import render
from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from django.http import HttpResponse, HttpResponseServerError
from django.http import HttpResponseRedirect
from django.conf import settings
from django.core import serializers
from django.shortcuts import render_to_response
from django.shortcuts import redirect
from django.core.servers.basehttp import FileWrapper
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout as auth_logout
from django.contrib.messages.api import get_messages
from contextlib import closing
from zipfile import ZipFile, ZIP_DEFLATED
import urllib
import urllib2
import tempfile
import shutil
from urlparse import urlparse
import datetime, random, sha
from datetime import timedelta
from django.core.mail import send_mail
from django.template import RequestContext
from md5 import md5
from string import whitespace
import time
from django.utils import timezone
import subprocess
import os
import ftplib
from os import listdir
import traceback
from agglobal.models import *
from agglobal.forms import *
import json
import datetime
import logging
import sys
from dateutil.parser import parse
import ntpath
import mysite.settings

logger = logging.getLogger(__name__)



############  utility functions ###############

def make_fn( filename):
    return   ntpath.basename(filename).replace(' ','_')

def make_ufn( email, filename):
    print '============= in make_ufn: ' + email + ' : ' + filename
    if email in filename:
       new_fn = ntpath.basename(filename).replace(' ','_')
    else:
      new_fn = email + '_' + ntpath.basename(filename).replace(' ','_')

    print new_fn
    return  new_fn

def make_url( email, filename):
    print '============= in make_url: ' + email + ' : ' + filename

    if email in filename:
      new_url = os.path.join( settings.MEDIA_URL,  ntpath.basename(filename).replace(' ','_') )
    else:
      new_url = os.path.join( settings.MEDIA_URL, email +  '_' +  ntpath.basename(filename).replace(' ','_') )
    print new_url
    return new_url

def rename_file( file1, file2):
     os.rename( os.path.join(settings.MEDIA_ROOT, make_fn(file1)), os.path.join(settings.MEDIA_ROOT,make_fn(file2)) )


def getUserEmailCount( userid ):
    unreadcnt = Message.objects.filter( receiver=userid, read='F').count()
    return unreadcnt


############  views  ############


@csrf_exempt
def login_user(request):
    logger.info("user logging to angie global website")

    error = ''
    username = password = ''

    print request.body

    print "entering user login page"

    print request.method

    success = True
    if request.method == 'GET':
        response =  HttpResponseRedirect('/static/Layout.html?promptlogin=true')
        return response

    #username ='whereis@sandiego.com'
    #password ='mrqwerty'

    mydata = json.loads(str(request.body))
    username = mydata['email']
    password = mydata['password']
    print username
    print password
    usertype = ''

    user = authenticate(username=username, password=password)

    if user is not None:
        if user.is_active:
            print "user is active"
            login(request, user)
            message =   'Your have succesfully logged in.'
            u_usr = user.userprofile_set.all()
            u_inv = user.investorprofile_set.all()

            if u_inv.exists():
               usertype = 'i'
               firstname = u_inv[0].first_name
               lastname = u_inv[0].last_name
               profileimg = u_inv[0].profile_img.name
               print "this user is investor"
 
            if u_usr.exists():
               usertype = 'e'
               firstname = u_usr[0].first_name
               lastname = u_usr[0].last_name
               profileimg = u_usr[0].profile_img.name
               print "this user is normal user"

        else:
            print 'Your account is not active, please contact the site'
            message = 'Your account is not active, please contact the site'
            success = False
            auth_logout(request)
    else:
        message = 'Your username and/or password are incorrect.'
        print  'Your username and/or password are incorrect.'
        success = False
        auth_logout(request)


    return_json = {}
    return_json['success'] = success
    return_json['message'] = message
    if (success):
       return_json['userType'] = usertype
       return_json['first_name'] = firstname
       return_json['last_name'] = lastname
       return_json['profile_img'] = profileimg
       return_json['totalemails'] = getUserEmailCount(user.id)

    json_str = json.dumps(return_json)
    print json_str
    response =  HttpResponse(json_str, content_type='application/json')
    if (success):
       response.set_cookie('userid',user.id)
       response.set_cookie('keyagg','successful_login')
       response.set_cookie('userType',usertype)
       response.set_cookie('email',user.email)
       response.set_cookie('totalemails',getUserEmailCount(user.id))

       #print "first name: " + firstname
       #print "last name: " + lastname
       response.set_cookie('first_name',firstname.encode('utf8').encode('quoted-printable').replace('=','\\x'))
       response.set_cookie('last_name',lastname.encode('utf8').encode('quoted-printable').replace('=','\\x'))
       response.set_cookie('profile_img',profileimg)

    return response


    
@csrf_exempt
def register_check(request):
    print "in register check loop"
    print request.body

    mydata = json.loads(str(request.body))
    email_addr = mydata["email"]
    #email_addr = 'whereis@sandiego.com'
    success = True

    try:
        if User.objects.filter(email=email_addr).exists():
           success=False
           message="Email address has already been used" 
        else:
           success=True
           message="Email address is available for registration"
    except:
        success = False
        print sys.exc_info()
        message = "error occurred " + str(sys.exc_info()[0])

    return_json = {} 
    return_json['availble'] = success
    return_json['message'] = message
    json_str = json.dumps(return_json)
    response =  HttpResponse(json_str, content_type='application/json')
    return response

@login_required
@csrf_exempt
def password_reset(request):
    print "in password reset loop"
    print request.body
    success = True
    message = "You have successfully updated password"
    mydata = json.loads(str(request.body))
    currentp = mydata["cpassword"]
    newp1 = mydata['password1']
    newp2 = mydata['password2']

    if not (request.user.check_password(currentp)):
        success = False
        message = "Old password doesn't match"
    elif (newp1 != newp2):
        success = False
        message = "New password doesn't match"
    else:
        request.user.set_password(newp1)
        request.user.save()


    return_json = {}
    return_json['success'] = success
    return_json['message'] = message
    json_str = json.dumps(return_json)
    response =  HttpResponse(json_str, content_type='application/json')
    return response

 
def psssucess(request):
    return_json=create_generic_headerjson(request.user)
    return render_to_response('agglobal/pass-reset-success.html', return_json, context_instance=RequestContext(request))


# view to save user type to session data and call social auth
@csrf_exempt
def login_linkedin(request):

    #mydata = json.loads(str(request.body))
    #mydata = json.loads( '{ "userType": "e"}')
    #print mydata  
    print "entering login_linkedin"   

    if request.GET.get('userType'):
         userType = request.GET['userType']
    else:
         userType = 'None'
    print "user type is: " + userType

    request.session['userType'] = userType

    return  HttpResponseRedirect('/login/linkedin')


# Where the user is redirected after successful authentication
@login_required
def complete(request): 
    ### need to change username to email for linkedin created user
    print "entering linkedin complete routine" 

    userType = request.session.get('userType', 'Not found')
    user = request.user
    user.username=user.email
    user.save()

    print "user email: " + user.email
    print "first name: " + user.first_name
    print "last name: " + user.last_name
    print "user type: " + userType 

   
    response =  HttpResponseRedirect('/static/Layout.html')

    u_user = user.userprofile_set.all()
    u_inv = user.investorprofile_set.all()
   
    exptime = datetime.datetime.now() + timedelta(seconds=15) 
    if u_inv.exists():
        i_obj = InvestorProfile.objects.get( user = user )
        response.set_cookie('from','',  max_age=15, expires=exptime)
        response.set_cookie('redirect','N')    
        response.set_cookie('keyagg','successful_login')
        response.set_cookie('profile_img', i_obj.profile_img.name)
        response.set_cookie('userType','i')
        print "this linkedin user is investor"
        print "image file name: " +  i_obj.profile_img.name
    elif u_user.exists():
        u_obj = UserProfile.objects.get( user = user )
        response.set_cookie('from','',  max_age=15, expires=exptime)
        response.set_cookie('redirect','N')
        response.set_cookie('keyagg','successful_login')
        response.set_cookie('profile_img',u_obj.profile_img.name)
        response.set_cookie('userType','e')
        print "this linkedin user is normal user"
        print "image file name: " + u_obj.profile_img.name
    else: 
        response.set_cookie('from','linkedin',  max_age=15, expires=exptime)
        response.set_cookie('redirect','Y')
        response.set_cookie('userType',userType)
        print "this linkedin user type is unknown - NONE"
        print "this user needs to complete detailed profile"

    #response.cookies['from']['expires'] = datetime.datetime.now() + timedelta(seconds=5)
    response.set_cookie('email',user.email)
    response.set_cookie('first_name',user.first_name.encode('quoted-printable').replace('=','\\x'))
    print json.dumps(user.first_name)
    response.set_cookie('last_name',user.last_name.encode('quoted-printable').replace('=','\\x'))
    print json.dumps(user.last_name)
    return response

@csrf_exempt
@login_required
def create_startup(request):
    print '###### entering create_startup ######'
    exptime = datetime.datetime.now() + timedelta(seconds=15) 
    response =  HttpResponseRedirect('/static/Layout.html')
    response.set_cookie('from','CreateStartup1' , max_age=15, expires=exptime)
    response.set_cookie('totalemails', getUserEmailCount(request.user.id) , max_age=15, expires=exptime)

    return response
 
 
# Since the logged in user is a normal Django user instance, we logout the user the natural Django way:
@csrf_exempt
def logout(request):
    """Logs out user"""
    auth_logout(request)
    return HttpResponseRedirect('/')

def error(request):
    """Error view"""
    messages = get_messages(request)
    print messages
    return render_to_response('registration/error.html', {'messages': messages}, RequestContext(request))

def create_owl_data(email, dict):
    print email

    owljson = {}
    owljson["owl"] = []
    print "x"
    if (dict["product_img1"]):
        print dict["product_img1"]
        owljson["owl"].append({"item": "<div class='overlay-container margin-top-clear'><img src='" + dict["product_img1"] +"'><a href='" + dict["product_img1"] + "'></a></div>"})
    print owljson
    if (dict["product_img2"]):
        owljson["owl"].append({"item": "<div class='overlay-container margin-top-clear'><img src='" + dict["product_img2"] +"'><a href='" + dict["product_img2"] + "'></a></div>"})

    if (dict["product_img3"]):
        owljson["owl"].append({"item": "<div class='overlay-container margin-top-clear'><img src='" + dict["product_img3"] +"'><a href='" + dict["product_img3"] + "'></a></div>"})

    if (dict["product_img4"]):
        owljson["owl"].append({"item": "<div class='overlay-container margin-top-clear'><img src='" + dict["product_img4"] +"'><a href='" + dict["product_img4"] + "'></a></div>"})

    filename0 = make_ufn(email, "owldata.json")
    filename = os.path.join(settings.MEDIA_ROOT,  filename0)

    try:
        print 'writing to file ' + filename
        with open(str(filename), 'w') as owlfile:
            owlfile.write(json.dumps(owljson))
            owlfile.close()
    except:
        traceback.print_exc()
    return os.path.join(settings.MEDIA_URL, filename0)

@csrf_exempt
def file_upload(request):
    #esponseRedirect('/test/Layout.html')

    success = True
    #print request.body
    print request.POST
    #mydata = json.loads(str(request.body))
    print "in file_upload"
    print request.POST['email']

    for filename, file in request.FILES.items():
        print request.FILES[filename].name

    if request.user.is_authenticated():
        email = request.user.email
    else:
        email = request.POST['email']

    if request.method == 'POST':

        for filename, file in request.FILES.items():
            print request.FILES[filename].name

        newdoc = Document(
           docfile = request.FILES['file'],
           email = email,
           seq = request.POST.get('seq'),
           created = datetime.datetime.now() 
        )
        #newdoc.filename = newdoc.email + '_' + ntpath.basename(newdoc.docfile.name).replace(' ','_')
        newdoc.filename = make_ufn(newdoc.email, newdoc.docfile.name) 
        newdoc.save()
        print newdoc.email
        print newdoc.filename
        #os.rename(os.path.join(settings.MEDIA_ROOT, ntpath.basename(newdoc.docfile.name).replace(' ','_')), os.path.join(settings.MEDIA_ROOT, newdoc.filename) )
        rename_file(newdoc.docfile.name, newdoc.filename)
    else:
         message = " request was not post type"
         success = False
    
    return_json = {}
    if (success):
        message = "please redirect to user reigstration confirmation page" 
        return_json['file_name']= os.path.join(settings.MEDIA_URL,  newdoc.filename)
    return_json['success'] = success    
    return_json['message'] = message
    json_str = json.dumps(return_json)
    return HttpResponse(json_str, content_type='application/json')

@csrf_exempt
def file_upload2(request):
    #esponseRedirect('/test/Layout.html')

    success = True
    #mydata = json.loads(str(request.body))
    print "in file_upload2"

    print request.FILES
    print request.COOKIES
    email = request.user.email
    print email
    try:
	    if request.method == 'POST':

		for filename, file in request.FILES.items():
		    print request.FILES[filename].name

		    newdoc = Document(
		       docfile = file,
		       email = email.replace('"',''),
		       created = datetime.datetime.now()
		    )
		    #newdoc.filename = newdoc.email + '_' + ntpath.basename(newdoc.docfile.name).replace(' ','_')
		    newdoc.filename = make_ufn(newdoc.email, newdoc.docfile.name)
		    newdoc.save()
		    rename_file(newdoc.docfile.name, newdoc.filename)
	    else:
		 message = " request was not post type"
		 success = False
    except:
	    traceback.print_exc()
            success = False
            message = " error during the file upload" 

    return_json = {}
    if (success):
        message = "file is successfully uploaded"
        return_json['file_name']= os.path.join(settings.MEDIA_URL,  newdoc.filename)

    return_json['success'] = success
    return_json['message'] = message
    json_str = json.dumps(return_json)
    return HttpResponse(json_str, content_type='application/json')

@csrf_exempt
def file_upload4(request):
    #esponseRedirect('/test/Layout.html')

    success = True
    #mydata = json.loads(str(request.body))
    print "in file_upload4"

    email = request.user.email
    print "email is " + email
    #email = "whereis@sandiego.com"
    if request.method == 'POST':
        for filename, file in request.FILES.items():
            print request.FILES[filename].name

            newdoc = Document(
               docfile = file,
               email = email.replace('"',''),
               created = datetime.datetime.now()
            )
            #newdoc.filename = newdoc.email + '_' + ntpath.basename(newdoc.docfile.name).replace(' ','_')
            newdoc.filename = make_ufn(newdoc.email, newdoc.docfile.name)
            newdoc.save()
            rename_file(newdoc.docfile.name, newdoc.filename)

            # save to bp_plan
            try:
                print request.POST
                if request.POST.get('companyid'):
                    print request.POST.get('companyid')
                    company = Company.objects.get(company_id = int(request.POST.get('companyid')))
                else:
                    compq = Company.objects.filter(user=request.user)
                    if (compq):
                        company = compq[0]
                urlname = make_url(newdoc.email, filename)
                company.business_plan.name = urlname
                company.save()
            except:
                traceback.print_exc()

            # update user upload bp plan  link
            '''
            usr = None
            invprof = UserProfile.objects.filter( email = email )
            if invprof.exists():
              usr = UserProfile.objects.get( email = email )

            usrprof = InvestorProfile.objects.filter( email = email )
            if usrprof.exists():
              usr = InvestorProfile.objects.get( email = email )

            if usr:
                usr.profile_img = os.path.join(settings.MEDIA_URL,  newdoc.filename)
                usr.save()
            '''
    else:
         message = " request was not post type"
         success = False

    return_json = {}
    if (success):
        message = "file is successfully uploaded"
        return_json['file_name']= os.path.join(settings.MEDIA_URL,  newdoc.filename)
    return_json['success'] = success
    return_json['message'] = message
    json_str = json.dumps(return_json)
    print json_str
    return HttpResponse(json_str, content_type='application/json')

@csrf_exempt
def file_upload3(request):
    #esponseRedirect('/test/Layout.html')

    success = True
    #mydata = json.loads(str(request.body))
    print "in file_upload3"

    email = request.user.email
    if request.method == 'POST':

        for filename, file in request.FILES.items():
            print request.FILES[filename].name

            newdoc = Document(
               docfile = file,
               email = email.replace('"',''),
               created = datetime.datetime.now()
            )
            #newdoc.filename = newdoc.email + '_' + ntpath.basename(newdoc.docfile.name).replace(' ','_')
            newdoc.filename = make_ufn(newdoc.email, newdoc.docfile.name)
            newdoc.save()
            rename_file(newdoc.docfile.name, newdoc.filename)

            # update user profile img link
            usr = None
            invprof = UserProfile.objects.filter( email = email )
            if invprof.exists():
              usr = UserProfile.objects.get( email = email )

            usrprof = InvestorProfile.objects.filter( email = email )
            if usrprof.exists():
              usr = InvestorProfile.objects.get( email = email )

            if usr:
                usr.profile_img = os.path.join(settings.MEDIA_URL,  newdoc.filename)
                usr.save()
    else:
         message = " request was not post type"
         success = False

    return_json = {}
    if (success):
        message = "file is successfully uploaded"
        return_json['file_name']= os.path.join(settings.MEDIA_URL,  newdoc.filename)
    return_json['success'] = success
    return_json['message'] = message
    json_str = json.dumps(return_json)
    return HttpResponse(json_str, content_type='application/json')

@csrf_exempt
def logout_user(request):
    print "Logging out user..."
    auth_logout(request)
    response =  HttpResponseRedirect('/static/Layout.html')

    if request.COOKIES.has_key('keyagg'):
       print 'Deleted cookie keyagg:  ' + request.COOKIES.get('keyagg')
       response.delete_cookie('keyagg')

    if request.COOKIES.has_key('email'):
       print 'deleted cookie email:  ' +  request.COOKIES.get('email')
       response.delete_cookie('email')

    if request.COOKIES.has_key('userType'):
       print 'deleted cookie userType:  ' + request.COOKIES.get('userType')
       response.delete_cookie('userType')

    if request.COOKIES.has_key('first_name'):
       print 'deleted cookie first_name:  ' + request.COOKIES.get('first_name')
       response.delete_cookie('first_name')

    if request.COOKIES.has_key('last_name'):
       print 'deleted cookie last_name:  ' + request.COOKIES.get('last_name')
       response.delete_cookie('last_name')

    if request.COOKIES.has_key('profile_img'):
       print 'deleted cookie profile_key:  ' + request.COOKIES.get('profile_img')
       response.delete_cookie('profile_img')

    if request.COOKIES.has_key('userid'):
       print 'deleted cookie userid:  ' + request.COOKIES.get('userid')
       response.delete_cookie('userid')

    if request.COOKIES.has_key('from'):
       print 'deleted cookie from:  ' + request.COOKIES.get('from')
       response.delete_cookie('from')

    return  response


@csrf_exempt
@login_required
def home(request):
    return_json = create_generic_headerjson(request.user)
    return render_to_response('agglobal/index.html', return_json, context_instance=RequestContext(request))


@csrf_exempt
@login_required
def events(request):
    return render_to_response('agglobal/events.html', {}, context_instance=RequestContext(request))


# TO-DO
@csrf_exempt
@login_required
def submit_bp(request):
    return_json = create_generic_headerjson(request.user)
    return_json['companyid'] = request.GET.get("companyid")
    return render_to_response('agglobal/bp_upload.html', return_json, context_instance=RequestContext(request))


# TO-DO
@csrf_exempt
@login_required
def review_bp(request):
    return_json = create_generic_headerjson(request.user)
    return render_to_response('agglobal/bp-review.html', return_json, context_instance=RequestContext(request))


# TO-DO
@csrf_exempt
@login_required
def search_startups(request):
    return_json = create_generic_headerjson(request.user)

    return render_to_response('agglobal/startups.html', return_json, context_instance=RequestContext(request))

# TO-DO
@csrf_exempt
def search_investors(request):
    if request.user.is_authenticated():
        return_json = create_generic_headerjson(request.user)
        return render_to_response('agglobal/investor.html', return_json, context_instance=RequestContext(request))
    else:
        return render_to_response('agglobal/investor.html', {}, context_instance=RequestContext(request))


# TO-DO
@csrf_exempt
@login_required
def user_settings(request):
    return_json = create_generic_headerjson(request.user)
    return render_to_response('agglobal/settings.html', return_json, context_instance=RequestContext(request))




@csrf_exempt
def register_user(request):
    print "in register user loop"
    print request.body

    mydata = json.loads(str(request.body))
    success = True
    uemail = mydata['email'].replace('"', '')
    try:
        # create a new user in the database
        if request.user.is_authenticated():
           print "already authenticated, not creating new user"
           user = request.user
        else:
           user = User.objects.create_user(uemail, uemail, mydata['password'])
           user.is_active = True 
           user.is_staff = False
           user.is_superuser = False
           user.save()

        u_user = user.userprofile_set.all()
        if u_user.exists():
            new_user = UserProfile.objects.get( user = user )
        else:
            # build activation key
            salt = sha.new(str(random.random())).hexdigest()[:5]
            activation_key = sha.new(salt + uemail).hexdigest()
            key_expires = datetime.datetime.today() + datetime.timedelta(2)

            #sfilename = mydata['profile_img'] if 'profile_img' in mydata else ''
            #doc = Document.objects.filter(email=mydata['email'], seq = 0).order_by('-created').first()

            new_user = UserProfile(
                first_name=mydata['first_name'] if 'first_name' in mydata else '',
                last_name=mydata['last_name'] if 'last_name' in mydata else '',
                email=uemail,
                company_name=mydata['company_name'] if 'company_name' in mydata else '',
                domain=mydata['domain'] if 'domain' in mydata else '',
                introduction=mydata['introduction'] if 'introduction' in mydata else '',
                job_title=mydata['job_title'] if 'job_title' in mydata else '',
                personal_profile=mydata['personal_profile'] if 'personal_profile' in mydata else '',
                city=mydata['city'] if 'city' in mydata else '',
                country=mydata['country'] if 'country' in mydata else '',
                industry=mydata['industry'] if 'industry' in mydata else '',
                industryofconcern=mydata['industryofconcern'] if 'industryofconcern' in mydata else '',
                profession=mydata['profession'] if 'profession' in mydata else '',
                activation_key = activation_key,
                key_expires = key_expires,
                user = user,
            )
            #print os.path.join( settings.MEDIA_URL , mydata['profile_img'])
            if 'profile_img' in mydata:
                if mydata['profile_img'] <> '':
                  new_user.profile_img.name =   mydata['profile_img'].replace(' ','_')  
                  print new_user.profile_img.name

            new_user.save()
            # nu = User.objects.latest()

            for exp in mydata['business_experiences']:
                #json_parsed_startime = None
                #if exp['start_time']:
                #    json_parsed_startime = parse(exp['start_time'])

                #json_parsed_endtime = None
                #if exp['end_time']:
                #    json_parsed_endtime = parse(exp['end_time'])

                #sfilename = exp['attached_file'] if 'attached_file' in exp else ''
                doc = Document.objects.filter(email=uemail, seq = exp['id']).order_by('-created').first()

                new_exp = User_exp(
                    company_name=exp['company_name'] if 'company_name' in exp else '',
                    job_title=exp['job_title'] if 'job_title' in exp else '',
                    description=exp['description'] if 'description' in exp else '',
                    location=exp['location'] if 'location' in exp else '',
                    #start_time=json_parsed_startime,
                    #end_time=json_parsed_endtime,
                    start_year=exp['start_year'] if 'start_year' in exp else '',
                    start_month=exp['start_month'] if 'start_month' in exp else '',
                    end_year=exp['end_year'] if 'end_year' in exp else '',
                    end_month=exp['end_month'] if 'end_month' in exp else '',
                    user=new_user,
                )
                #print  os.path.join( settings.MEDIA_URL ,exp['attached_file'] )
                if 'attached_file' in exp:
                   if exp['attached_file'] <> '':
                     #new_exp.attached_file.name = os.path.join( settings.MEDIA_URL ,new_user.email + '_' + exp['attached_file'].replace(' ','_') )
                     new_exp.attached_file.name = make_url( new_user.email, exp['attached_file'] )
                     print new_exp.attached_file.name 

                new_exp.save()
    except:
        success = False
        print sys.exc_info()
        message = "error occurred " + str(sys.exc_info()[0])

    return_json = {}
    if (success):
       try:
           user = authenticate(username=uemail, password=mydata['password'])
           login(request, user)
       except:
           traceback.print_exc()
       message = "please redirect to user reigstration confirmation page"
       return_json['userType'] = 'e'
       return_json['first_name'] = new_user.first_name
       return_json['last_name'] = new_user.last_name
       return_json['profile_img'] = new_user.profile_img.name
       return_json['totalemails'] = getUserEmailCount(user.id)

    return_json['success'] = success
    return_json['message'] = message
    json_str = json.dumps(return_json)
    response =  HttpResponse(json_str, content_type='application/json')
    if (success):
        print uemail
        print new_user.first_name
        print new_user.last_name
        print new_user.profile_img.name
        response.set_cookie('userid',user.id)
        response.set_cookie('keyagg','successful_login')
        response.set_cookie('userType','e')
        response.set_cookie('email',uemail)
        response.set_cookie('first_name',new_user.first_name)
        response.set_cookie('last_name',new_user.last_name)
        response.set_cookie('profile_img',new_user.profile_img.name)
        print 'return response'
    return response
 
    
@csrf_exempt
def register_investor(request):
    print "in register investor loop"
    print request.POST.dict()
    print request.POST.items()
    print request.body

    mydata = json.loads(str(request.body))

    print mydata
    if 'email' in mydata:
        iemail =  mydata['email'].replace('"','')

    success = True
    try:
        # create a new user in the database
        if request.user.is_authenticated():
           print "already authenticated, not creating new user"
           user = request.user
        else:
           user = User.objects.create_user(iemail,iemail, mydata['password'])
           user.is_active = True 
           user.is_staff = False
           user.is_superuser = False
           user.save()


        u_inv = user.investorprofile_set.all()
        if u_inv.exists():
            new_investor = InvestorProfile.objects.get( user = user ) 
        else:
            # build activation keya

            salt = sha.new(str(random.random())).hexdigest()[:5]
            activation_key = sha.new(salt + iemail).hexdigest()
            key_expires = datetime.datetime.today() + datetime.timedelta(2)

            #sfilename = mydata['profile_img'] if 'profile_img' in mydata else ''
            doc = Document.objects.filter(email=iemail, seq = 0).order_by('-created').first()

            new_investor = InvestorProfile(
                first_name=mydata['first_name'] if 'first_name' in mydata else '',
                last_name=mydata['last_name'] if 'last_name' in mydata else '',
                email = iemail, 
                company_name=mydata['company_name'] if 'company_name' in mydata else '',
                introduction=mydata['introduction'] if 'introduction' in mydata else '',
                job_title=mydata['job_title'] if 'job_title' in mydata else '',
                personal_profile=mydata['personal_profile'] if 'personal_profile' in mydata else '',
                city=mydata['city'] if 'city' in mydata else '',
                country=mydata['country'] if 'country' in mydata else '',
                profession=mydata['profession'] if 'profession' in mydata else '',
                industryofconcern=mydata['industryofconcern'] if 'industryofconcern' in mydata else '',
                industry=mydata['industry'] if 'industry' in mydata else '',
                investment_field = mydata['investment_field'] if 'investment_field' in mydata else '',
                investment_location = mydata['investment_location']  if 'investment_location' in mydata else '',
                investment_budget = mydata['investment_budget'] if 'investment_budget' in mydata else '',
                investment_angel_status = mydata['investment_angel_status'] if 'investment_angel_status' in mydata else '',
                investment_a_status = mydata['investment_a_status'] if 'investment_a_status' in mydata else '',
                investment_b_status = mydata['investment_b_status'] if 'investment_b_status' in mydata else '',
                investment_pre_a_status = mydata['investment_pre_a_status'] if 'investment_pre_a_status' in mydata else '',
                investment_b_after_status = mydata['investment_b_after_status'] if 'investment_b_after_status' in mydata else '',
                activation_key = activation_key,
                key_expires = key_expires,
                user = user,
            )
            #print settings.MEDIA_URL + mydata['profile_img']
            if 'profile_img' in mydata:
               if mydata['profile_img'] <> '':
                 new_investor.profile_img.name =   mydata['profile_img'].replace(' ','_')
                 print new_investor.profile_img.name

            new_investor.save()

            for iexp in  mydata['investment_experiences']:

              #json_parsed_investdate = None
              #if iexp['invest_date']:
              #     json_parsed_investdate = parse(iexp['invest_date'])


              #sfilename = iexp['attached_project_file'] if 'attached_project_file' in iexp else ''
              doc = Document.objects.filter(email=iemail, seq = iexp['id'] ).order_by('-created').first()

              new_iexp = Investment_experiences(
                 project_name = iexp['project_name'] if 'project_name' in iexp else '',
                 #invest_date = json_parsed_investdate,
                 invest_year=iexp['invest_year'] if 'invest_year' in iexp else '',
                 invest_month=iexp['invest_month'] if 'invest_month' in iexp else '',

                 invested_status = iexp['invested_status'] if 'invested_status' in iexp else '',
                 project_detail = iexp['project_detail'] if 'project_detail' in iexp else '',
                 investor = new_investor,
              )
              #print settings.MEDIA_URL + iexp['attached_project_file']
              #if (doc is not None):
              if 'attached_project_file' in iexp:
                 if  iexp['attached_project_file'] <> '': 
                   #new_iexp.attached_project_file.name = os.path.join( settings.MEDIA_URL ,new_investor.email + '_' + iexp['attached_project_file'].replace(' ','_') ) 
                   new_iexp.attached_project_file.name = make_url( new_investor.email, iexp['attached_project_file'])
                   print new_iexp.attached_project_file.name

              new_iexp.save()

    except:
        success = False
        print sys.exc_info()
        message = "error occurred " + str(sys.exc_info()[0])

    return_json = {}
    if (success):
       message = "please redirect to investor reigstration confirmation page" 
       return_json['userType'] = 'i'
       return_json['first_name'] = new_investor.first_name
       return_json['last_name'] = new_investor.last_name
       return_json['profile_img'] = new_investor.profile_img.name
       return_json['totalemails'] = getUserEmailCount(user.id)

    return_json['success'] = success    
    return_json['message'] = message
    json_str = json.dumps(return_json)
    response =  HttpResponse(json_str, content_type='application/json')
    if (success):
        try:
	   #print iemail
	   #print new_investor.first_name
	   #print new_investor.last_name
	   #print new_investor.profile_img.name
           user = authenticate(username=iemail, password=mydata['password'])
           login(request, user)
        except:
           traceback.print_exc()
        response.set_cookie('userid',user.id)
        response.set_cookie('keyagg','successful_login')
        response.set_cookie('userType','i')
        response.set_cookie('email',iemail)
        response.set_cookie('first_name',new_investor.first_name)
        response.set_cookie('last_name',new_investor.last_name)
        response.set_cookie('profile_img',new_investor.profile_img.name)

    return response

@login_required
@csrf_exempt
def query_investor(request):
    print "in query investor loop"

    if request.COOKIES.has_key('sessionid'):
       print 'session id cookie:  ' +  request.COOKIES.get('sessionid')

    try:
     print request.POST.dict()
     print request.POST.items()
     print request.body

     mydata = json.loads(str(request.body))
     emailadd =  mydata['email'] if 'email' in mydata else '' 

     #emailadd = 'jj@jjj.com'
     print mydata
     print mydata['email']

     emailadd = request.user.email
     print emailadd

     success = True
    
     if request.user.is_authenticated(): 
        
       inv = InvestorProfile.objects.filter( email = emailadd )
       if inv.exists():
          inv = InvestorProfile.objects.get( email = emailadd )
          print inv
          inv_dict = model_to_dict(inv)
          inv_dict["key_expires"] = inv_dict["key_expires"].isoformat()
          inv_dict["profile_img"] = inv_dict["profile_img"].name
          inv_dict["investment_experiences"]=[]
          print inv_dict

          inv_exp = inv.investment_experiences_set.all()
          for exp in inv_exp:
             print exp
             inv_exp_dict = model_to_dict(exp) 
             inv_exp_dict["attached_project_file"]=inv_exp_dict["attached_project_file"].name

             print inv_exp_dict
             inv_dict["investment_experiences"].append( inv_exp_dict )

          print inv_dict
       else:
          success=False
          message = "There is no investor with given email address"
     else:
        success=False
        message = "user has not logged in yet"
 
    except:
        success = False
        print sys.exc_info()
        message = "error occurred " + str(sys.exc_info()[0])
 
    return_json = {}
    return_json['success'] = success

    if (success):
        message = "investor profile has been successfully retrieved" 
        return_json['data'] = inv_dict

    return_json['message'] = message
    json_str = json.dumps(return_json, ensure_ascii=False)
    return HttpResponse(json_str, content_type='application/json')

@csrf_exempt
def query_user(request):
    print "in query user loop"
    try:

     if request.COOKIES.has_key('sessionid'):
       print 'session id cookie:  ' +  request.COOKIES.get('sessionid')

     print request.POST.dict()
     print request.POST.items()
     print request.body

     mydata = json.loads(str(request.body))
     emailadd =  mydata['email']
     print mydata
     print mydata['email']
     #emailadd = 'whereis@sandiego.com'
     emailadd = request.user.email
     print emailadd
     success = True


     if request.user.is_authenticated():

        usr = UserProfile.objects.filter( email = emailadd )
        if usr.exists():
          usr = UserProfile.objects.get( email = emailadd )
          print usr
          usr_dict = model_to_dict(usr)
          usr_dict["key_expires"] = usr_dict["key_expires"].isoformat()
          usr_dict["profile_img"] = usr_dict["profile_img"].name
          usr_dict["business_experiences"]=[]
          print usr_dict

          usr_exp = usr.user_exp_set.all()
          for exp in usr_exp:
             print exp
             usr_exp_dict = model_to_dict(exp)
             usr_exp_dict["attached_file"]=usr_exp_dict["attached_file"].name
             print usr_exp_dict
             usr_dict["business_experiences"].append( usr_exp_dict )

          print usr_dict
        else:
          success=False
          message = "There are no user with give email address"

     else:
        success=False
        message = "user has not logged in yet"
     
    except:
        success = False
        print sys.exc_info()
        message = "error occurred " + str(sys.exc_info()[0])

    return_json = {}
    return_json['success'] = success

    if (success):
        message = "user profile has been successfully retrieved" 
        return_json['data'] = usr_dict

    return_json['message'] = message
    json_str = json.dumps(return_json, ensure_ascii=False)
    return HttpResponse(json_str, content_type='application/json')


@csrf_exempt
def queryall_investor(request):
   print "in query investor loop"

   try:
     print request.POST.dict()
     print request.POST.items()
     print request.body

     #mydata = json.loads(str(request.body))
     #emailadd =  mydata['email'] if 'email' in mydata else ''

     #print mydata
     #print mydata['email']
     success = True
     inv_results = []

     #if request.user.is_authenticated():
     if success:

       #inv_list = InvestorProfile.objects.filter( profession  = 'investing', job_title='manager' )
       inv_list = InvestorProfile.objects.all()    
       if inv_list.exists():
          for inv in inv_list:
             print inv
             inv_dict = model_to_dict(inv)
             inv_dict["key_expires"] = inv_dict["key_expires"].isoformat()
             inv_dict["profile_img"] = inv_dict["profile_img"].name
             inv_dict["investment_experiences"]=[]
             print inv_dict

             inv_exp = inv.investment_experiences_set.all()
             for exp in inv_exp:
                print exp
                inv_exp_dict = model_to_dict(exp)
                inv_exp_dict["attached_project_file"]=inv_exp_dict["attached_project_file"].name

                print inv_exp_dict
                inv_dict["investment_experiences"].append( inv_exp_dict )

             print inv_dict
             inv_results.append( inv_dict )

       else:
          success=False
          message = "There is no investor with given search conditions"
     #else:
     #   success=False
     #   message = "user has not logged in yet"

   except:
        success = False
        print sys.exc_info()
        message = "error occurred " + str(sys.exc_info()[0])

   return_json = {}
   return_json['success'] = success

   if (success):
        message = "investor profile has been successfully retrieved"
        return_json['data'] = inv_results

   return_json['message'] = message
   json_str = json.dumps(return_json, ensure_ascii=False)
   return HttpResponse(json_str, content_type='application/json')





@csrf_exempt
def queryfilter_investor(request):
   print "################ in queryfilter investor loop"

   try:
     print request.POST.dict()
     print request.POST.items()
     print request.body

     mydata = json.loads(str(request.body))
     #mydata = json.loads( ' {"investment_location":"sdf" } ')
     print mydata
     #print mydata['email']
     success = True
     inv_results = []
     kwargs = {}
     #if request.user.is_authenticated():
     if success:

       if 'investment_field' in mydata:
          if mydata['investment_field'] != '':
                       kwargs['investment_field'] = mydata['investment_field']
       if 'investment_pre_a_status' in mydata:
          if mydata['investment_pre_a_status'] != '':
                       kwargs['investment_pre_a_status'] = mydata['investment_pre_a_status']
       if 'investment_a_status' in mydata:
          if mydata['investment_a_status'] != '':
                       kwargs['investment_a_status'] = mydata['investment_a_status']
       if 'investment_angel_status' in mydata:
          if mydata['investment_angel_status'] != '':
                       kwargs['investment_angel_status'] = mydata['investment_angel_status']
       if 'investment_b_after_status' in mydata:
          if mydata['investment_b_after_status'] != '':
                       kwargs['investment_b_after_status'] = mydata['investment_b_after_status']
       if 'investment_b_status' in mydata:
          if mydata['investment_b_status'] != '':
                       kwargs['investment_b_status'] = mydata['investment_b_status']
       if 'investment_budget' in mydata:
          if mydata['investment_budget'] != '':
                       kwargs['investment_budget'] = mydata['investment_budget']
       if 'investment_location' in mydata:
          if mydata['investment_location'] != '':
                       kwargs['investment_location'] = mydata['investment_location']

       inv_list = InvestorProfile.objects.filter( **kwargs )
       #inv_list = InvestorProfile.objects.all()
       if inv_list.exists():
          for inv in inv_list:
             print inv
             inv_dict = model_to_dict(inv)
             inv_dict["key_expires"] = inv_dict["key_expires"].isoformat()
             inv_dict["profile_img"] = inv_dict["profile_img"].name
             inv_dict["investment_experiences"]=[]
             print inv_dict

             inv_exp = inv.investment_experiences_set.all()
             for exp in inv_exp:
                print exp
                inv_exp_dict = model_to_dict(exp)
                inv_exp_dict["attached_project_file"]=inv_exp_dict["attached_project_file"].name

                print inv_exp_dict
                inv_dict["investment_experiences"].append( inv_exp_dict )

             print inv_dict
             inv_results.append( inv_dict )

       else:
          success=False
          message = "There is no investor with given search conditions"
     #else:
     #   success=False
     #   message = "user has not logged in yet"

   except:
        success = False
        print sys.exc_info()
        message = "error occurred " + str(sys.exc_info()[0])

   return_json = {}
   return_json['success'] = success

   if (success):
        message = "investor profile has been successfully retrieved"
        return_json['data'] = inv_results

   return_json['message'] = message
   json_str = json.dumps(return_json, ensure_ascii=False)
   return HttpResponse(json_str, content_type='application/json')






@csrf_exempt
def queryall_user(request):
   print "in query all user loop"

   try:
     print request.POST.dict()
     print request.POST.items()
     print request.body

     #mydata = json.loads(str(request.body))
     #emailadd =  mydata['email'] if 'email' in mydata else ''

     #print mydata
     #print mydata['email']
     success = True
     usr_results = []

     #if request.user.is_authenticated():
     if success:
       #usr_list = userProfile.objects.filter( profession  = 'usresting', job_title='manager' )
       usr_list = UserProfile.objects.all()    
       if usr_list.exists():
          for usr in usr_list:
             print usr
             usr_dict = model_to_dict(usr)
             usr_dict["key_expires"] = usr_dict["key_expires"].isoformat()
             usr_dict["profile_img"] = usr_dict["profile_img"].name
             usr_dict["business_experiences"]=[]
             print usr_dict

             usr_exp = usr.user_exp_set.all()
             for exp in usr_exp:
                print exp
                usr_exp_dict = model_to_dict(exp)
                usr_exp_dict["attached_file"]=usr_exp_dict["attached_file"].name

                print usr_exp_dict
                usr_dict["business_experiences"].append( usr_exp_dict )

             print usr_dict
             usr_results.append( usr_dict )

       else:
          success=False
          message = "There is no usrestor with given search conditions"
     #else:
     #   success=False
     #   message = "user has not logged in yet"

   except:
        success = False
        print sys.exc_info()
        message = "error occurred " + str(sys.exc_info()[0])

   return_json = {}
   return_json['success'] = success

   if (success):
        message = "user profiles has been successfully retrieved"
        return_json['data'] = usr_results

   return_json['message'] = message
   json_str = json.dumps(return_json, ensure_ascii=False)
   return HttpResponse(json_str, content_type='application/json')


@login_required
@csrf_exempt
def register_company(request):
    print "####################  in register company loopi #######################"
    #mydata = json.loads(' { "project_name":"New project", "company_logo":"images (1).jpg", "industry":"..", "location":"GuangZhou", "invested_status":"A .", "investment_budget":"<= .500.", "company_site":"http://www.designs88.us/agweb/creart_startup_2.html", "introduction":"test", "product_img1":"baby-unicorn-small.jpg", "product_img2":"a.png", "product_img3":"images (2).jpg", "product_img4":"33.256.gif", "business_plan":"Hoteles_cerca.pdf", "description":"company introduction", "financing_experiences":[ { "id":1, "financing_title":"fdg", "start_year":"2014?", "start_month":"1?", "end_year":"2015?", "end_month":"2?", "introduction":"dfg" }, { "id":2, "file":"", "financing_title":"dfg", "start_year":"2014?", "start_month":"2?", "end_year":"2014?", "end_month":"3?", "introduction":"dfg" } ], "investors":[ { "id":1, "name":"dfg", "position":"fdg", "introduction":"fg", "file":"", "file_name":"ddsaa.png" }, { "id":2, "file":[ { } ], "name":"fdg", "position":"dfg", "introduction":"dfg" } ], "members":[ { "id":1, "name":"dfg", "position":"fdg", "introduction":"dfg", "file":"", "file_name":"adf adf.png" }, { "id":2, "file":"", "name":"fdg", "position":"fdg", "introduction":"fdg", "file_name":"model_0.53951800_1391063185.jpg" } ] } ')

    #mydata = json.loads(' {"project_name":"fsdf","company_logo":"ddsaa.png","industry":"??","location":"sdfdsf","invested_status":"Pre A","investment_budget":"<= ?200?","company_site":"sdf","product_img1":"ddsaa.png","product_img2":"adf adf.png","product_img3":"imaa.jpg","product_img4":"ttt.jpg","introduction":"sdfs","business_plan":"Hoteles_cerca.pdf","description":"sdfs","email":"uuuu@uuuu.com","financing_experiences":[{"id":1,"financing_title":"sdfsd","start_year":"2014?","start_month":"3?","end_year":"2013?","end_month":"3?","introduction":"sdfs"}],"investors":[{"id":1,"name":"sdfdsf","position":"sdfds","introduction":"sdf","file":"","file_name":"ddsaa.png"}],"members":[{"id":1,"name":"sdf","position":"dfs","introduction":"sdf","file":"","file_name":"baby-unicorn-small.jpg"}]} ')
    print request.body
    mydata = json.loads(str(request.body))
    print mydata
    success = True;

    #if success:
    try:

      if request.user.is_authenticated():
        usr1 = request.user

        print "Company does not exist - creating a new company"
        new_company = Company( user = usr1 )

        if 'invested_status' in mydata:
             new_company.invested_status=mydata['invested_status']
        if 'description' in mydata:
             new_company.description=mydata['description']
        if 'location' in mydata:
             new_company.location=mydata['location']
        if 'industry' in mydata:
             new_company.industry=mydata['industry']
        if 'project_name' in mydata:
             new_company.project_name=mydata['project_name']
        if 'investment_budget' in mydata:
             new_company.investment_budget=mydata['investment_budget']
        if 'company_site' in mydata:
             new_company.company_site=mydata['company_site']
        if 'introduction' in mydata:
             new_company.introduction=mydata['introduction']


        if 'company_logo' in mydata:
            if  mydata['company_logo'] <> '':
              #new_company.company_logo.name =  os.path.join( settings.MEDIA_URL , new_company.user.email + '_' +  mydata['company_logo'].replace(' ','_'))
              new_company.company_logo.name = make_url( new_company.user.email, mydata['company_logo'])
              print new_company.company_logo.name

        if 'business_plan' in mydata:
            if  mydata['business_plan'] <> '':
              #new_company.business_plan.name =  os.path.join( settings.MEDIA_URL , new_company.user.email + '_' +  mydata['business_plan'].replace(' ','_'))
              new_company.business_plan.name = make_url( new_company.user.email, mydata['business_plan'])
              print new_company.business_plan.name

        if 'product_img1' in mydata:
            if  mydata['product_img1'] <> '':
              #new_company.product_img1.name =  os.path.join( settings.MEDIA_URL , inew_company.user.email + '_' +  mydata['product_img1'].replace(' ','_'))
              new_company.product_img1.name = make_url( new_company.user.email,  mydata['product_img1'])
              print new_company.product_img1.name
                                                               
        if 'product_img2' in mydata:
            if  mydata['product_img2'] <> '':
              #new_company.product_img2.name =  os.path.join( settings.MEDIA_URL , new_company.user.email + '_' +  mydata['product_img2'].replace(' ','_'))
              new_company.product_img2.name = make_url( new_company.user.email, mydata['product_img2'])
              print new_company.product_img2.name

        if 'product_img3' in mydata:
            if  mydata['product_img3'] <> '':
              #new_company.product_img3.name =  os.path.join( settings.MEDIA_URL , new_company.user.email + '_' +  mydata['product_img3'].replace(' ','_'))
              new_company.product_img3.name = make_url( new_company.user.email, mydata['product_img3'])
              print new_company.product_img3.name

        if 'product_img4' in mydata:
            if  mydata['product_img4'] <> '':
              #new_company.product_img4.name =  os.path.join( settings.MEDIA_URL , new_company.user.email + '_' +  mydata['product_img4'].replace(' ','_'))
              new_company.product_img4.name = make_url( new_company.user.email, mydata['product_img4'])
              print new_company.product_img4.name


        new_company.save()

        print "Updating financeing experience data"

        for fexp in  mydata['financing_experiences']:
          print fexp
          if 'id' in fexp:
             if fexp['id'] == 0:
                 new_financing = Company_financing( company = new_company )                 
             else:
                 new_financing = Company_financing.objects.get( company = new_company, id = fexp['id']  )
                 
             if 'financing_title' in fexp:
                  new_financing.financing_title=fexp['financing_title']
             if 'start_year' in fexp:
                  new_financing.start_year=fexp['start_year']
             if 'start_month' in fexp:
                  new_financing.start_month=fexp['start_month']
             if 'end_year' in fexp:
                  new_financing.end_year=fexp['end_year']
             if 'end_month' in fexp:
                  new_financing.end_month=fexp['end_month']
             if 'introduction' in fexp:
                  new_financing.introduction=fexp['introduction']

             if 'file_name' in fexp:
               if  fexp['file_name']:
                 new_financing.file_name = make_url( request.user.email, fexp['file_name'])
                 print new_financing.file_name

             new_financing.save()
          



        print "Updating company investor data"

        for inv in  mydata['investors']:
          print inv
          if 'id' in inv:
            if inv['id'] == 0:
                 new_investor = Company_investors( company = new_company )                 
            else:
                 new_investor = Company_investors.objects.get( company = new_company, id = inv['id']  )

            if 'name' in inv:
                new_investor.name=inv['name']
                 
            if 'position' in inv:
                new_investor.position=inv['position']
                 
            if 'introduction' in inv:
                new_investor.introduction=inv['introduction'] 

            if 'file_name' in inv:
              if  inv['file_name']:
                new_investor.file_name = make_url( request.user.email, inv['file_name'])
                print new_investor.file_name

            new_investor.save()



        print "Updating company members data"

        for memb in  mydata['members']:
          print memb
          if 'id' in memb:
            if memb['id'] == 0:
                 new_member = Company_members( company = new_company )                 
            else:
                 new_member = Company_members.objects.get( company = new_company, id = memb['id']  )

            if 'name' in memb:
                new_member.name=memb['name']
                 
            if 'position' in memb:
                new_member.position=memb['position']
                 
            if 'introduction' in memb:
                new_member.introduction=memb['introduction'] 

            if 'file_name' in memb:
              if  memb['file_name']:
                new_member.file_name = make_url( request.user.email, memb['file_name'])
                print new_member.file_name

            new_member.save()

      else:
        success=False
        message = "user has not logged in yet"

    except:
        success = False
        print sys.exc_info()
        message = "error occurred " + str(sys.exc_info()[0])


    if (success):
        message = "please redirect to com;pany reigstration confirmation page"

    #response =  HttpResponseRedirect('/static/Layout.html')

    return_json = {}
    return_json['success'] = success
    return_json['message'] = message
    json_str = json.dumps(return_json)
    response =  HttpResponse(json_str, content_type='application/json')
    if (success):
        response.set_cookie('userid',usr1.id)
    return response



@login_required
@csrf_exempt
def register_company_old(request):
    print "####################  in register company loopi #######################"
    #mydata = json.loads(' { "project_name":"New project", "company_logo":"images (1).jpg", "industry":"..", "location":"GuangZhou", "invested_status":"A .", "investment_budget":"<= .500.", "company_site":"http://www.designs88.us/agweb/creart_startup_2.html", "introduction":"test", "product_img1":"baby-unicorn-small.jpg", "product_img2":"a.png", "product_img3":"images (2).jpg", "product_img4":"33.256.gif", "business_plan":"Hoteles_cerca.pdf", "description":"company introduction", "financing_experiences":[ { "id":1, "financing_title":"fdg", "start_year":"2014?", "start_month":"1?", "end_year":"2015?", "end_month":"2?", "introduction":"dfg" }, { "id":2, "file":"", "financing_title":"dfg", "start_year":"2014?", "start_month":"2?", "end_year":"2014?", "end_month":"3?", "introduction":"dfg" } ], "investors":[ { "id":1, "name":"dfg", "position":"fdg", "introduction":"fg", "file":"", "file_name":"ddsaa.png" }, { "id":2, "file":[ { } ], "name":"fdg", "position":"dfg", "introduction":"dfg" } ], "members":[ { "id":1, "name":"dfg", "position":"fdg", "introduction":"dfg", "file":"", "file_name":"adf adf.png" }, { "id":2, "file":"", "name":"fdg", "position":"fdg", "introduction":"fdg", "file_name":"model_0.53951800_1391063185.jpg" } ] } ')

    #mydata = json.loads(' {"project_name":"fsdf","company_logo":"ddsaa.png","industry":"软件","location":"sdfdsf","invested_status":"Pre A","investment_budget":"<= ￥200万","company_site":"sdf","product_img1":"ddsaa.png","product_img2":"adf adf.png","product_img3":"imaa.jpg","product_img4":"ttt.jpg","introduction":"sdfs","business_plan":"Hoteles_cerca.pdf","description":"sdfs","email":"uuuu@uuuu.com","financing_experiences":[{"id":1,"financing_title":"sdfsd","start_year":"2014年","start_month":"3月","end_year":"2013年","end_month":"3月","introduction":"sdfs"}],"investors":[{"id":1,"name":"sdfdsf","position":"sdfds","introduction":"sdf","file":"","file_name":"ddsaa.png"}],"members":[{"id":1,"name":"sdf","position":"dfs","introduction":"sdf","file":"","file_name":"baby-unicorn-small.jpg"}]} ')
    print request.body
    mydata = json.loads(str(request.body))
    print mydata
    success = True;

    #if success:
    try:

      if request.user.is_authenticated():

 
        new_company = Company(
            invested_status=mydata['invested_status'] if 'invested_status' in mydata else '',                
            description=mydata['description'] if 'description' in mydata else '',                            
            location=mydata['location'] if 'location' in mydata else '',                                     
            industry=mydata['industry'] if 'industry' in mydata else '',                                     
            project_name=mydata['project_name'] if 'project_name' in mydata else '',                         
            investment_budget=mydata['investment_budget'] if 'investment_budget' in mydata else '',
            company_site=mydata['company_site'] if 'company_site' in mydata else '',                         
            introduction=mydata['introduction'] if 'introduction' in mydata else '',                         
            user = request.user,
        )

        if 'company_logo' in mydata:
            if  mydata['company_logo'] <> '':
              #new_company.company_logo.name =  os.path.join( settings.MEDIA_URL , new_company.user.email + '_' +  mydata['company_logo'].replace(' ','_'))
              new_company.company_logo.name = make_url( new_company.user.email, mydata['company_logo'])
              print new_company.company_logo.name

        if 'business_plan' in mydata:
            if  mydata['business_plan'] <> '':
              #new_company.business_plan.name =  os.path.join( settings.MEDIA_URL , new_company.user.email + '_' +  mydata['business_plan'].replace(' ','_'))
              new_company.business_plan.name = make_url( new_company.user.email, mydata['business_plan'])
              print new_company.business_plan.name

        if 'product_img1' in mydata:
            if  mydata['product_img1'] <> '':
              #new_company.product_img1.name =  os.path.join( settings.MEDIA_URL , inew_company.user.email + '_' +  mydata['product_img1'].replace(' ','_'))
              new_company.product_img1.name = make_url( new_company.user.email,  mydata['product_img1'])
              print new_company.product_img1.name
            
        if 'product_img2' in mydata:
            if  mydata['product_img2'] <> '':
              #new_company.product_img2.name =  os.path.join( settings.MEDIA_URL , new_company.user.email + '_' +  mydata['product_img2'].replace(' ','_'))
              new_company.product_img2.name = make_url( new_company.user.email, mydata['product_img2'])
              print new_company.product_img2.name

        if 'product_img3' in mydata:
            if  mydata['product_img3'] <> '':
              #new_company.product_img3.name =  os.path.join( settings.MEDIA_URL , new_company.user.email + '_' +  mydata['product_img3'].replace(' ','_'))
              new_company.product_img3.name = make_url( new_company.user.email, mydata['product_img3'])
              print new_company.product_img3.name

        if 'product_img4' in mydata:
            if  mydata['product_img4'] <> '':
              #new_company.product_img4.name =  os.path.join( settings.MEDIA_URL , new_company.user.email + '_' +  mydata['product_img4'].replace(' ','_'))
              new_company.product_img4.name = make_url( new_company.user.email, mydata['product_img4'])
              print new_company.product_img4.name
            
            
        new_company.save()

        for fexp in  mydata['financing_experiences']:

          new_financing = Company_financing(
            financing_title=fexp['financeing_title'] if 'financeing_title' in fexp else '',
            start_year=fexp['start_year'] if 'start_year' in fexp else '',
            start_month=fexp['start_month'] if 'start_month' in fexp else '',
            end_year=fexp['end_year'] if 'end_year' in fexp else '',                                 
            end_month=fexp['end_month'] if 'end_month' in fexp else '',                              
            introduction=fexp['introduction'] if 'introduction' in fexp else '',                     
            company = new_company,
          )

          if 'file_name' in fexp:
            if  fexp['file_name'] <> '':
              new_financing.file_name = make_url( request.user.email, fexp['file_name'])
              print new_financing.file_name

          new_financing.save()


        for inv in  mydata['investors']:
 
          new_investor = Company_investors(
            name=inv['name'] if 'name' in inv else '',                                             
            position=inv['position'] if 'position' in inv else '',                                            
            introduction=inv['introduction'] if 'introduction' in inv else '',                     
            company =  new_company,                      
          )

          if 'file_name' in inv:
            if  inv['file_name'] <> '':
              new_investor.file_name = make_url( request.user.email, inv['file_name'])
              print new_investor.file_name

          new_investor.save()


        for memb in  mydata['members']:
          new_member = Company_members(
            name=memb['name'] if 'name' in memb else '',                                             
            position=memb['position'] if 'position' in memb else '',                                 
            introduction=memb['introduction'] if 'introduction' in memb else '',                     
            company = new_company,                           
          )

          if 'file_name' in memb:
            if  memb['file_name'] <> '':
              new_member.file_name = make_url( request.user.email, memb['file_name'])
              print new_member.file_name
            
          new_member.save()

      else:
        success=False
        message = "user has not logged in yet"

    except:
        success = False
        print sys.exc_info()
        message = "error occurred " + str(sys.exc_info()[0])


    if (success):
        message = "please redirect to com;pany reigstration confirmation page"
    return_json = {}
    return_json['success'] = success
    return_json['message'] = message
    json_str = json.dumps(return_json)
    response =  HttpResponse(json_str, content_type='application/json')
    if (success):
        response.set_cookie('keyagg','successful_company_registration')
    return response




@login_required
@csrf_exempt
def register_company_details(request):
    print "in register company details loop"
    #mydata = json.loads(' { "financing_experiences":[ { "id":1, "financing_title":"sdf", "start_year":"2014?", "start_month":"2?", "end_year":"2013?", "end_month":"4?", "introduction":"sdf" }, { "id":2, "file":"", "financing_title":"dsf", "start_year":"2013?", "start_month":"2?", "end_year":"2014?", "end_month":"3?", "introduction":"sdf" } ], "investors":[ { "id":1, "name":"sdf", "position":"dsf", "introduction":"sdf", "file":"", "file_name":"ddsaa.png" }, { "id":2,                 "name":"sdf", "position":"dsf", "introduction":"sdf", "file":"", "file_name":"ddsaa.png" } ], "members":[ { "id":1, "name":"dsf", "position":"sdf", "introduction":"sdf", "file":"", "file_name":"1.jpg" }, { "id":2, "file":"", "name":"sdf", "position":"dsf", "introduction":"sdf", "file_name":"ddsaa.png" } ] } ')
    print request.body
    mydata = json.loads(str(request.body))
    print mydata

    success = True

    try:

      if request.user.is_authenticated():
 
        this_company = Company.objects.get( user = request.user)
        #this_company = Company.objects.get( company_id = 1)

 
        for fexp in  mydata['financing_experiences']:

          new_financing = Company_financing(
            financing_title=fexp['financeing_title'] if 'financeing_title' in fexp else '',
            start_year=fexp['start_year'] if 'start_year' in fexp else '',
            start_month=fexp['start_month'] if 'start_month' in fexp else '',
            end_year=fexp['end_year'] if 'end_year' in fexp else '',                                 
            end_month=fexp['end_month'] if 'end_month' in fexp else '',                              
            introduction=fexp['introduction'] if 'introduction' in fexp else '',                     
            file_name=fexp['file_name'] if 'file_name' in fexp else '',                                      
            company = this_company,
          )

          if 'file' in fexp:
            if  fexp['file'] <> '':
              new_financing.file.name = make_url( request.user.email, fexp['file'])
              print new_financing.file.name

          new_financing.save()


        for inv in  mydata['investors']:
 
          new_investor = Company_investors(
            name=inv['name'] if 'name' in inv else '',                                             
            position=inv['position'] if 'position' in inv else '',                                            
            introduction=inv['introduction'] if 'introduction' in inv else '',                     
            file_name=inv['file_name'] if 'file_name' in inv else '',                              
            company =  this_company,                      
          )

          if 'file' in inv:
            if  inv['file'] <> '':
              new_investor.file.name = make_url( request.user.email, inv['file'])
              print new_investor.file.name

          new_investor.save()


        for memb in  mydata['members']:
          new_member = Company_members(
            name=memb['name'] if 'name' in memb else '',                                             
            position=memb['position'] if 'position' in memb else '',                                 
            introduction=memb['introduction'] if 'introduction' in memb else '',                     
            file_name=memb['file_name'] if 'file_name' in memb else '',                              
            company = this_company,                           
          )

          if 'file' in memb:
            if  memb['file'] <> '':
              new_member.file.name = make_url( request.user.email, memb['file'])
              print new_member.file.name
            
          new_member.save()

      else:
        success=False
        message = "user has not logged in yet"

    except:
        success = False
        print sys.exc_info()
        message = "error occurred " + str(sys.exc_info()[0])


    if (success):
        message = "please redirect to com;pany reigstration confirmation page"
    return_json = {}
    return_json['success'] = success
    return_json['message'] = message
    json_str = json.dumps(return_json)
    response =  HttpResponse(json_str, content_type='application/json')
    if (success):
        response.set_cookie('keyagg','successful_company_registration')
    return response


@login_required
@csrf_exempt
def queryallcompany_user(request):
    print "in query company loop "

    #print request.body
    #mydata = json.loads(str(request.body))
    #print mydata
    success = True
    comp_results = []

    if request.user.is_authenticated():

       compq = Company.objects.filter( user = request.user )
       if compq.exists():
           for comp in compq:
             print "company information:"
             print comp
             comp_dict = model_to_dict(comp)
             comp_dict["company_logo"] = comp_dict["company_logo"].name
             comp_dict["business_plan"] = comp_dict["business_plan"].name
             comp_dict["product_img1"] = comp_dict["product_img1"].name
             comp_dict["product_img2"] = comp_dict["product_img2"].name
             comp_dict["product_img3"] = comp_dict["product_img3"].name
             comp_dict["product_img4"] = comp_dict["product_img4"].name
             print comp_dict
             comp_dict["owljsondata"]= create_owl_data(request.user.email, comp_dict)
             comp_dict["financing_experiences"] = []
             comp_dict["investors"] = []
             comp_dict["members"] = []


             cfinexp = comp.company_financing_set.all()
             cinvestors = comp.company_investors_set.all()
             cmembers = comp.company_members_set.all()

             for fexp in cfinexp:
                print "company financing experience:"
                print fexp
                fexp_dict = model_to_dict(fexp)
                #fexp_dict["file"]=fexp_dict["file"].name

                print fexp_dict
                comp_dict["financing_experiences"].append( fexp_dict )

             for inv in cinvestors:
                print "company investors:"
                print inv
                inv_dict = model_to_dict(inv)
                #inv_dict["file"]=inv_dict["file"].name

                print inv_dict
                comp_dict["investors"].append( inv_dict )

             for memb in cmembers:
                print "company members:"
                print memb
                memb_dict = model_to_dict(memb)
                #memb_dict["file"]=memb_dict["file"].name

                print memb_dict
                comp_dict["members"].append( memb_dict )


             totallike = len(User_company_rel.objects.all().filter(company = comp, like='T'))
             comp_dict['totallike'] = totallike

             print comp_dict
             comp_results.append(comp_dict)

       else:
          success=False
          message = "There is no company associated with current user"
    else:
        success=False
        message = "user has not logged in yet"


    return_json = {}
    return_json['success'] = success

    if (success):
        message = "company profile has been successfully retrieved"
        return_json['data'] = comp_results

    return_json['message'] = message
    json_str = json.dumps(return_json, ensure_ascii=False)
    return HttpResponse(json_str, content_type='application/json')

@login_required
@csrf_exempt
def query_company(request, startupid):
    print "in query company loop " + str(startupid)

    #print request.body
    #mydata = json.loads(str(request.body))
    #print mydata
    success = True

    if request.user.is_authenticated():

       compq = Company.objects.filter( company_id = startupid)
       if compq.exists():
             comp = compq[0]
             print "company information:"
             print comp
             comp_dict = model_to_dict(comp)
             comp_dict["company_logo"] = comp_dict["company_logo"].name
             comp_dict["business_plan"] = comp_dict["business_plan"].name
             comp_dict["product_img1"] = comp_dict["product_img1"].name
             comp_dict["product_img2"] = comp_dict["product_img2"].name
             comp_dict["product_img3"] = comp_dict["product_img3"].name
             comp_dict["product_img4"] = comp_dict["product_img4"].name
             print comp_dict
             comp_dict["owljsondata"]= create_owl_data(request.user.email, comp_dict)
             comp_dict["financing_experiences"] = []
             comp_dict["investors"] = []
             comp_dict["members"] = []


             cfinexp = comp.company_financing_set.all()
             cinvestors = comp.company_investors_set.all()
             cmembers = comp.company_members_set.all()

             for fexp in cfinexp:
                print "company financing experience:"
                print fexp
                fexp_dict = model_to_dict(fexp)
                #fexp_dict["file"]=fexp_dict["file"].name

                print fexp_dict
                comp_dict["financing_experiences"].append( fexp_dict )

             for inv in cinvestors:
                print "company investors:"
                print inv
                inv_dict = model_to_dict(inv)
                #inv_dict["file"]=inv_dict["file"].name

                print inv_dict
                comp_dict["investors"].append( inv_dict )

             for memb in cmembers:
                print "company members:"
                print memb
                memb_dict = model_to_dict(memb)
                #memb_dict["file"]=memb_dict["file"].name

                print memb_dict
                comp_dict["members"].append( memb_dict )

             # try to query user_rel table to get like status
             likestatus = "F"
             followstatus = "F"
             relobject = User_company_rel.objects.all().filter(user = request.user, company = comp)
             if relobject and (relobject[0].like == "T"):
                likestatus = "T"

             if relobject and (relobject[0].follow == "T"):
                 followstatus = "T"

             comp_dict['likestatus'] = likestatus
             comp_dict['followstatus'] = followstatus

             totallike = len(User_company_rel.objects.all().filter(company = comp, like='T'))
             comp_dict['totallike'] = totallike

             print comp_dict

       else:
          success=False
          message = "There is no company associated with current user"
    else:
        success=False
        message = "user has not logged in yet"


    return_json = {}
    return_json['success'] = success

    if (success):
        message = "company profile has been successfully retrieved"
        return_json['data'] = comp_dict

    return_json['message'] = message
    json_str = json.dumps(return_json, ensure_ascii=False)
    return HttpResponse(json_str, content_type='application/json')

@login_required
@csrf_exempt
def query_bp(request, companyid):
    print "in query business plan "

    #print request.body
    #mydata = json.loads(str(request.body))
    #print mydata
    success = True
    message = ""
    bp_obj = {}

    try:
         company = Company.objects.all().get(company_id = companyid)
         bp_obj["project_name"] = company.project_name
         bp_obj["investment_budget"] = company.investment_budget
         cmembers = company.company_members_set.all()
         if len(cmembers) > 0:
            bp_obj["member"] = cmembers[0].name
         bp_obj["business_plan"] = company.business_plan.name

         bp_ratings = Businessplan_rating.objects.all().filter(company=companyid, user=request.user)
         if len(bp_ratings) > 0:
             bp_rating = bp_ratings[0]
             bp_obj["business_model"] = bp_rating.business_model
             bp_obj["expandibility"] = bp_rating.expandibility
             bp_obj["innovativeness"] = bp_rating.innovativeness
             bp_obj["projectteam"] = bp_rating.projectteam
             bp_obj["usercomment"] = bp_rating.usercomment
    except:
        success = False
        message = "error in query business plan " + str(sys.exc_info())
        traceback.print_exc()


    return_json = {}
    return_json['success'] = success

    if (success):
        message = "bp profile has been successfully retrieved"
        return_json['data'] = bp_obj

    return_json['message'] = message
    json_str = json.dumps(return_json, ensure_ascii=False)
    return HttpResponse(json_str, content_type='application/json')



@csrf_exempt
def queryall_company(request):
   print "in query all company loop"

   try:
     print request.POST.dict()
     print request.POST.items()
     print request.body

     #mydata = json.loads(str(request.body))
     #emailadd =  mydata['email'] if 'email' in mydata else ''

     #print mydata
     #print mydata['email']
     success = True
     comp_results = []

     #if request.user.is_authenticated():
     if success:
       #comp_list = userProfile.objects.filter( profession  = 'compesting', job_title='manager' )
       comp_list = Company.objects.all()
       if comp_list.exists():
          for comp in comp_list:
             print comp
             comp_dict = model_to_dict(comp)
             comp_dict["company_logo"] = comp_dict["company_logo"].name
             comp_dict["business_plan"] = comp_dict["business_plan"].name
             comp_dict["product_img1"] = comp_dict["product_img1"].name
             comp_dict["product_img2"] = comp_dict["product_img2"].name
             comp_dict["product_img3"] = comp_dict["product_img3"].name
             comp_dict["product_img4"] = comp_dict["product_img4"].name
             
             comp_dict["financing_experiences"] = []
             comp_dict["investors"] = []
             comp_dict["members"] = []
             
             # query for company fans
             relobject = User_company_rel.objects.all().filter(company = comp, like="T")
             comp_dict["totallike"] = len(relobject)

             cfinexp = comp.company_financing_set.all()
             cinvestors = comp.company_investors_set.all()
             cmembers = comp.company_members_set.all()
             
             for fexp in cfinexp: 
                print fexp
                fexp_dict = model_to_dict(fexp)

                print fexp_dict
                comp_dict["financing_experiences"].append( fexp_dict )           
             
             for inv in cinvestors:
                print inv
                inv_dict = model_to_dict(inv)

                print inv_dict
                comp_dict["investors"].append( inv_dict )             
             
             for memb in cmembers:
                print memb
                memb_dict = model_to_dict(memb)

                print memb_dict
                comp_dict["members"].append( memb_dict )             
                         
             
             print comp_dict
             comp_results.append( comp_dict )

       else:
          success=False
          message = "There is no company with given search conditions"
     #else:
     #   success=False
     #   message = "user has not logged in yet"

   except:
        success = False
        print sys.exc_info()
        message = "error occurred " + str(sys.exc_info()[0])

   return_json = {}
   return_json['success'] = success

   if (success):
        message = "company profiles has been successfully retrieved"
        return_json['data'] = comp_results

   return_json['message'] = message
   json_str = json.dumps(return_json, ensure_ascii=False)
   return HttpResponse(json_str, content_type='application/json')





@csrf_exempt
def queryfilter_company(request):
   print "in query all company loop"

   try:
     print request.POST.dict()
     print request.POST.items()
     print request.body

     mydata = json.loads(str(request.body))
     #emailadd =  mydata['email'] if 'email' in mydata else ''

     print mydata
     #print mydata['email']
     success = True
     comp_results = []
     kwargs = {}

     #if request.user.is_authenticated():
     if success:
            
       if 'invested_status' in mydata:
          if mydata['invested_status'] != '':
                       kwargs['invested_status'] = mydata['invested_status']
       if 'description' in mydata:
          if mydata['description'] != '':
                       kwargs['description'] = mydata['description']
       if 'location' in mydata:
          if mydata['location'] != '':
                       kwargs['location'] = mydata['location']
       if 'industry' in mydata:
          if mydata['industry'] != '':
                       kwargs['industry'] = mydata['industry']
       if 'project_name' in mydata:
          if mydata['project_name'] != '':
                       kwargs['project_name'] = mydata['project_name']
       if 'investment_budget' in mydata:
          if mydata['investment_budget'] != '':
                       kwargs['investment_budget'] = mydata['investment_budget']
       if 'business_plan' in mydata:
          if mydata['business_plan'] != '':
                       kwargs['business_plan'] = mydata['business_plan']
       if 'company_logo' in mydata:
          if mydata['company_logo'] != '':
                       kwargs['company_logo'] = mydata['company_logo']
       if 'company_site' in mydata:
          if mydata['company_site'] != '':
                       kwargs['company_site'] = mydata['company_site']

       print kwargs
       comp_list = Company.objects.filter( **kwargs )
       
       if comp_list.exists():
          for comp in comp_list:
             print comp
             comp_dict = model_to_dict(comp)
             comp_dict["company_logo"] = comp_dict["company_logo"].name
             comp_dict["business_plan"] = comp_dict["business_plan"].name
             comp_dict["product_img1"] = comp_dict["product_img1"].name
             comp_dict["product_img2"] = comp_dict["product_img2"].name
             comp_dict["product_img3"] = comp_dict["product_img3"].name
             comp_dict["product_img4"] = comp_dict["product_img4"].name
             
             comp_dict["financing_experiences"] = []
             comp_dict["investors"] = []
             comp_dict["members"] = []
             
             # query for company fans
             relobject = User_company_rel.objects.all().filter(company = comp, like="T")
             comp_dict["totallike"] = len(relobject)

             cfinexp = comp.company_financing_set.all()
             cinvestors = comp.company_investors_set.all()
             cmembers = comp.company_members_set.all()
             
             for fexp in cfinexp: 
                print fexp
                fexp_dict = model_to_dict(fexp)

                print fexp_dict
                comp_dict["financing_experiences"].append( fexp_dict )           
             
             for inv in cinvestors:
                print inv
                inv_dict = model_to_dict(inv)

                print inv_dict
                comp_dict["investors"].append( inv_dict )             
             
             for memb in cmembers:
                print memb
                memb_dict = model_to_dict(memb)

                print memb_dict
                comp_dict["members"].append( memb_dict )             
                         
             
             print comp_dict
             comp_results.append( comp_dict )

       else:
          success=False
          message = "There is no company with given search conditions"
     #else:
     #   success=False
     #   message = "user has not logged in yet"

   except:
        success = False
        print sys.exc_info()
        message = "error occurred " + str(sys.exc_info()[0])

   return_json = {}
   return_json['success'] = success

   if (success):
        message = "company profiles has been successfully retrieved"
        return_json['data'] = comp_results

   return_json['message'] = message
   json_str = json.dumps(return_json, ensure_ascii=False)
   return HttpResponse(json_str, content_type='application/json')






@login_required
@csrf_exempt
def edit_profile(request):
    print "############### in edit profile loop ##############"

    if request.COOKIES.has_key('sessionid'):
       print 'session id cookie:  ' +  request.COOKIES.get('sessionid')

    print request.POST
    mydata = request.POST
    #mydata = json.loads( ' {"last_name":"paul", "first_name":"brandy", "country":"US of A", "city":"Los Gatos", "industry":"fashion", "industryofconcern":"hi-tech", "company_name":"apple roasting", "profession":"King of apple", "introduction":"The biggest apple roaster in west coast", "investment_location":"here", "investment_budget":"huge", "investment_experiences":[  {  "id":"20", "project_name":"Big Apple", "invest_year":"1999", "invest_month":"8", "invested_status":"Yes Sir", "project_detail":"Still looking for the big opportunity"} ] }')

    #mydata = json.loads('{ "last_name": "where", "first_name": "is", "country": "US of America", "city": "a", "email": "joo@blah.com", "domain":"inernet",  "company_name": "electric art", "job_title":"Janitor", "personal_profile":"Traveled to Sandiego", "profession": "traveling", "business_experiences": [{ "id":"8", "company_name": "zzz", "job_title": "zzz", "location": "zzz", "start_year": "1912", "end_year": "1954", "description": "zzz" } ] }' ) 
    success = True

    #uemail = 'whereis@sandiego.com'
    #uemail = 'paul@brandy.com'
    uemail = request.user.email
    print uemail
    
    usr1 = request.user
    #usr1 =  User.objects.get(email=uemail)
    success = True
    try: 

      if request.user.is_authenticated():

        # locate user or investor in the database       
        usrprofq = usr1.userprofile_set.all()
        invprofq = usr1.investorprofile_set.all()
        
        if usrprofq.exists():
           print "Editing user profile"
           usrprof = UserProfile.objects.get( email = uemail )

           if ('name' in mydata) and (mydata['name'] == 'personal_profile'):
               usrprof.personal_profile = mydata['value']
           if 'first_name' in mydata:
              usrprof.first_name=mydata['first_name']
           if 'last_name' in mydata:
              usrprof.last_name=mydata['last_name']
           if 'company_name' in mydata:
              usrprof.company_name=mydata['company_name']
           if 'domain' in mydata:
              usrprof.domain=mydata['domain']
           if 'introduction' in mydata:
              usrprof.introduction=mydata['introduction']
           if 'job_title' in mydata:
              usrprof.job_title=mydata['job_title']
           if 'personal_profile' in mydata:
              usrprof.personal_profile=mydata['personal_profile']
           if 'city' in mydata:
              usrprof.city=mydata['city']
           if 'country' in mydata:
              usrprof.country=mydata['country']
           if 'industry' in mydata:
              usrprof.industry=mydata['industry']
           if 'industryofconcern' in mydata:
              usrprof.industryofconcern=mydata['industryofconcern']
           if 'profession' in mydata:
              usrprof.profession=mydata['profession']
                   
           if 'profile_img' in mydata:
              if mydata['profile_img'] <> '':
                #usrprof.profile_img.name =   mydata['profile_img'].replace(' ','_')  
                usrprof.profile_img.name =   make_url( uemail, mydata['profile_img'] )  
                print usrprof.profile_img.name

           usrprof.save()
           
           expid = 0
           expstr = 'usr_exp_list['+ str(expid) + ']'
           while expstr + '[usr_exp_id]' in mydata:

              print "Editing user business experiences: " + expstr

              uexp = User_exp.objects.filter( user = usrprof, id = mydata[expstr + '[usr_exp_id]'] )
              if uexp.exists():
                print 'uexp found: ' +  mydata[expstr + '[usr_exp_id]']
                uexp = uexp[0]
              else:
                print 'uexp not found create one new'
                uexp = User_exp( user = usrprof );

              if  expstr + '[company_name]' in mydata:
                uexp.company_name = mydata[expstr + '[company_name]']
              if  expstr + '[job_title]' in mydata:
                uexp.job_title = mydata[expstr + '[job_title]']
              if  expstr + '[description]' in mydata:
                uexp.description = mydata[expstr + '[description]']
              if  expstr + '[location]' in mydata:
                uexp.location = mydata[expstr + '[location]']
              if  expstr + '[start_year]' in mydata:
                uexp.start_year=mydata[expstr + '[start_year]']
              if  expstr + '[start_month]' in mydata:
                uexp.start_month=mydata[expstr + '[start_month]']
              if  expstr + '[end_year]' in mydata:
                uexp.end_year=mydata[expstr + '[end_year]']
              if  expstr + '[end_month]' in mydata:
                uexp.end_month=mydata[expstr + '[end_month]']

              if  expstr + '[attached_file]' in mydata:
                 if  mydata[expstr + '[attached_file]'] <> '':
                    uexp.attached_file.name = make_url( uemail, mydata[expstr + '[attached_file]'])
                    print "user exp attached file name " + uexp.attached_file.name

              uexp.save()
                  
              expid = expid + 1
              expstr = 'usr_exp_list['+ str(expid) + ']'
          


 
        elif invprofq.exists():
           print "Editing investor profile"
           invprof = InvestorProfile.objects.get( email = uemail )

           if ('name' in mydata) and (mydata['name'] == 'personal_profile'):
               invprof.personal_profile = mydata['value']

           if 'first_name' in mydata:
              invprof.first_name=mydata['first_name']
           if 'last_name' in mydata:
              invprof.last_name=mydata['last_name']
           if 'company_name' in mydata:
              invprof.company_name=mydata['company_name']
           if 'introduction' in mydata:
              invprof.introduction=mydata['introduction']
           if 'job_title' in mydata:
              invprof.job_title=mydata['job_title']
           if 'personal_profile' in mydata:
              invprof.personal_profile=mydata['personal_profile']
           if 'city' in mydata:
              invprof.city=mydata['city']
           if 'country' in mydata:
              invprof.country=mydata['country']
           if 'profession' in mydata:
              invprof.profession=mydata['profession']
           if 'industryofconcern' in mydata:
              invprof.industryofconcern=mydata['industryofconcern']
           if 'industry' in mydata:
              invprof.industry=mydata['industry']
           if 'investment_field' in mydata:
              invprof.investment_field = mydata['investment_field']
           if 'investment_location' in mydata:
              invprof.investment_location = mydata['investment_location']
           if 'investment_budget' in mydata:
              invprof.investment_budget = mydata['investment_budget']
           if 'investment_angel_status' in mydata:
              invprof.investment_angel_status = mydata['investment_angel_status']
           if 'investment_a_status' in mydata:
              invprof.investment_a_status = mydata['investment_a_status']
           if 'investment_b_status' in mydata:
              invprof.investment_b_status = mydata['investment_b_status']
           if 'investment_pre_a_status' in mydata:
              invprof.investment_pre_a_status = mydata['investment_pre_a_status']
           if 'investment_b_after_status' in mydata:
              invprof.investment_b_after_status = mydata['investment_b_after_status']

           if 'profile_img' in mydata:
               if mydata['profile_img'] <> '':
                 #invprof.profile_img.name =   mydata['profile_img'].replace(' ','_')
                 invprof.profile_img.name =  make_url( uemail,  mydata['profile_img'] )
                 print invprof.profile_img.name
                 
           invprof.save()


           expid = 0
           expstr = 'inv_exp_list['+ str(expid) + ']'
           while expstr + '[inv_exp_id]' in mydata:

              print "Editing investment experiences: " + expstr

              iexp = Investment_experiences.objects.filter(investor = invprof, id = mydata[expstr + '[inv_exp_id]'] )
              if iexp.exists():
                print 'iexp found: ' +  mydata[expstr + '[inv_exp_id]']
                iexp = iexp[0]
              else:
                iexp = Investment_experiences(investor = invprof)

              if  expstr + '[project_name]' in mydata:
                iexp.project_name = mydata[expstr + '[project_name]']
              if  expstr + '[invest_year]' in mydata:
                iexp.invest_year=mydata[expstr + '[invest_year]']
              if  expstr + '[invest_month]' in mydata:
                iexp.invest_month=mydata[expstr + '[invest_month]']
              if  expstr + '[invested_status]' in mydata:
                iexp.invested_status = mydata[expstr + '[invested_status]']
              if  expstr + '[project_detail]' in mydata:
                iexp.project_detail = mydata[expstr + '[project_detail]']

              if  expstr + '[attached_file]' in mydata:
                 if  mydata[expstr + '[attached_file]'] <> '':
                    iexp.attached_project_file.name = make_url( uemail, mydata[expstr + '[attached_file]'])
                    print "attached project file name " + iexp.attached_project_file.name

              iexp.save()
                  
              expid = expid + 1
              expstr = 'inv_exp_list['+ str(expid) + ']'             

        else:
          success = False
          message = 'user profile object is not found for email: ' + uemail 
        
        
    except:
        success = False
        print sys.exc_info()
        traceback.print_exc()
        message = "error occurred " + str(sys.exc_info()[0])

    return_json = create_generic_headerjson(request.user)
    if (success):
       message = "Successful update" 

    return_json['success'] = success
    return_json['message'] = message

    print return_json
    json_str = json.dumps(return_json, ensure_ascii=False)
    return HttpResponse(json_str, content_type='application/json')

    
##################  TEST ROUTINES  #####################



def query_test(request):
    print "in query test loop"
    print request.POST.dict()
    print request.POST.items()
    print request.body

    #mydata = json.loads(str(request.body))
    #emailadd =  mydata['email']
    #print mydata
    #print mydata['email']
    emailadd = 'whereis@sandiego.com'
    success = True


    try:

     #if request.user.is_authenticated():

        usr = UserProfile.objects.get( email = emailadd )
        print usr
        usr_dict = model_to_dict(usr)
        usr_dict["key_expires"] = usr_dict["key_expires"].isoformat()
        usr_dict["profile_img"] = usr_dict["profile_img"].name
        usr_dict["business_experience"]=[]
        print usr_dict

        usr_exp = usr.user_exp_set.all()
        for exp in usr_exp:
           print exp
           usr_exp_dict = model_to_dict(exp)
           usr_exp_dict["attached_file"]=usr_exp_dict["attached_file"].name
           print usr_exp_dict
           usr_dict["business_experience"].append( usr_exp_dict )

        print usr_dict

     #else:
     #   success=False
     #   message = "user has not logged in yet"

    except:
        success = False
        print sys.exc_info()
        message = "error occurred " + str(sys.exc_info()[0])

    if (success):
        message = "user profile has been successfully retrieved"
    return_json = {}
    return_json['success'] = success
    return_json['message'] = message
    return_json['data'] = usr_dict
    json_str = json.dumps(return_json, ensure_ascii=False)
    return HttpResponse(json_str, content_type='application/json')


   
@csrf_exempt
def register_check_test(request):
    print "in register check loop"
    print request.body

    #mydata = json.loads(str(request.body))
    #email_addr = mydata["email"]
    email_addr = 'herei@om'
    success = True

    print email_addr
    if success:
    #try:
        if User.objects.filter(email=email_addr).exists():
           success=False
           message="Email address has already been used"
        else:
           success=True
           message="Email address is available for registration"
    #except:
    #    success = False
    #    print sys.exc_info()
    #    message = "error occurred " + str(sys.exc_info()[0])

    return_json = {}
    return_json['availble'] = success
    return_json['message'] = message
    json_str = json.dumps(return_json)
    response =  HttpResponse(json_str, content_type='application/json')
    return response

            
            
@csrf_exempt
def login_user_test(request):
    logger.info("user logging to angie global website")
    error = ''
    username = password = ''
    print request.body

    print "entering user login page"

    print request.method

    success = True
        
    #mydata = json.loads(str(request.body))

    #username ='whereis@sandiego.com'
    #password ='world
    username = 'e@angelsglobal.com'
    password = '123456'

    #username = mydata['email']
    #password = mydata['password']
    print username
    print password
    usertype = ''

    user = authenticate(username=username, password=password)

    if user is not None:
        if user.is_active:
            print "user is active"
            login(request, user)
            message =   'Your have succesfully logged in.'
            u_usr = user.userprofile_set.all()
            u_inv = user.investorprofile_set.all()

            if u_inv.exists():
               usertype = 'i'
               print "this user is investor"

            if u_usr.exists():
               usertype = 'e'
               print "this user is normal user"

        else:
            print 'Your account is not active, please contact the site'
            message = 'Your account is not active, please contact the site'
            success = False
            auth_logout(request)
    else:
        message = 'Your username and/or password are incorrect.'
        print  'Your username and/or password are incorrect.'
        success = False
        auth_logout(request)


    return_json = {}
    return_json['success'] = success
    return_json['message'] = message
    if (success):
       return_json['userType'] = usertype

    json_str = json.dumps(return_json)
    print json_str
    response =  HttpResponse(json_str, content_type='application/json')
    if (success):
           response.set_cookie('keyagg','successful_login')
    return response
 

def register_user_test(request):
    print "in register user loop"
    print request.method
    # if  request.method == 'POST':
    print "hello world"
    print request.POST.dict()
    print request.POST.items()
    print request.body

@csrf_exempt
def list(request):
    # Handle file upload

    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            newdoc = Document(
                docfile = request.FILES['docfile'],
                email = 'me@me.mindcraft.com',
                #filename= 'testfile.txt',
                created = datetime.datetime.now()
            )

            newdoc.filename = make_ufn(newdoc.email, newdoc.docfile.name )
            newdoc.save()
            rename_file( newdoc.docfile.name, newdoc.filename )
            # Redirect to the document list after POST
            return HttpResponseRedirect(reverse('agglobal.views.list'))
    else:
        form = DocumentForm() # A empty, unbound form

    # Load documents for the list page
    documents = Document.objects.all()

    # Render list page with the documents and the form
    return render_to_response(
        'agglobal/list.html',
        {'documents': documents, 'form': form},
        context_instance=RequestContext(request)
    )



@csrf_exempt
def multiquery_test(request):
    print "in query investor loop"

    try:
     print request.POST.dict()
     print request.POST.items()
     print request.body

     #mydata = json.loads(str(request.body))
     #emailadd =  mydata['email'] if 'email' in mydata else ''

     #emailadd = 'jj@jjj.com'
     #print mydata
     #print mydata['email']
     success = True
     inv_results = []
     
     #if request.user.is_authenticated():

     inv_list = InvestorProfile.objects.filter( profession  = 'investing', job_title='manager' )
     if inv_list.exists():
      for inv in inv_list:
             print inv
             inv_dict = model_to_dict(inv)
             inv_dict["key_expires"] = inv_dict["key_expires"].isoformat()
             inv_dict["profile_img"] = inv_dict["profile_img"].name
             inv_dict["investment_experiences"]=[]
             print inv_dict

             inv_exp = inv.investment_experiences_set.all()
             for exp in inv_exp:
                print exp
                inv_exp_dict = model_to_dict(exp)
                inv_exp_dict["attached_project_file"]=inv_exp_dict["attached_project_file"].name

                print inv_exp_dict
                inv_dict["investment_experiences"].append( inv_exp_dict )

             print inv_dict
             inv_results.append( inv_dict )
             
     else:
        success=False
        message = "There is no investor with given search conditions"
    #else:
    #    success=False
    #    message = "user has not logged in yet"

    except:
        success = False
        print sys.exc_info()
        message = "error occurred " + str(sys.exc_info()[0])

    return_json = {}
    return_json['success'] = success

    if (success):
        message = "investor profile has been successfully retrieved"
        return_json['data'] = inv_results

    return_json['message'] = message
    json_str = json.dumps(return_json, ensure_ascii=False)
    return HttpResponse(json_str, content_type='application/json')


@csrf_exempt
def register_investor_test(request):
    print "in register investor test loop"
    #print request.POST.dict()
    #print request.POST.items()
    #print request.body

    #mydata = json.loads(str(request.body))
    #mydata = json.loads( ' {  "last_name":"a", "first_name":"adf", "country":"US", "city":"ad", "industry":"互联网", "industryofconcern":"互联网", "company_name":"saf", "profession":"df", "introduction":"a", "investment_location":"adf", "investment_budget":"50???", "investment_pre_a_status":"Y", "investment_a_status":"Y", "investment_b_status":"Y", "profile_img":"Joris.jpg", "email":"ccab@a.com", "password":"a", "confirmPassword":"a", "investment_experiences":[  {  "id":1, "project_name":"adf", "invest_year":"2014", "invest_month":"3", "invested_status":"A ?", "project_detail":"adsf", "files":"", "attached_project_file":"images.jpg" }, {  "id":2, "project_name":"adsf", "invested_status":"Pre A", "invest_year":"2014", "invest_month":"3", "project_detail":"adsf", "files":"", "attached_project_file":"question.jpg" } ] }')


    mydata = json.loads( ' {"profile_img":"33.256.gif","last_name":"dsfds","industryofconcern":"金融","country":"美国","city":"sdf","company_name":"sdf","profession":"sdf","introduction":"sdf","investment_location":"sdf","investment_budget":"100-500万","investment_angel_status":"Y","investment_pre_a_status":"Y","email":"kkk@first_national.com","password":"kkkkkk","confsrmPassword":"aaaaaa","investment_experiences":[{"id":1,"project_name":"dsf","invest_year":"2014","invest_month":"3","invested_status":"Pre A","project_detail":"sdfds","files":"","attached_project_file":"baby-unicorn-small.jpg"},{"id":2,"project_name":"sdf","invested_status":"A 轮","invest_year":"2014","invest_month":"2","project_detail":"dsf","files":"","attached_project_file":"ddsaa.png"}]} ')

    print mydata
    if 'email' in mydata:
        print mydata['email']

    success = True
    if success:
	    try:
		# create a new user in the database
		user = User.objects.create_user(mydata['email'], mydata['email'], mydata['password'])
		user.is_active = True
		user.is_staff = False
		user.is_superuser = False
		user.save()
		# build activation keya

		salt = sha.new(str(random.random())).hexdigest()[:5]
		activation_key = sha.new(salt + mydata['email']).hexdigest()
		key_expires = datetime.datetime.today() + datetime.timedelta(2)

		#sfilename = mydata['profile_img'] if 'profile_img' in mydata else ''
		doc = Document.objects.filter(email=mydata['email'], seq = 0).order_by('-created').first()

		new_investor = InvestorProfile(
		    first_name=mydata['first_name'] if 'first_name' in mydata else '',
		    last_name=mydata['last_name'] if 'last_name' in mydata else '',
		    email = mydata['email'],
		    company_name=mydata['company_name'] if 'company_name' in mydata else '',
		    introduction=mydata['introduction'] if 'introduction' in mydata else '',
		    job_title=mydata['job_title'] if 'job_title' in mydata else '',
		    personal_profile=mydata['personal_profile'] if 'personal_profile' in mydata else '',
		    city=mydata['city'] if 'city' in mydata else '',
		    country=mydata['country'] if 'country' in mydata else '',
		    profession=mydata['profession'] if 'profession' in mydata else '',
		    industryofconcern=mydata['industryofconcern'] if 'industryofconcern' in mydata else '',
		    industry=mydata['industry'] if 'industry' in mydata else '',
		    investment_field = mydata['investment_field'] if 'investment_field' in mydata else '',
		    investment_location = mydata['investment_location']  if 'investment_location' in mydata else '',
		    investment_budget = mydata['investment_budget'] if 'investment_budget' in mydata else '',
		    investment_angel_status = mydata['investment_angel_status'] if 'investment_angel_status' in mydata else '',
		    investment_a_status = mydata['investment_a_status'] if 'investment_a_status' in mydata else '',
		    investment_b_status = mydata['investment_b_status'] if 'investment_b_status' in mydata else '',
		    investment_pre_a_status = mydata['investment_pre_a_status'] if 'investment_pre_a_status' in mydata else '',
		    investment_b_after_status = mydata['investment_b_after_status'] if 'investment_b_after_status' in mydata else '',
		    activation_key = activation_key,
		    key_expires = key_expires,
		    user = user,
		)
		#print settings.MEDIA_URL + mydata['profile_img']
		if 'profile_img' in mydata:
		   #new_investor.profile_img.name =  os.path.join( settings.MEDIA_URL , mydata['profile_img'].replace(' ','_'))
		   new_investor.profile_img.name = make_url( new_investor.email, mydata['profile_img'])
		   print new_investor.profile_img.name

		new_investor.save()

		for iexp in  mydata['investment_experiences']:

		  #json_parsed_investdate = None
		  #if iexp['invest_date']:
		  #     json_parsed_investdate = parse(iexp['invest_date'])


		  #sfilename = iexp['attached_project_file'] if 'attached_project_file' in iexp else ''
		  doc = Document.objects.filter(email=mydata['email'], seq = iexp['id'] ).order_by('-created').first()

		  new_iexp = Investment_experiences(
		     project_name = iexp['project_name'] if 'project_name' in iexp else '',
		     #invest_date = json_parsed_investdate,
		     invest_year=iexp['invest_year'] if 'invest_year' in iexp else '',
		     invest_month=iexp['invest_month'] if 'invest_month' in iexp else '',

		     invested_status = iexp['invested_status'] if 'invested_status' in iexp else '',
		     project_detail = iexp['project_detail'] if 'project_detail' in iexp else '',
		     investor = new_investor,
		  )
		  #print settings.MEDIA_URL + iexp['attached_project_file']
		  #if (doc is not None):
		  if 'attached_project_file' in iexp:
		     #new_iexp.attached_project_file.name = os.path.join( settings.MEDIA_URL ,iexp['attached_project_file'].replace(' ','_') )
		     new_iexp.attached_project_file.name = make_url( new_investor.email, iexp['attached_project_file'])
		     print new_iexp.attached_project_file.name

		  new_iexp.save()

	    except:
	        success = False
        	traceback.print_exc()
	        message = "error occurred " + str(sys.exc_info()[0])

    if (success):
        message = "please redirect to user reigstration confirmation page"
    return_json = {}
    return_json['success'] = success
    return_json['message'] = message



    json_str = json.dumps(return_json)
    response =  HttpResponse(json_str, content_type='application/json')
    if (success):
        response.set_cookie('keyagg','successful_login')
    return response

@login_required
@csrf_exempt
def view_user_profile(request, userid):
    logger.info("view profile on user id" + str(userid))
    try:
     success = True

     if request.user.is_authenticated():

        usr = UserProfile.objects.filter( user = userid )
        if usr.exists():
          usr = UserProfile.objects.get( user = userid )
          print usr
          usr_dict = model_to_dict(usr)
          usr_dict["key_expires"] = usr_dict["key_expires"].isoformat()
          usr_dict["profile_img"] = usr_dict["profile_img"].name
          usr_dict["business_experiences"]=[]
          print usr_dict

          usr_exp = usr.user_exp_set.all()
          for exp in usr_exp:
             print exp
             usr_exp_dict = model_to_dict(exp)
             usr_exp_dict["attached_file"]=usr_exp_dict["attached_file"].name
             print usr_exp_dict
             usr_dict["business_experiences"].append( usr_exp_dict )

          print usr_dict
        else:
          success=False
          message = "There are no user with give email address"

     else:
        success=False
        message = "user has not logged in yet"

    except:
        success = False
        print sys.exc_info()
        message = "error occurred " + str(sys.exc_info()[0])

    return_json = create_generic_headerjson(request.user)
    return_json['success'] = success

    if (success):
        message = "user profile has been successfully retrieved"
        return_json['data'] = usr_dict

    return_json['message'] = message


    # try to query user_rel table to get like status
    likestatus = "F"
    relobject = User_rel.objects.all().filter(from_user = request.user, to_user = User.objects.get(id=int(userid)))
    if relobject and (relobject[0].like == "T"):
        likestatus = "T"

    return_json['likestatus'] = likestatus

    totallike = len(User_rel.objects.all().filter(to_user = User.objects.get(id=int(userid)), like='T'))
    return_json['totallike'] = totallike

    json_str = json.dumps(return_json, ensure_ascii=False)

    if (request.user.id == int(userid)):
        return render_to_response('agglobal/profile_user_edit_2.html', return_json,context_instance=RequestContext(request))
    else:
        return render_to_response('agglobal/profile_user.html', return_json,context_instance=RequestContext(request))


@csrf_exempt
def view_investor_profile(request, userid):
    print "########  in view investor profile loopi #############"
    if request.COOKIES.has_key('sessionid'):
       print 'session id cookie:  ' +  request.COOKIES.get('sessionid')

    try:
       success = True

       inv = InvestorProfile.objects.filter( user = userid )

       if inv.exists():
          inv = InvestorProfile.objects.get( user = userid )
          print inv
          inv_dict = model_to_dict(inv)
          inv_dict["key_expires"] = inv_dict["key_expires"].isoformat()
          inv_dict["profile_img"] = inv_dict["profile_img"].name
          inv_dict["investment_experiences"]=[]
          print inv_dict

          inv_exp = inv.investment_experiences_set.all()
          for exp in inv_exp:
             print exp
             inv_exp_dict = model_to_dict(exp)
             inv_exp_dict["attached_project_file"]=inv_exp_dict["attached_project_file"].name

             print inv_exp_dict
             inv_dict["investment_experiences"].append( inv_exp_dict )

          print inv_dict
       else:
          success=False
          message = "There is no investor with given email address"

    except:
        success = False
        print sys.exc_info()
        message = "error occurred " + str(sys.exc_info()[0])

    return_json = {}
    return_json['success'] = success

    if (success):
        message = "investor profile has been successfully retrieved"
        return_json['data'] = inv_dict

    return_json['message'] = message

    if request.user.is_authenticated():

        return_json2= create_generic_headerjson(request.user)
        return_json = dict(return_json.items() + return_json2.items())

        # try to query user_rel table to get like status
        likestatus = "F"
        relobject = User_rel.objects.all().filter(from_user = request.user, to_user = User.objects.get(id=int(userid)))
        if relobject and (relobject[0].like == "T"):
            likestatus = "T"

        return_json['likestatus'] = likestatus

    totallike = len(User_rel.objects.all().filter(to_user = User.objects.get(id=int(userid)), like = 'T'))
    return_json['totallike'] = totallike

    json_str = json.dumps(return_json, ensure_ascii=False)
    if (request.user.is_authenticated() and (request.user.id == int(userid))):
        return render_to_response('agglobal/profile_investor_edit_2.html', return_json, context_instance=RequestContext(request))
    else:
        return render_to_response('agglobal/profile_investor.html', return_json,  context_instance=RequestContext(request))


@csrf_exempt
def get_all_sent_message(request, userid=None):
    print "##### entering get_all_sent_messages ####"
    if (userid):
        all_messages = Message.objects.filter(sender=userid, parentid=None, deleted='F').order_by('senttime')

    json_dict = {}

    for message in all_messages:
        senduser = message.sender
        print senduser.email

        u_user = senduser.userprofile_set.all()

	if u_user.exists():
            print u_user[0].email
            sender = UserProfile.objects.get( user =  senduser )
	else:
            sender = InvestorProfile.objects.get( user = senduser)

        receiveuser = message.receiver
        u_user = receiveuser.userprofile_set.all()
	if u_user.exists():
           receiver = UserProfile.objects.get( user = receiveuser )
	else:
           receiver = InvestorProfile.objects.get( user = receiveuser )


        json_dict[message.id] = {
            'subject': message.subject,
            'body': message.body,
            'sender': {
                'id': message.sender.id,
                'display_name': sender.last_name + " " + sender.first_name,
                'profile_img': sender.profile_img.name
            },
            'reciever': {
                'id': message.receiver.id,
                'display_name': receiver.last_name + " " + receiver.first_name,
                'profile_img': receiver.profile_img.name
            },
            'read': message.read,
            'deleted': message.deleted,
            'senttime': str(message.senttime),
            'parentid': message.parentid
        }

    json_str = json.dumps(json_dict)
    return HttpResponse(json_str, content_type='application/json')

def get_all_message(request, userid=None):
    print "##### entering get_all_messages ####"
    print userid
    if (userid):
        all_messages = Message.objects.filter(receiver=userid, deleted='F').order_by('senttime')
    else:
        all_messages = Message.objects.filter(deleted='F').order_by('senttime')

    json_dict = {}

    for message in all_messages:
        senduser = message.sender
        print senduser.email

        u_user = senduser.userprofile_set.all()

        if u_user.exists():
            print u_user[0].email
            sender = UserProfile.objects.get( user =  senduser )
        else:
            sender = InvestorProfile.objects.get( user = senduser)

        receiveuser = message.receiver
        u_user = receiveuser.userprofile_set.all()

        if u_user.exists():
           receiver = UserProfile.objects.get( user = receiveuser )
        else:
           receiver = InvestorProfile.objects.get( user = receiveuser )


        json_dict[message.id] = {
            'subject': message.subject,
            'body': message.body,
            'sender': {
                'id': message.sender.id,
                'display_name': sender.last_name + " " + sender.first_name,
                'profile_img': sender.profile_img.name
            },

            'reciever': {
                'id': message.receiver.id,
                'display_name': receiver.last_name + " " + receiver.first_name,
                'profile_img': receiver.profile_img.name
            },
            'read': message.read,
            'deleted': message.deleted,
            'senttime': str(message.senttime),
            'parentid': message.parentid
        }

    json_str = json.dumps(json_dict)
    print json_str
    return HttpResponse(json_str, content_type='application/json')

@csrf_exempt
def post_new_message(request, parentid=None):
    success = True
    message = 'message is saved successfully'
    if request.method == 'POST':
        try:
            mydata = json.loads(str(request.body))
            parentid = None
            if ('parentid' in mydata) and mydata['parentid']:
               parentid = int(mydata['parentid'])
          
            newMessage = Message(
                subject = mydata['subject'],
                body = mydata['body'],
                sender = User.objects.get(id=int(mydata['sender'])),
                receiver = User.objects.get(id=int(mydata['receiver'])),
                read = 'F',
                deleted = 'F',
                parentid = parentid,
                senttime = datetime.datetime.now()
            )
            newMessage.save()
        except:
            success = False
            message = "error occurred " + str(sys.exc_info()[0])
	    traceback.print_exc()

        return_json={}
        return_json['success'] = success
        return_json['message'] = message
        json_str = json.dumps(return_json)
        return HttpResponse(json_str, content_type='application/json')

    else:
        return_json={}
        return_json['success'] = success
        return_json['message'] = message
        return HttpResponse(json_str, content_type='application/json')

def mark_message_delete(request, messageid):
    success = True
    messagestr = 'message is saved successfully'
    try:
        message = Message.objects.get(id=messageid)
        message.deleted = 'T'
        message.save()
    except:
        success = False
        messagestr = "error occurred " + str(sys.exc_info()[0])

    return_json={}
    return_json['success'] = success
    return_json['message'] = messagestr
    json_str = json.dumps(return_json)
    return HttpResponse(json_str, content_type='application/json')

def mark_message_read(request, messageid):
    success = True
    messagestr = 'message is saved successfully'
    try:
        message = Message.objects.get(id=messageid)
        message.read = 'T'
        message.save()
    except:
        success = False
        messagestr = "error occurred " + str(sys.exc_info()[0])

    return_json={}
    return_json['success'] = success
    return_json['message'] = messagestr
    json_str = json.dumps(return_json)
    return HttpResponse(json_str, content_type='application/json')


@csrf_exempt
def get_message_chain(request, messageid=None):
    print "##### entering get_messages_chain ####"
    #messageid=12
    messageids = []
    messageids.append(messageid)
    messages = Message.objects.filter(pk__in=messageids, deleted='F').order_by('senttime')

    json_dict = {}
    
    while messages.exists():
    
      nextlevel_messageids = [] 

      for message in messages:
        
        cur_parent_id = message.id

        print "cur_parent_id: " + str(cur_parent_id)
        senduser = message.sender
        print senduser.email

        u_user = senduser.userprofile_set.all()

        if u_user.exists():
            print u_user[0].email
            sender = UserProfile.objects.get( user =  senduser )
        else:
            sender = InvestorProfile.objects.get( user = senduser)

        receiveuser = message.receiver
        u_user = receiveuser.userprofile_set.all()
        if u_user.exists():
           receiver = UserProfile.objects.get( user = receiveuser )
        else:
           receiver = InvestorProfile.objects.get( user = receiveuser )


        json_dict[message.id] = {
            'subject': message.subject,
            'body': message.body,
            'sender': {
                'id': message.sender.id,
                'display_name': sender.last_name + " " + sender.first_name,
                'profile_img': sender.profile_img.name
            },
            'reciever': {
                'id': message.receiver.id,
                'display_name': receiver.last_name + " " + receiver.first_name,
                'profile_img': receiver.profile_img.name
            },
            'read': message.read,
            'deleted': message.deleted,
            'senttime': str(message.senttime),
            'parentid': message.parentid
        }

        child_messages = Message.objects.filter(parentid=cur_parent_id, deleted='F').order_by('senttime')

        if child_messages.exists():
           for cmess in child_messages:
               nextlevel_messageids.append(cmess.id)  
      
      if len(nextlevel_messageids) > 0:             
         messages = Message.objects.filter(pk__in=nextlevel_messageids, deleted='F').order_by('senttime')
      else:
         messages = Message.objects.none()

    json_str = json.dumps(json_dict)
    return HttpResponse(json_str, content_type='application/json')


@csrf_exempt
def get_lastmessage(request, messageid=None):
    print "##### entering get_messages_chain ####"
    #messageid=12
    messageids = []
    messageids.append(messageid)
    messages = Message.objects.filter(pk__in=messageids, deleted='F').order_by('senttime')

    json_dict = {}

    while messages.exists():

      nextlevel_messageids = []

      for message in messages:

        cur_parent_id = message.id

        print "cur_parent_id: " + str(cur_parent_id)

        child_messages = Message.objects.filter(parentid=cur_parent_id, deleted='F').order_by('senttime')

        if child_messages.exists():
           for cmess in child_messages:
               nextlevel_messageids.append(cmess.id)

      if len(nextlevel_messageids) > 0:
         messages = Message.objects.filter(pk__in=nextlevel_messageids, deleted='F').order_by('senttime')
      else:
         messages = Message.objects.none()

    if cur_parent_id > 0:  
        message = Message.objects.get(id=cur_parent_id, deleted='F')
          
        senduser = message.sender
        print senduser.email

        u_user = senduser.userprofile_set.all()

        if u_user.exists():
            print u_user[0].email
            sender = UserProfile.objects.get( user =  senduser )
        else:
            sender = InvestorProfile.objects.get( user = senduser)

        receiveuser = message.receiver
        u_user = receiveuser.userprofile_set.all()
        if u_user.exists():
           receiver = UserProfile.objects.get( user = receiveuser )
        else:
           receiver = InvestorProfile.objects.get( user = receiveuser )


        json_dict[message.id] = {
            'subject': message.subject,
            'body': message.body,
            'sender': {
                'id': message.sender.id,
                'display_name': sender.last_name + " " + sender.first_name,
                'profile_img': sender.profile_img.name
            },
            'reciever': {
                'id': message.receiver.id,
                'display_name': receiver.last_name + " " + receiver.first_name,
                'profile_img': receiver.profile_img.name
            },
            'read': message.read,
            'deleted': message.deleted,
            'senttime': str(message.senttime),
            'parentid': message.parentid
        }


    json_str = json.dumps(json_dict)
    return HttpResponse(json_str, content_type='application/json')

@csrf_exempt
def view_email_page(request):
    return_json = create_generic_headerjson(request.user)
    return render_to_response('agglobal/email_user.html', return_json, context_instance=RequestContext(request))

@login_required
@csrf_exempt
def view_startup_page(request, startupid):
    return_json=create_generic_headerjson(request.user)
    return_json['startupid'] = startupid
    company = Company.objects.all().get(company_id=startupid)
    return_json['startupusrname'] = company.user.last_name + " " + company.user.first_name
    print return_json['startupusrname']
    return render_to_response('agglobal/startup.html', return_json, context_instance=RequestContext(request))

@csrf_exempt
def post_company_like(request):
    success = True
    message = 'like / follow company message is posted successfully'
    if request.method == 'POST':
        try:
            mydata = json.loads(str(request.body))
            companyid = int(mydata['companyid'])
            fromuserid = request.user.id
            action = mydata['action']

            tousertype = ""
            fromusertype = ""
            company = Company.objects.all().get(company_id=companyid)


            fromuser = User.objects.all().get(id=fromuserid)
            u_usr = fromuser.userprofile_set.all()
            u_inv = fromuser.investorprofile_set.all()

            if u_inv.exists():
                fromusertype = 'Investor'

            if u_usr.exists():
                fromusertype = 'User'

            relobject = User_company_rel.objects.all().filter(company = company, user = fromuser)

            print 'action ' + action
            if action == 'like':
                if (relobject):
                    relobject = relobject[0]
                    relobject.like = 'T'
                else:
                    relobject = User_company_rel (
                        user = fromuser,
                        company = company,
                        user_type = fromusertype,
                        like = "T",
                    )
                relobject.save()

            if action == 'unlike':
                if (relobject):
                    relobject = relobject[0]
                    relobject.like = 'F'
                else:
                    relobject = User_company_rel (
                        user = fromuser,
                        company = company,
                        user_type = fromusertype,
                        like = "F",
                    )
                relobject.save()

            if action == 'followup':
                if (relobject):
                    relobject = relobject[0]
                    relobject.follow = 'T'
                else:
                    relobject = User_company_rel (
                        user = fromuser,
                        company = company,
                        user_type = fromusertype,
                        follow = "T",
                    )
                relobject.save()

            if action == 'unfollowup':
                if (relobject):
                    relobject = relobject[0]
                    relobject.follow = 'F'
                else:
                    relobject = User_company_rel (
                        user = fromuser,
                        company = company,
                        user_type = fromusertype,
                        follow = "F",
                    )
                relobject.save()
        except:
            traceback.print_exc()
            success = False
            message = "error occurred " + str(sys.exc_info()[0])

        return_json={}
        return_json['success'] = success
        return_json['message'] = message
        json_str = json.dumps(return_json)
        return HttpResponse(json_str, content_type='application/json')

    else:
        return_json={}
        return_json['success'] = success
        return_json['message'] = message
        json_str = json.dumps(return_json)
        return HttpResponse(json_str, content_type='application/json')

@login_required
@csrf_exempt
def bpupload_success(request):
    return_json=create_generic_headerjson(request.user)
    return render_to_response('agglobal/bp_upload_success.html', return_json, context_instance=RequestContext(request))

@csrf_exempt
def postbp_rating(request):
    success = True
    message = 'message is posted successfully'
    if request.method == 'POST':
        try:
            mydata = json.loads(str(request.body))
            print request.body
            userid = request.user.id
            company_id = mydata['companyid']
            business_model_rating = mydata['business_model']
            expandibility_rating = mydata['expandibility']
            innovativeness_rating = mydata['innovativeness']
            projectteam_rating = mydata['projectteam']
            usercomment = mydata['usercomment']


            bp_rating = Businessplan_rating.objects.all().filter(user = request.user)

            if (bp_rating):
                print 'existing'
                bp_rating = bp_rating[0]
                bp_rating.business_model = business_model_rating
                bp_rating.expandibility = expandibility_rating
                bp_rating.innovativeness = innovativeness_rating
                bp_rating.projectteam = projectteam_rating
                bp_rating.usercomment = usercomment
                bp_rating.company =Company.objects.all().get(company_id=company_id)
                bp_rating.user  = request.user
            else:
                print 'xxx'
                bp_rating = Businessplan_rating (
                    business_model = business_model_rating,
                    expandibility = expandibility_rating,
                    innovativeness = innovativeness_rating,
                    projectteam = projectteam_rating,
                    usercomment = usercomment,
                    company = Company.objects.all().get(company_id=company_id),
                    user = request.user
                )

            bp_rating.save()
        except:
            traceback.print_exc()
            success = False
            message = "error occurred " + str(sys.exc_info()[0])

        return_json={}
        return_json['success'] = success
        return_json['message'] = message
        json_str = json.dumps(return_json)
        return HttpResponse(json_str, content_type='application/json')

    else:
        return_json={}
        return_json['success'] = success
        return_json['message'] = message
        json_str = json.dumps(return_json)
        return HttpResponse(json_str, content_type='application/json')

@csrf_exempt
def post_like(request):
    success = True
    message = 'like message is posted successfully'
    if request.method == 'POST':
        try:
            mydata = json.loads(str(request.body))
            touserid = int(mydata['userid'])
            fromuserid = request.user.id
            action = mydata['action']

            tousertype = ""
            fromusertype = ""
            touser = User.objects.all().get(id=touserid)
            u_usr = touser.userprofile_set.all()
            u_inv = touser.investorprofile_set.all()

            if u_inv.exists():
                tousertype = 'Investor'

            if u_usr.exists():
                tousertype = 'User'

            fromuser = User.objects.all().get(id=fromuserid)
            u_usr = fromuser.userprofile_set.all()
            u_inv = fromuser.investorprofile_set.all()

            if u_inv.exists():
                fromusertype = 'Investor'

            if u_usr.exists():
                fromusertype = 'User'

            relobject = User_rel.objects.all().filter(from_user = fromuser, to_user = touser)

            print 'action ' + action
            if action == 'like':
                if (relobject):
                    relobject = relobject[0]
                    relobject.like = 'T'
                else:
                    relobject = User_rel (
                        from_user = fromuser,
                        to_user = touser,
                        from_user_type = fromusertype,
                        to_user_type = tousertype,
                        like = "T",
                    )
                relobject.save()

            if action == 'unlike':
                if (relobject):
                    relobject = relobject[0]
                    relobject.like = 'F'
                else:
                    relobject = User_rel (
                        from_user = fromuser,
                        to_user = touser,
                        from_user_type = fromusertype,
                        to_user_type = tousertype,
                        like = "F",
                    )
                relobject.save()

            if action == 'followup':
                if (relobject):
                    relobject = relobject[0]
                    relobject.follow = 'T'
                else:
                    relobject = User_rel (
                        from_user = fromuser,
                        to_user = touser,
                        from_user_type = fromusertype,
                        to_user_type = tousertype,
                        follow = "T",
                    )
                relobject.save()
        except:
            traceback.print_exc()
            success = False
            message = "error occurred " + str(sys.exc_info()[0])

        return_json={}
        return_json['success'] = success
        return_json['message'] = message
        json_str = json.dumps(return_json)
        return HttpResponse(json_str, content_type='application/json')

    else:
        return_json={}
        return_json['success'] = success
        return_json['message'] = message
        json_str = json.dumps(return_json)
        return HttpResponse(json_str, content_type='application/json')



@login_required
@csrf_exempt
def query_follow(request):

   print "################ in query follow loop ###############"

   print request.POST.dict()
   print request.POST.items()
   print request.body

   mydata = json.loads(str(request.body))
   print mydata

   if 'userid' in mydata:
          userid = mydata['userid']
          print mydata['userid']
   success = True
   tot_results = []
 
   try:

       inv_list = User_rel.objects.filter( from_user=userid, follow='T', to_user_type='Investor' )
       if inv_list.exists():
          for inv in inv_list:
             print inv
             inv_detail = InvestorProfile.objects.filter( user = inv.to_user )[0]
             inv_dict = model_to_dict(inv_detail)
             inv_dict["key_expires"] = inv_dict["key_expires"].isoformat()
             inv_dict["profile_img"] = inv_dict["profile_img"].name
             inv_dict["user_type"] = "investor"
             print inv_dict

             tot_results.append( inv_dict )


       usr_list = User_rel.objects.filter( from_user=userid, follow='T', to_user_type='User' )
       if usr_list.exists():
          for usr in usr_list:
             print usr
             usr_detail = UserProfile.objects.filter( user = usr.to_user )[0]
             usr_dict= model_to_dict(usr_detail)
             usr_dict["key_expires"] = usr_dict["key_expires"].isoformat()
             usr_dict["profile_img"] = usr_dict["profile_img"].name
             usr_dict["user_ype"]= "user"
             print usr_dict

             tot_results.append( usr_dict )


   except:
        success = False
        print sys.exc_info()
        message = "error occurred " + str(sys.exc_info()[0])
   print "about to return json"
   return_json = {}
   return_json['success'] = success

   if (success):
        message = "follow profiles has been successfully retrieved"
        return_json['data'] = tot_results

   return_json['message'] = message
   json_str = json.dumps(return_json, ensure_ascii=False)
   return HttpResponse(json_str, content_type='application/json')


@login_required
@csrf_exempt
def query_befollowed(request):

     print "################ in query befollowed loop ###############"

   
     print request.POST.dict()
     print request.POST.items()
     print request.body

     mydata = json.loads(str(request.body))
     print mydata

     if 'userid' in mydata:
          userid = mydata['userid']
          print mydata['userid']

     #userid = 133
     success = True
     tot_results = []

     #if request.user.is_authenticated():
     if success:

       inv_list = User_rel.objects.filter( to_user=userid, follow='T', from_user_type='Investor' )
       if inv_list.exists():
          for inv in inv_list:
             print inv
             inv_detail = InvestorProfile.objects.filter( user = inv.from_user )[0]
             inv_dict = model_to_dict(inv_detail)
             inv_dict["key_expires"] = inv_dict["key_expires"].isoformat()
             inv_dict["profile_img"] = inv_dict["profile_img"].name
             inv_dict["user_type"] = "investor"

             print inv_dict

             tot_results.append( inv_dict )


       usr_list = User_rel.objects.filter( to_user=userid, follow='T', from_user_type='User' )
       if usr_list.exists():
          for usr in usr_list:
             print usr
             usr_detail = UserProfile.objects.filter( user = usr.from_user )[0]
             usr_dict= model_to_dict(usr_detail)
             usr_dict["key_expires"] = usr_dict["key_expires"].isoformat()
             usr_dict["profile_img"] = usr_dict["profile_img"].name
             usr_dict["user_type"] = "user"

             print usr_dict

             tot_results.append( usr_dict )

     #else:
     #   success=False
     #   message = "user has not logged in yet"

   #except:
   #     success = False
   #     print sys.exc_info()
   #     message = "error occurred " + str(sys.exc_info()[0])
     return_json = {}
     return_json['success'] = success

     if (success):
        message = "befollowed profiles has been successfully retrieved"
        return_json['data'] = tot_results

     return_json['message'] = message
     json_str = json.dumps(return_json, ensure_ascii=False)
     return HttpResponse(json_str, content_type='application/json')

@login_required
@csrf_exempt
def queryall_bpreviews(request, companyid):
    print 'in query all bp reviews page'
    br_results = []
    brlist = Businessplan_rating.objects.filter(company = companyid)

    return_json = {}

    if brlist.exists():
       for br in brlist:
            br_dict = model_to_dict( br )
            u_usr = request.user.userprofile_set.all()
            u_inv = request.user.investorprofile_set.all()

            if u_inv.exists():
               usertype = 'i'
               firstname = u_inv[0].first_name
               lastname = u_inv[0].last_name
               profileimg = u_inv[0].profile_img.name
               print "this user is investor"

            if u_usr.exists():
               usertype = 'e'
               firstname = u_usr[0].first_name
               lastname = u_usr[0].last_name
               profileimg = u_usr[0].profile_img.name
               print "this user is normal user"
            br_dict['profileimg'] = profileimg
            br_dict['username'] = lastname + " " + firstname
            br_results.append( br_dict )

       return_json['data'] = br_results

    json_str = json.dumps(return_json, ensure_ascii=False)
    return HttpResponse(json_str, content_type='application/json') 


def create_generic_headerjson(user):
    return_json={}
    if (user):
        usrprof = UserProfile.objects.all().filter( user = user )
        if (usrprof):
            return_json['lusertype'] = 'u'
            return_json['luserid'] = user.id
            return_json['lusername'] = usrprof[0].last_name
            return_json['luseremail'] = user.email
            return_json['lprofimg'] = usrprof[0].profile_img.name
            return_json['totalemails'] = getUserEmailCount(user.id)

        invprof = InvestorProfile.objects.all().filter(user = user)
        if (invprof):
            return_json['lusertype'] = 'i'
            return_json['luserid'] = user.id
            return_json['lusername'] = invprof[0].last_name
            return_json['luseremail'] = user.email
            return_json['lprofimg'] = invprof[0].profile_img.name
            return_json['totalemails'] = getUserEmailCount(user.id)

        usercompanies = Company.objects.all().filter(user = user)

        companies = []
        for company in usercompanies:
             comp_dict = model_to_dict(company)
             comp_dict["company_logo"] = comp_dict["company_logo"].name
             comp_dict["business_plan"] = comp_dict["business_plan"].name
             comp_dict["product_img1"] = comp_dict["product_img1"].name
             comp_dict["product_img2"] = comp_dict["product_img2"].name
             comp_dict["product_img3"] = comp_dict["product_img3"].name
             comp_dict["product_img4"] = comp_dict["product_img4"].name
             companies.append(comp_dict)

        return_json['lcompanies'] = companies

    return return_json
