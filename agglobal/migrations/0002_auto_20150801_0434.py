# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agglobal', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='logo',
            field=models.FileField(null=True, upload_to=b'documents/%Y/%m/%d', blank=True),
        ),
        migrations.AlterField(
            model_name='invested_project',
            name='attached_project_file',
            field=models.FileField(null=True, upload_to=b'documents/%Y/%m/%d', blank=True),
        ),
        migrations.AlterField(
            model_name='investment_experiences',
            name='attached_project_file',
            field=models.FileField(null=True, upload_to=b'documents/%Y/%m/%d', blank=True),
        ),
        migrations.AlterField(
            model_name='investorprofile',
            name='profile_logo',
            field=models.FileField(null=True, upload_to=b'documents/%Y/%m/%d', blank=True),
        ),
        migrations.AlterField(
            model_name='user_exp',
            name='attached_file',
            field=models.FileField(null=True, upload_to=b'documents/%Y/%m/%d', blank=True),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='profile_logo',
            field=models.FileField(null=True, upload_to=b'documents/%Y/%m/%d', blank=True),
        ),
    ]
