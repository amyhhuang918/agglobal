# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agglobal', '0011_auto_20150802_1758'),
    ]

    operations = [
        migrations.RenameField(
            model_name='investment_experiences',
            old_name='invest_a_status',
            new_name='investment_a_status',
        ),
        migrations.RenameField(
            model_name='investment_experiences',
            old_name='invest_angel_status',
            new_name='investment_angel_status',
        ),
        migrations.RenameField(
            model_name='investment_experiences',
            old_name='invest_b_status',
            new_name='investment_b_status',
        ),
        migrations.RenameField(
            model_name='investment_experiences',
            old_name='invest_c_status',
            new_name='investment_c_status',
        ),
        migrations.RenameField(
            model_name='investment_experiences',
            old_name='invest_o_status',
            new_name='investment_o_status',
        ),
    ]
