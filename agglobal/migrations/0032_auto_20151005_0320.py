# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agglobal', '0031_businessplan'),
    ]

    operations = [
        migrations.CreateModel(
            name='Businessplan_rating',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('business_model', models.CharField(max_length=1, null=True, blank=True)),
                ('expandibility', models.CharField(max_length=1, null=True, blank=True)),
                ('innovativeness', models.CharField(max_length=1, null=True, blank=True)),
                ('projectteam', models.CharField(max_length=1, null=True, blank=True)),
                ('usercomment', models.CharField(max_length=200, null=True, blank=True)),
                ('company', models.ForeignKey(to='agglobal.Company')),
            ],
        ),
        migrations.RemoveField(
            model_name='businessplan',
            name='company',
        ),
        migrations.RemoveField(
            model_name='businessplan',
            name='user',
        ),
        migrations.DeleteModel(
            name='Businessplan',
        ),
    ]
