# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agglobal', '0005_auto_20150801_0609'),
    ]

    operations = [
        migrations.RenameField(
            model_name='document',
            old_name='usertype',
            new_name='filename',
        ),
        migrations.RemoveField(
            model_name='document',
            name='seq',
        ),
    ]
