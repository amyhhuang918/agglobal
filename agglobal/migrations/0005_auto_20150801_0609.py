# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agglobal', '0004_auto_20150801_0551'),
    ]

    operations = [
        migrations.RenameField(
            model_name='document',
            old_name='filetype',
            new_name='usertype',
        ),
        migrations.AddField(
            model_name='document',
            name='seq',
            field=models.IntegerField(null=True, blank=True),
        ),
    ]
