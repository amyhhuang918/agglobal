# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agglobal', '0012_auto_20150802_2144'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='company',
            name='end_date',
        ),
        migrations.RemoveField(
            model_name='company',
            name='start_date',
        ),
        migrations.RemoveField(
            model_name='investment_experiences',
            name='invest_date',
        ),
        migrations.RemoveField(
            model_name='investment_experiences',
            name='investment_a_status',
        ),
        migrations.RemoveField(
            model_name='investment_experiences',
            name='investment_angel_status',
        ),
        migrations.RemoveField(
            model_name='investment_experiences',
            name='investment_b_status',
        ),
        migrations.RemoveField(
            model_name='investment_experiences',
            name='investment_budget',
        ),
        migrations.RemoveField(
            model_name='investment_experiences',
            name='investment_c_status',
        ),
        migrations.RemoveField(
            model_name='investment_experiences',
            name='investment_field',
        ),
        migrations.RemoveField(
            model_name='investment_experiences',
            name='investment_location',
        ),
        migrations.RemoveField(
            model_name='investment_experiences',
            name='investment_o_status',
        ),
        migrations.AddField(
            model_name='company',
            name='end_month',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='company',
            name='end_year',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='company',
            name='start_month',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='company',
            name='start_year',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='investment_experiences',
            name='invest_month',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='investment_experiences',
            name='invest_year',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='investorprofile',
            name='investment_a_status',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='investorprofile',
            name='investment_angel_status',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='investorprofile',
            name='investment_b_after_status',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='investorprofile',
            name='investment_b_status',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='investorprofile',
            name='investment_budget',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='investorprofile',
            name='investment_field',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='investorprofile',
            name='investment_location',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='investorprofile',
            name='investment_pre_a_status',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
    ]
