# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('agglobal', '0023_auto_20150904_1553'),
    ]

    operations = [
        migrations.CreateModel(
            name='User_rel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('from_user_type', models.CharField(max_length=50, null=True, blank=True)),
                ('to_user_type', models.CharField(max_length=50, null=True, blank=True)),
                ('like', models.CharField(max_length=1, null=True, blank=True)),
                ('follow', models.CharField(max_length=1, null=True, blank=True)),
                ('from_user', models.ForeignKey(related_name='user_rel_from_user', to=settings.AUTH_USER_MODEL)),
                ('to_user', models.ForeignKey(related_name='user_rel_to_user', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
