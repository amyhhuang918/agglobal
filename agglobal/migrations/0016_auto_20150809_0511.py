# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import agglobal.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('agglobal', '0015_user_exp_description'),
    ]

    operations = [
        migrations.RenameField(
            model_name='company',
            old_name='logo',
            new_name='business_plan',
        ),
        migrations.RenameField(
            model_name='company',
            old_name='company_name',
            new_name='invested_status',
        ),
        migrations.RenameField(
            model_name='company',
            old_name='funding_range',
            new_name='investment_budget',
        ),
        migrations.RenameField(
            model_name='company',
            old_name='funding_status',
            new_name='project_name',
        ),
        migrations.RemoveField(
            model_name='company',
            name='end_month',
        ),
        migrations.RemoveField(
            model_name='company',
            name='end_year',
        ),
        migrations.RemoveField(
            model_name='company',
            name='start_month',
        ),
        migrations.RemoveField(
            model_name='company',
            name='start_year',
        ),
        migrations.AddField(
            model_name='company',
            name='company_logo',
            field=models.FileField(storage=agglobal.models.OverwriteStorage(), null=True, upload_to=b'', blank=True),
        ),
        migrations.AddField(
            model_name='company',
            name='company_site',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='company',
            name='introduction',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='company',
            name='product_img1',
            field=models.FileField(storage=agglobal.models.OverwriteStorage(), null=True, upload_to=b'', blank=True),
        ),
        migrations.AddField(
            model_name='company',
            name='product_img2',
            field=models.FileField(storage=agglobal.models.OverwriteStorage(), null=True, upload_to=b'', blank=True),
        ),
        migrations.AddField(
            model_name='company',
            name='product_img3',
            field=models.FileField(storage=agglobal.models.OverwriteStorage(), null=True, upload_to=b'', blank=True),
        ),
        migrations.AddField(
            model_name='company',
            name='product_img4',
            field=models.FileField(storage=agglobal.models.OverwriteStorage(), null=True, upload_to=b'', blank=True),
        ),
        migrations.AddField(
            model_name='company',
            name='user',
            field=models.ForeignKey(db_column=b'userid', default=1, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
    ]
