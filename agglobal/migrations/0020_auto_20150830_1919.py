# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agglobal', '0019_auto_20150830_1518'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='company_financing',
            name='file',
        ),
        migrations.RemoveField(
            model_name='company_investors',
            name='file',
        ),
        migrations.RemoveField(
            model_name='company_members',
            name='file',
        ),
    ]
