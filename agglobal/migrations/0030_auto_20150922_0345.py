# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('agglobal', '0029_auto_20150920_2315'),
    ]

    operations = [
        migrations.CreateModel(
            name='User_company_rel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('user_type', models.CharField(max_length=50, null=True, blank=True)),
                ('like', models.CharField(max_length=1, null=True, blank=True)),
                ('follow', models.CharField(max_length=1, null=True, blank=True)),
                ('company', models.ForeignKey(to='agglobal.Company')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, db_column=b'userid')),
            ],
        ),
        migrations.RemoveField(
            model_name='user_company_ref',
            name='company',
        ),
        migrations.RemoveField(
            model_name='user_company_ref',
            name='user',
        ),
        migrations.DeleteModel(
            name='User_company_ref',
        ),
    ]
