# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import agglobal.models


class Migration(migrations.Migration):

    dependencies = [
        ('agglobal', '0016_auto_20150809_0511'),
    ]

    operations = [
        migrations.CreateModel(
            name='Company_financing',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('financeing_title', models.CharField(max_length=100, null=True, blank=True)),
                ('start_year', models.CharField(max_length=50, null=True, blank=True)),
                ('start_month', models.CharField(max_length=50, null=True, blank=True)),
                ('end_year', models.CharField(max_length=50, null=True, blank=True)),
                ('end_month', models.CharField(max_length=50, null=True, blank=True)),
                ('introduction', models.TextField(null=True, blank=True)),
                ('file', models.FileField(storage=agglobal.models.OverwriteStorage(), null=True, upload_to=b'', blank=True)),
                ('file_name', models.CharField(max_length=100, null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Company_investors',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, null=True, blank=True)),
                ('position', models.CharField(max_length=100, null=True, blank=True)),
                ('introduction', models.TextField(null=True, blank=True)),
                ('file', models.FileField(storage=agglobal.models.OverwriteStorage(), null=True, upload_to=b'', blank=True)),
                ('file_name', models.CharField(max_length=100, null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Company_members',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, null=True, blank=True)),
                ('position', models.CharField(max_length=100, null=True, blank=True)),
                ('introduction', models.TextField(null=True, blank=True)),
                ('file', models.FileField(storage=agglobal.models.OverwriteStorage(), null=True, upload_to=b'', blank=True)),
                ('file_name', models.CharField(max_length=100, null=True, blank=True)),
            ],
        ),
    ]
