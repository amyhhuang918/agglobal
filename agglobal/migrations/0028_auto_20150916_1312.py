# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agglobal', '0027_auto_20150906_1720'),
    ]

    operations = [
        migrations.AddField(
            model_name='investorprofile',
            name='certified',
            field=models.CharField(max_length=1, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='message',
            name='parentid',
            field=models.IntegerField(db_index=True, null=True, blank=True),
        ),
    ]
