# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('agglobal', '0024_user_rel'),
    ]

    operations = [
        migrations.AddField(
            model_name='message',
            name='parentid',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='message',
            name='reciever',
            field=models.ForeignKey(related_name='message_reciever', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='message',
            name='sender',
            field=models.ForeignKey(related_name='message_sender', to=settings.AUTH_USER_MODEL),
        ),
    ]
