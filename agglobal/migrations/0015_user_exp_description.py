# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agglobal', '0014_auto_20150804_1357'),
    ]

    operations = [
        migrations.AddField(
            model_name='user_exp',
            name='description',
            field=models.TextField(null=True, blank=True),
        ),
    ]
