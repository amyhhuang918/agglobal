# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('agglobal', '0022_auto_20150902_0539'),
    ]

    operations = [
        migrations.AddField(
            model_name='message',
            name='senttime',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 4, 15, 52, 33, 187631, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='message',
            name='reciever',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='message',
            name='sender',
            field=models.CharField(max_length=50),
        ),
    ]
