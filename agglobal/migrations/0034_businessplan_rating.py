# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('agglobal', '0033_auto_20151007_0121'),
    ]

    operations = [
        migrations.CreateModel(
            name='Businessplan_rating',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('business_model', models.CharField(max_length=1, null=True, blank=True)),
                ('expandibility', models.CharField(max_length=1, null=True, blank=True)),
                ('innovativeness', models.CharField(max_length=1, null=True, blank=True)),
                ('projectteam', models.CharField(max_length=1, null=True, blank=True)),
                ('usercomment', models.CharField(max_length=200, null=True, blank=True)),
                ('company', models.ForeignKey(to='agglobal.Company')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, db_column=b'userid')),
            ],
        ),
    ]
