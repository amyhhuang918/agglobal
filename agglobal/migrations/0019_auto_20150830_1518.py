# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agglobal', '0018_auto_20150830_1505'),
    ]

    operations = [
        migrations.RenameField(
            model_name='company_financing',
            old_name='financeing_title',
            new_name='financing_title',
        ),
    ]
