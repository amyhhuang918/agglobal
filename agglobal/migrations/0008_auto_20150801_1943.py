# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agglobal', '0007_auto_20150801_1711'),
    ]

    operations = [
        migrations.RenameField(
            model_name='investorprofile',
            old_name='profile_logo',
            new_name='profile_img',
        ),
        migrations.RenameField(
            model_name='userprofile',
            old_name='profile_logo',
            new_name='profile_img',
        ),
    ]
