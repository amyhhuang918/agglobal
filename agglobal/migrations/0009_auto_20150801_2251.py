# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import agglobal.models


class Migration(migrations.Migration):

    dependencies = [
        ('agglobal', '0008_auto_20150801_1943'),
    ]

    operations = [
        migrations.AddField(
            model_name='document',
            name='seq',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='company',
            name='logo',
            field=models.FileField(storage=agglobal.models.OverwriteStorage(), null=True, upload_to=b'documents', blank=True),
        ),
        migrations.AlterField(
            model_name='document',
            name='docfile',
            field=models.FileField(storage=agglobal.models.OverwriteStorage(), upload_to=b'documents'),
        ),
        migrations.AlterField(
            model_name='invested_project',
            name='attached_project_file',
            field=models.FileField(storage=agglobal.models.OverwriteStorage(), null=True, upload_to=b'documents', blank=True),
        ),
        migrations.AlterField(
            model_name='investment_experiences',
            name='attached_project_file',
            field=models.FileField(storage=agglobal.models.OverwriteStorage(), null=True, upload_to=b'documents', blank=True),
        ),
        migrations.AlterField(
            model_name='investorprofile',
            name='profile_img',
            field=models.FileField(storage=agglobal.models.OverwriteStorage(), null=True, upload_to=b'documents', blank=True),
        ),
        migrations.AlterField(
            model_name='user_exp',
            name='attached_file',
            field=models.FileField(storage=agglobal.models.OverwriteStorage(), null=True, upload_to=b'documents', blank=True),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='profile_img',
            field=models.FileField(storage=agglobal.models.OverwriteStorage(), null=True, upload_to=b'documents', blank=True),
        ),
    ]
