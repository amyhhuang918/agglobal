# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agglobal', '0010_auto_20150802_1700'),
    ]

    operations = [
        migrations.RenameField(
            model_name='investment_experiences',
            old_name='invest_status',
            new_name='invest_a_status',
        ),
        migrations.AddField(
            model_name='investment_experiences',
            name='invest_angel_status',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='investment_experiences',
            name='invest_b_status',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='investment_experiences',
            name='invest_c_status',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='investment_experiences',
            name='invest_o_status',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
    ]
