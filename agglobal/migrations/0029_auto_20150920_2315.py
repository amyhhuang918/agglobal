# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agglobal', '0028_auto_20150916_1312'),
    ]

    operations = [
        migrations.AlterField(
            model_name='investorprofile',
            name='investment_a_status',
            field=models.CharField(default=b'N', max_length=100, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='investorprofile',
            name='investment_angel_status',
            field=models.CharField(default=b'N', max_length=100, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='investorprofile',
            name='investment_b_after_status',
            field=models.CharField(default=b'N', max_length=100, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='investorprofile',
            name='investment_b_status',
            field=models.CharField(default=b'N', max_length=100, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='investorprofile',
            name='investment_pre_a_status',
            field=models.CharField(default=b'N', max_length=100, null=True, blank=True),
        ),
    ]
