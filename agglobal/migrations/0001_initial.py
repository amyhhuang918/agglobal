# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('company_id', models.AutoField(serialize=False, primary_key=True)),
                ('company_name', models.CharField(max_length=100, null=True, blank=True)),
                ('start_date', models.DateField(null=True, blank=True)),
                ('end_date', models.DateField(null=True, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
                ('location', models.CharField(max_length=100, null=True, blank=True)),
                ('industry', models.CharField(max_length=100, null=True, blank=True)),
                ('funding_status', models.CharField(max_length=100, null=True, blank=True)),
                ('funding_range', models.CharField(max_length=100, null=True, blank=True)),
                ('logo', models.FilePathField(null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('docfile', models.FileField(upload_to=b'documents/%Y/%m/%d')),
            ],
        ),
        migrations.CreateModel(
            name='Invested_project',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('project_name', models.CharField(max_length=100, null=True, blank=True)),
                ('invest_date', models.DateField(null=True, blank=True)),
                ('invest_status', models.CharField(max_length=100, null=True, blank=True)),
                ('project_detail', models.TextField(null=True, blank=True)),
                ('attached_project_file', models.FilePathField(null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Investment_experiences',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('investment_field', models.CharField(max_length=100, null=True, blank=True)),
                ('investment_location', models.CharField(max_length=100, null=True, blank=True)),
                ('investment_budget', models.CharField(max_length=100, null=True, blank=True)),
                ('project_name', models.CharField(max_length=100, null=True, blank=True)),
                ('invest_date', models.DateField(null=True, blank=True)),
                ('invest_status', models.CharField(max_length=100, null=True, blank=True)),
                ('invested_status', models.CharField(max_length=100, null=True, blank=True)),
                ('project_detail', models.TextField(null=True, blank=True)),
                ('attached_project_file', models.FilePathField(null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Investment_plan',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('investment_field', models.CharField(max_length=100, null=True, blank=True)),
                ('investment_location', models.CharField(max_length=100, null=True, blank=True)),
                ('investment_budget', models.CharField(max_length=100, null=True, blank=True)),
                ('investment_status', models.CharField(max_length=100, null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='InvestorProfile',
            fields=[
                ('investor_id', models.AutoField(serialize=False, primary_key=True)),
                ('first_name', models.CharField(max_length=100, null=True, blank=True)),
                ('last_name', models.CharField(max_length=100, null=True, blank=True)),
                ('email', models.EmailField(max_length=254)),
                ('country', models.CharField(max_length=100, null=True, blank=True)),
                ('city', models.CharField(max_length=100, null=True, blank=True)),
                ('industryofconcern', models.CharField(max_length=100, null=True, blank=True)),
                ('company_name', models.CharField(max_length=100, null=True, blank=True)),
                ('job_title', models.CharField(max_length=100, null=True, blank=True)),
                ('profession', models.CharField(max_length=100, null=True, blank=True)),
                ('industry', models.CharField(max_length=100, null=True, blank=True)),
                ('introduction', models.TextField(null=True, blank=True)),
                ('personal_profile', models.TextField(null=True, blank=True)),
                ('profile_logo', models.FilePathField(null=True, blank=True)),
                ('activation_key', models.CharField(max_length=40)),
                ('key_expires', models.DateTimeField()),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, db_column=b'userid')),
            ],
        ),
        migrations.CreateModel(
            name='User_company_ref',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('user_title', models.CharField(max_length=100, null=True, blank=True)),
                ('company', models.ForeignKey(to='agglobal.Company')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, db_column=b'userid')),
            ],
        ),
        migrations.CreateModel(
            name='User_exp',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('company_name', models.CharField(max_length=100, null=True, blank=True)),
                ('job_title', models.CharField(max_length=100, null=True, blank=True)),
                ('location', models.CharField(max_length=100, null=True, blank=True)),
                ('start_time', models.DateField(null=True, blank=True)),
                ('end_time', models.DateField(null=True, blank=True)),
                ('attached_file', models.FilePathField(null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('user_profile_id', models.AutoField(serialize=False, primary_key=True)),
                ('first_name', models.CharField(max_length=100, null=True, blank=True)),
                ('last_name', models.CharField(max_length=100, null=True, blank=True)),
                ('email', models.EmailField(max_length=254)),
                ('company_name', models.CharField(max_length=100, null=True, blank=True)),
                ('domain', models.CharField(max_length=100, null=True, blank=True)),
                ('introduction', models.TextField(null=True, blank=True)),
                ('job_title', models.CharField(max_length=100, null=True, blank=True)),
                ('personal_profile', models.TextField(null=True, blank=True)),
                ('city', models.CharField(max_length=100, null=True, blank=True)),
                ('country', models.CharField(max_length=100, null=True, blank=True)),
                ('industry', models.CharField(max_length=100, null=True, blank=True)),
                ('industryofconcern', models.CharField(max_length=100, null=True, blank=True)),
                ('profession', models.CharField(max_length=100, null=True, blank=True)),
                ('profile_logo', models.FilePathField(null=True, blank=True)),
                ('activation_key', models.CharField(max_length=40)),
                ('key_expires', models.DateTimeField()),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, db_column=b'userid')),
            ],
        ),
        migrations.AddField(
            model_name='user_exp',
            name='user',
            field=models.ForeignKey(to='agglobal.UserProfile'),
        ),
        migrations.AddField(
            model_name='investment_plan',
            name='investor',
            field=models.ForeignKey(to='agglobal.InvestorProfile'),
        ),
        migrations.AddField(
            model_name='investment_experiences',
            name='investor',
            field=models.ForeignKey(to='agglobal.InvestorProfile'),
        ),
        migrations.AddField(
            model_name='invested_project',
            name='investor',
            field=models.ForeignKey(to='agglobal.InvestorProfile'),
        ),
    ]
