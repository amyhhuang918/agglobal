# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agglobal', '0021_message'),
    ]

    operations = [
        migrations.AddField(
            model_name='message',
            name='deleted',
            field=models.CharField(max_length=1, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='message',
            name='read',
            field=models.CharField(max_length=1, null=True, blank=True),
        ),
    ]
