# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import agglobal.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('agglobal', '0009_auto_20150801_2251'),
    ]

    operations = [
        migrations.CreateModel(
            name='User_investor_ref',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.AlterField(
            model_name='company',
            name='logo',
            field=models.FileField(storage=agglobal.models.OverwriteStorage(), null=True, upload_to=b'', blank=True),
        ),
        migrations.AlterField(
            model_name='document',
            name='docfile',
            field=models.FileField(storage=agglobal.models.OverwriteStorage(), upload_to=b''),
        ),
        migrations.AlterField(
            model_name='invested_project',
            name='attached_project_file',
            field=models.FileField(storage=agglobal.models.OverwriteStorage(), null=True, upload_to=b'', blank=True),
        ),
        migrations.AlterField(
            model_name='investment_experiences',
            name='attached_project_file',
            field=models.FileField(storage=agglobal.models.OverwriteStorage(), null=True, upload_to=b'', blank=True),
        ),
        migrations.AlterField(
            model_name='investorprofile',
            name='profile_img',
            field=models.FileField(storage=agglobal.models.OverwriteStorage(), null=True, upload_to=b'', blank=True),
        ),
        migrations.AlterField(
            model_name='user_exp',
            name='attached_file',
            field=models.FileField(storage=agglobal.models.OverwriteStorage(), null=True, upload_to=b'', blank=True),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='profile_img',
            field=models.FileField(storage=agglobal.models.OverwriteStorage(), null=True, upload_to=b'', blank=True),
        ),
        migrations.AddField(
            model_name='user_investor_ref',
            name='investor',
            field=models.ForeignKey(to='agglobal.InvestorProfile'),
        ),
        migrations.AddField(
            model_name='user_investor_ref',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, db_column=b'userid'),
        ),
    ]
