# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agglobal', '0013_auto_20150804_1354'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user_exp',
            name='end_time',
        ),
        migrations.RemoveField(
            model_name='user_exp',
            name='start_time',
        ),
        migrations.AddField(
            model_name='user_exp',
            name='end_month',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='user_exp',
            name='end_year',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='user_exp',
            name='start_month',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='user_exp',
            name='start_year',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
    ]
