# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agglobal', '0020_auto_20150830_1919'),
    ]

    operations = [
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('subject', models.CharField(max_length=500, null=True, blank=True)),
                ('body', models.TextField(null=True, blank=True)),
                ('sender', models.CharField(max_length=50, null=True, blank=True)),
                ('reciever', models.CharField(max_length=50, null=True, blank=True)),
            ],
        ),
    ]
