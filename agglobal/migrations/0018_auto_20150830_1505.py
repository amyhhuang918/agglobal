# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agglobal', '0017_company_financing_company_investors_company_members'),
    ]

    operations = [
        migrations.AddField(
            model_name='company_financing',
            name='company',
            field=models.ForeignKey(default=1, to='agglobal.Company'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='company_investors',
            name='company',
            field=models.ForeignKey(default=1, to='agglobal.Company'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='company_members',
            name='company',
            field=models.ForeignKey(default=1, to='agglobal.Company'),
            preserve_default=False,
        ),
    ]
