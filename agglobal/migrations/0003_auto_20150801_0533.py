# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agglobal', '0002_auto_20150801_0434'),
    ]

    operations = [
        migrations.AddField(
            model_name='document',
            name='created',
            field=models.DateField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='document',
            name='email',
            field=models.EmailField(max_length=254, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='document',
            name='filetype',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
    ]
