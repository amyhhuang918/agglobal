# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agglobal', '0032_auto_20151005_0320'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='businessplan_rating',
            name='company',
        ),
        migrations.DeleteModel(
            name='Businessplan_rating',
        ),
    ]
