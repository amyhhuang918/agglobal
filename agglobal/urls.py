from django.conf.urls import patterns, url
from agglobal.forms import CustomEmailRegistrationForm
from agglobal import views
urlpatterns = patterns('',
    # register user test remove later
    url(r'^agglobal/register_user_test/$',views.register_user_test, name='register_test'),
    url(r'^agglobal/register_user/$',views.register_user, name='register_user'),
    url(r'^agglobal/register_company/$',views.register_company, name='register_company'),
    url(r'^agglobal/register_company_details/$',views.register_company_details, name='register_company_details'),
    url(r'^agglobal/register_investor/$',views.register_investor, name='register_investor'),
    url(r'^agglobal/editprofile/$',views.edit_profile, name='edit_profile'),
    url(r'^agglobal/register_check/$',views.register_check, name='register_check'),
    url(r'^agglobal/register_check_test/$',views.register_check_test, name='register_check_test'),
    url(r'^agglobal/login/$', views.login_user, name='login_user'),
    url(r'^agglobal/login_test/$', views.login_user_test, name='login_user_test'),
    url(r'^agglobal/logout/$', views.logout_user, name='logout_user'),
    url(r'^agglobal/login_linkedin/$', views.login_linkedin, name='login_linkedin'),
    url(r'^agglobal/password_reset/$', views.password_reset, name='password_reset'),
    url(r'^agglobal/psssucess/$', views.psssucess, name='psssucess'),
    url(r'^agglobal/home/$', views.home, name='homepage'),

    url(r'^agglobal/create_startup/$', views.create_startup, name='create_startup'),

    url(r'^agglobal/events/$', views.events, name='events'),
    url(r'^agglobal/list/$', views.list, name='list'),
    url(r'^agglobal/file_upload/$', views.file_upload, name='file_upload'),
    url(r'^agglobal/file_upload2/$', views.file_upload2, name='file_upload2'),
    url(r'^agglobal/file_upload3/$', views.file_upload3, name='file_upload3'),
    url(r'^agglobal/file_upload4/$', views.file_upload4, name='file_upload4'),
    url(r'^agglobal/query_investor/$', views.query_investor, name='query_investor'),
    url(r'^agglobal/queryall_investor/$', views.queryall_investor, name='queryall_investor'),
    url(r'^agglobal/queryfilter_investor/$', views.queryfilter_investor, name='queryfilter_investor'),
    url(r'^agglobal/query_user/$', views.query_user, name='query_user'),
    url(r'^agglobal/queryall_user/$', views.queryall_user, name='queryall_user'),
    url(r'^agglobal/query_company/((?P<startupid>\d+)/)?$', views.query_company, name='query_company'),
    url(r'^agglobal/queryallcompany_user/$', views.queryallcompany_user, name='queryallcompany_user'),
    url(r'^agglobal/queryall_company/$', views.queryall_company, name='queryall_company'),
    url(r'^agglobal/queryfilter_company/$', views.queryfilter_company, name='queryfilter_company'),
    url(r'^agglobal/query_test/$', views.query_test, name='query_test'),
    url(r'^agglobal/query_follow/$', views.query_follow, name='query_follow'),
    url(r'^agglobal/query_befollowed/$', views.query_befollowed, name='query_befollowed'),
    url(r'^agglobal/multiquery_test/$', views.multiquery_test, name='multiquery_test'),
    url(r'^agglobal/register_investor_test/$',views.register_investor_test, name='register_investor_test'),
    # BP API
    url(r'^agglobal/query_bp/(?P<companyid>\d+)$', views.query_bp, name='query_bp'),
    url(r'^agglobal/queryall_bpreviews/(?P<companyid>\d+)$', views.queryall_bpreviews, name='queryall_bpreviews'),
    url(r'^agglobal/submitBp/$', views.submit_bp, name='submit_bp'),
    url(r'^agglobal/reviewBp/$', views.review_bp, name='review_bp'),
    url(r'^agglobal/bpupload_success/$', views.bpupload_success, name='bpupload_success'),
    url(r'^agglobal/postbp_rating/$', views.postbp_rating, name='postbp_rating'),

    # emails API
    url(r'^agglobal/allSentMessages/(?P<userid>\d+)$', views.get_all_sent_message, name='get_all_sent_message'),
    url(r'^agglobal/allMessages/((?P<userid>\d+)/)?$', views.get_all_message, name='get_all_message'),
    url(r'^agglobal/messageChain/((?P<messageid>\d+)/)?$', views.get_message_chain, name='get_message_chain'),
    url(r'^agglobal/lastMessage/((?P<messageid>\d+)/)?$', views.get_lastmessage, name='get_lastmessage'),
    url(r'^agglobal/newMessage/((?P<parentid>\d+)/)?$', views.post_new_message, name='post_new_message'),
    url(r'^agglobal/deleteMessage/(?P<messageid>\d+)$', views.mark_message_delete, name='mark_message_delete'),
    url(r'^agglobal/readMessage/(?P<messageid>\d+)$', views.mark_message_read, name='mark_message_read'),

    # profile API
    url(r'^agglobal/settings/$', views.user_settings, name='user_settings'),
    url(r'^agglobal/startups/$', views.search_startups, name='search_startups'),
    url(r'^agglobal/investors/$', views.search_investors, name='search_investors'),

    url(r'^agglobal/viewUserProfile/(?P<userid>\d+)$', views.view_user_profile, name='view_user_profile'),
    url(r'^agglobal/viewInvProfile/(?P<userid>\d+)$', views.view_investor_profile, name='view_investor_profile'),
    url(r'^agglobal/viewStartup/((?P<startupid>\d+)/)?$', views.view_startup_page, name='view_startup_page'),
    url(r'^agglobal/emails/$', views.view_email_page, name='view_email_page'),
    #post rating API
    url(r'^agglobal/postlike/$', views.post_like, name='post_like'),
    url(r'^agglobal/postlike_company/$', views.post_company_like, name='post_company_like'),

)
