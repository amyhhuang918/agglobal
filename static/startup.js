var all = 1;

$(document).ready(function(){
    loadInfo(); 
    $('#like').on('click',function (e){
        e.preventDefault();
        like();
    });
    $('#follow').on('click',function (e){
        e.preventDefault();
        follow();
    });
    $('#message-submit').on('click',function (event){
        event.preventDefault();
        var msgToSend = {
            "subject": "test",
            "sender": "47",
            "receiver": "46",
            "body": $('#my-msg').val(),
            "parentid": null
        };
        $.ajax({
            url: 'http://ai22.mooo.com:8080/agglobal/newMessage/',
            data: JSON.stringify(msgToSend),
            dataType: 'json',
            type: 'POST',
            crossDomain: true,
            success: function(msgs) {
                $('#sendMessage').modal('toggle')
            },
            error: function(err) {
                console.log(err);
            }
        });
        
    });
});

function loadInfo() {
    $.ajax({
        url: '/agglobal/query_company/',
        //crossDomain: true,
        //data: session,
        type: 'GET',
        dataType:'json',
        success: function(res) {
            console.log(res);
            // var owls = {
            //   "owl" : [
            //       {"item" : "<div class='overlay-container margin-top-clear'><img src='"+res.data.product_img1+"'><a href='test/1.jpg' class='popup-img            overlay' title='image title'><i class='fa fa-search-plus'></i></a></div>"},
            //       {"item" : "<div class='overlay-container margin-top-clear'><img src='test/2.jpg'><a href='test/2.jpg' class='popup-img            overlay' title='image title'><i class='fa fa-search-plus'></i></a></div>"},
            //       {"item" : "<div class='overlay-container margin-top-clear'><img src='test/3.jpg'><a href='test/3.jpg' class='popup-img            overlay' title='image title'><i class='fa fa-search-plus'></i></a></div>"},
            //       {"item" : "<div class='overlay-container margin-top-clear'><img src='test/4.jpg'><a href='test/4.jpg' class='popup-img            overlay' title='image title'><i class='fa fa-search-plus'></i></a></div>"}
            //   ]
            // };
            $("#owl-demo").owlCarousel({
                singleItem: true,
                autoPlay: false,
                navigation: true,
                navigationText: false,
                pagination: true,           
                jsonPath : "iistartups2.txt" 
            });
            //$('#owl-carousel-contain').append("<div class='overlay-container margin-top-clear'><img src='images/startup-img/about-1.jpg' alt=''><a href='"+res.data.product_img1+"' class='popup-img overlay' title='image title'><i class='fa fa-search-plus'></i></a></div>");
            $('#result').html('');
        },
        error: function() { alert('连接数据库失败！');  }
    });
}

function like() {
    var likeTotal = parseInt($('#like_total').text());
     var txt = $('#thumb').text();
     var action = '';

     if (txt == '赞') {

           likeTotal = likeTotal + 1;
           $('#thumb').text('取消赞');
           action = 'like';
     }
     else {
           likeTotal = likeTotal - 1;
           $('#thumb').text('赞');
           action = 'unlike';
     }
    data = {
        action: action,
        //userid: $('#cuserid').val()
        companyid: ag_companyId
    };
        $.ajax(
            {
                type: 'POST',
                async: true,
                cache: false,
                dataType: 'json',
                url: '/agglobal/postlike_company/',
                data: JSON.stringify(data),
                error: function(request, status, error) {
                    console.error(request.responseText);
                },
                success: function(result) {
                   console.log(result);
                }
            }
        );

   document.getElementById('like_total').innerHTML = likeTotal;
}
function follow() {
     var txt = $('#follow_txt').text();
     var action = '';
     if (txt == '关注') {
           $('#follow_txt').text('取消关注');
           action = 'followup';
     }
     else {
           $('#follow_txt').text('关注');
           action = 'unfollowup';
     }
    data = {
        action: action,
        //userid: $('#cuserid').val()
        companyid: ag_companyId
    };
        $.ajax(
            {
                type: 'POST',
                async: true,
                cache: false,
                dataType: 'json',
                url: '/agglobal/postlike_company/',
                data: JSON.stringify(data),
                error: function(request, status, error) {
                    console.error(request.responseText);
                },
                success: function(result) {
                   console.log(result);
                }
            }
        );
}
