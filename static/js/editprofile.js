/**
$.fn.editable.defaults.mode = 'inline';
$.fn.editable.defaults.showbuttons = false;
$.fn.editable.defaults.url = '/agglobal/editprofile/';
$.fn.editable.defaults.type = 'text';
$.fn.editable.defaults.disable = true;
**/

function followup() {
    data = {
        action: "followup",
        userid: $('#cuserid').val()
    };

    $.ajax(
        {
            type: "POST",
            async: true,
            cache: false,
            dataType: "json",
            url: '/agglobal/postlike/',
            data: JSON.stringify(data),
            error: function(request, status, error) {
                console.error(request.responseText);
            },
            success: function(result) {
               console.log(result);
               window.location.reload();
            }
        }
    );
}

function like() {
    var likeTotal = parseInt($('#like_total').text());
     var txt = $('#like').text();
     var action = "";

     if (txt == '赞') {

           likeTotal = likeTotal + 1;
           $('#like').text('取消赞');
           action = "like";
     }
     else {
           likeTotal = likeTotal - 1;
           $('#like').text('赞');
           action = "unlike";
     }
    data = {
        action: action,
        userid: $('#cuserid').val()
    };

        $.ajax(
            {
                type: "POST",
                async: true,
                cache: false,
                dataType: "json",
                url: '/agglobal/postlike/',
                data: JSON.stringify(data),
                error: function(request, status, error) {
                    console.error(request.responseText);
                },
                success: function(result) {
                   console.log(result);
                }
            }
        );

   document.getElementById("like_total").innerHTML = likeTotal;
}

$('#addmoreusrexp').click(function(e){
    var id = $(".usr_exp_class:last").attr('id');
    var newid = parseInt(id) + 1;
    var cloneobj = $(".usr_exp_class:last").clone().attr('id', newid);
    // company name
    cloneobj.find( '#company_name' + id).attr("id", "company_name" + newid).val("");
    // job title
    cloneobj.find( '#job_title' + id).attr("id", "job_title" + newid).val("");
    // location
    cloneobj.find( '#location' + id).attr("id", "location" + newid).val("");
    // start year
    cloneobj.find( '#start_year' + id).attr("id", "start_year" + newid).prop("selectedIndex",-1);
    // start month
    cloneobj.find( '#start_month' + id).attr("id", "start_month" + newid).prop("selectedIndex",-1);
    // end year
    cloneobj.find( '#end_year' + id).attr("id", "end_year" + newid).prop("selectedIndex",-1);
    // end month
    cloneobj.find( '#end_month' + id).attr("id", "end_month" + newid).prop("selectedIndex",-1);
    //description
    cloneobj.find( '#description' + id).attr("id", "description" + newid).empty();
    cloneobj.find( '#attach_file' + id).attr("id", "attach_file" + newid).empty();

    cloneobj.insertAfter(".usr_exp_class:last");
});

$('#addmoreinvexp').click(function(e){
    var id = $(".inv_exp_class:last").attr('id');
    var newid = parseInt(id) + 1;
    var cloneobj = $(".inv_exp_class:last").clone().attr('id', newid);
    // project name
    cloneobj.find( '#project_name' + id).attr("id", "project_name" + newid).val("");

    // invested status
    cloneobj.find( '#invested_status' + id).attr("id", "invested_status" + newid).prop("selectedIndex",-1);

    // start year
    cloneobj.find( '#invest_year' + id).attr("id", "invest_year" + newid).prop("selectedIndex",-1);
    // start month
    cloneobj.find( '#invest_month' + id).attr("id", "invest_month" + newid).prop("selectedIndex",-1);
    //project_detail
    cloneobj.find( '#project_detail' + id).attr("id", "project_detail" + newid).empty();
    cloneobj.find( '#attach_file' + id).attr("id", "attach_file" + newid).empty();

    cloneobj.insertAfter(".inv_exp_class:last");
});

$('#edit1save').click(function(e){
    //e.preventDefault();
    var edit1data = {
      id: $(this).data("pk"),
      last_name: $('#last_name').val(),
      industry: $('#industry').val(),
      industryofconcern: $('#industryofconcern').val(),
      profession: $('#profession').val(),
      city: $('#city').val(),
      country: $('#country').val(),
      introduction: $('#introduction').val(),
      profession: $('#profession').val(),
      company_name: $('#company_name').val(),
    }
    $.ajax(
        {
            type: "POST",
            async: true,
            cache: false,
            dataType: "json",
            url: '/agglobal/editprofile/',
            data: edit1data,
            error: function(request, status, error) {
            },
            success: function(result) {
               $('#edit1').modal('toggle');
               window.location.reload();
            }
        }
    );
});

$('#edit2').click(function(e){
    e.stopPropagation();
    e.preventDefault();
    $('#pprofile').editable({
        type: 'textarea',
        url: '/agglobal/editprofile/',
        name: "personal_profile",
        emptytext: '',
        showbuttons: "bottom",
        tpl: "<input type='textarea' style='width: 400px;height: 120px'>",
        rows: 7
    });

    $('#pprofile').editable('toggle');
});

$('#uedit3save').click(function(e){
    var edit3data = {usr_exp_list:[]};


    $(".usr_exp_class").each(function() {
        var jobexpid = this.id;
        edit3data.usr_exp_list.push({
            usr_exp_id: jobexpid,
            company_name: $('#company_name'+jobexpid).val(),
            description: $('#description'+jobexpid).val(),
            start_year: $('#start_year'+jobexpid).val(),
            start_month: $('#start_month'+jobexpid).val(),
            end_year: $('#start_year'+jobexpid).val(),
            end_month: $('#start_month'+jobexpid).val(),
            location: $('#location'+jobexpid).val(),
            job_title: $('#job_title' + jobexpid).val(),
            attached_file:$('#attach_file' + jobexpid).val()
        });
    });

        $.ajax(
            {
                type: "POST",
                async: true,
                cache: false,
                dataType: "json",
                url: '/agglobal/editprofile/',
                data: edit3data,
                error: function(request, status, error) {
                    console.log(error);
                    console.log(status);


                    console.error(request.responseText);
                },
                success: function(result) {
                   console.log(result);
                   $('#edit3').modal('toggle');
                }
            }
        );


});

$('#edit3save').click(function(e){
    var edit3data = {inv_exp_list:[]};


    $(".inv_exp_class").each(function() {
        var jobexpid = this.id;
        edit3data.inv_exp_list.push({
            inv_exp_id: jobexpid,
            project_name: $('#project_name'+jobexpid).val(),
            project_detail: $('#project_detail'+jobexpid).val(),
            invested_status: $('#invested_status'+jobexpid).val(),
            invest_year: $('#invest_year'+jobexpid).val(),
            invest_month: $('#invest_month'+jobexpid).val(),
            attached_file:$('#attach_file' + jobexpid).val()
        });
    });

        $.ajax(
            {
                type: "POST",
                async: true,
                cache: false,
                dataType: "json",
                url: '/agglobal/editprofile/',
                data: edit3data,
                error: function(request, status, error) {
                    console.error(request.responseText);
                },
                success: function(result) {
                   console.log(result);
                   $('#edit3').modal('toggle');
                   window.location.reload();
                }
            }
        );


});

$('#edit4save').click(function(e){
    //e.preventDefault();

    var edit4data = {
      id: $(this).data("pk"),
      userType: "investor",
      investment_field: $('#investment_field').val(),
      investment_location: $('#investment_location').val(),
      investment_budget: $('#investment_budget').val(),
    }
    
    $(".investstatus").each(function() {
	if ($(this).is(':checked')) {
        	edit4data[ $(this).data('id')] = "Y";
	} else {
        	edit4data[ $(this).data('id')] = "N";
        }
    });
    $.ajax(
        {
            type: "POST",
            async: true,
            cache: false,
            dataType: "json",
            url: '/agglobal/editprofile/',
            data: edit4data,
            error: function(request, status, error) {
                console.error(request.responseText);
            },
            success: function(result) {
               console.log(result);
               $('#edit4').modal('toggle');
  	       window.location.reload();
            }
        }
    );
});

 $("#inv_exps_form").submit(function(){
   event.preventDefault();

   var formData = new FormData();
   $('.company_files_logo').each(function() {
	if (this.files[0]) {
        	formData.append(this.files[0].name, this.files[0]);
	}
   });


   $.ajax({
     url: '/agglobal/file_upload2/',
     type: 'POST',
     data: formData,
     async: false,
     cache: false,
     contentType: false,
     enctype: "multipart/form-data",
     processData: false,
     success: function (response) {
	console.log("inv exp form succeed");
     },
                error: function(request, status, error) {
		    alert("upload " + error);
                    console.error(request.responseText);
                },
   });
 });

$('#uploadProfileImg').change(function(e){

   var formData = new FormData();
   formData.append($('#uploadProfileImg').prop('files')[0].name, $('#uploadProfileImg').prop('files')[0]);
    $.ajax({
         url: '/agglobal/file_upload3/',
         type: 'POST',
         data: formData,
         async: false,
         cache: false,
         contentType: false,
         enctype: "multipart/form-data",
         processData: false,
         success: function (json) {
            $("#profileImg").attr("src",json.file_name);
         }
    });
});

