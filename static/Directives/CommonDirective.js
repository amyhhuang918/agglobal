﻿(function () {
    'use strict';

    //var INTEGER_REGEXP = /^\-?\d+$/;
    var commonDirective = angular.module('commonDirective', []);
    commonDirective.directive('capitalize', [
        function($parse) {
            return {
                require: 'ngModel',
                link: function(scope, element, attrs, modelCtrl) {
                    var capitalize = function(inputValue) {
                        if (inputValue == undefined) inputValue = '';
                        var capitalized = inputValue.toUpperCase();
                        if (capitalized !== inputValue) {
                            modelCtrl.$setViewValue(capitalized);
                            modelCtrl.$render();
                        }
                        return capitalized;
                    };
                    var model = $parse(attrs.ngModel);
                    modelCtrl.$parsers.push(capitalize);
                    capitalize(model); // capitalize initial value
                }
            };
        }
    ]).directive("compareTo", function () {
        return {
            require: "ngModel",
            scope: {
                otherModelValue: "=compareTo"
            },
            link: function (scope, element, attributes, ngModel) {

                ngModel.$validators.compareTo = function (modelValue) {
                    return modelValue == scope.otherModelValue;
                };

                scope.$watch("otherModelValue", function () {
                    ngModel.$validate();
                });
            }
        };
    }).directive("popoverHtmlUnsafe", [
        "$tooltip", function($tooltip) {
            return $tooltip("popoverHtmlUnsafe", "popover", "click");
        }
    ]).directive('wfSetfocus', function ($timeout) {
        /*
        focus the input element on form load
        */
        return {
            link: function(scope, element, attrs) {
                $timeout(function() {
                    element.focus();
                }, 250);
            }
        }
    }).directive('wfAutoNext', function ($timeout) {
        /*
        auto-advance to the next visible input element once the current element is complete (length=maxlength)
        */
        return {
            restrict: "A",
            link: function($scope, element) {
                element.on("input", function(e) {
                    if (element.val().length == element.attr("maxlength")) {

                        var inputFields = $("input,textarea,button,a,select");
                        var curr = inputFields.index(element);
                        var next=null;
                        if (curr <= inputFields.length - 2) {
                            next = inputFields[curr + 1];
                        } else {
                            // there isn't a next field to go to so exit
                            return true;
                        }
                        var c = curr+1;

                        //wait one cycle so any ng-show/ng-hide events can get evaluated
                        $timeout(function () {
                                
                            //if the next input field is hidden, skip it
                            while (!$(next).is(":visible")) {                                    
                                c++;
                                if (c <= inputFields.length - 1) {
                                    next = inputFields[c];
                                } else {                                        
                                    // there isn't a next field to go to so exit
                                    return true;
                                }                                                                        
                            }

                            if (next != null) {
                                //if we programmatically focus, the blur event isn't triggered, so fire it here                                
                                element.blur();
                                next.focus();
                            }
                        }, 0);
                    }
                                          
                });

            }
        }
    });
    

    /*
    angular.module('app-ag.shared-dialog').directive('wfSetfocus', function ($timeout) {
        return {
            link: function (scope, element, attrs) {
                $timeout(function() {
                    element.focus(); 
                },250);
            }
        };
    });

    angular.module('app-ag.shared-dialog').directive('wfAutoNext', function ($timeout) {
        return {
            restrict: "A",
            link: function($scope, element) {
                element.on("input", function(e) {
                    if (element.val().length == element.attr("maxlength")) {

                        var inputFields = $("input,textarea,button,a,select");
                        var curr = inputFields.index(element);
                        var next=null;
                        if (curr <= inputFields.length - 2) {
                            next = inputFields[curr + 1];
                        } else {
                            // there isn't a next field to go to so exit
                            return true;
                        }
                        var c = curr+1;

                            //wait one cycle so any ng-show/ng-hide events can get evaluated
                            $timeout(function () {
                                
                                //if the next input field is hidden, skip it
                                while (!$(next).is(":visible")) {                                    
                                    c++;
                                    if (c <= inputFields.length - 1) {
                                        next = inputFields[c];
                                    } else {                                        
                                        // there isn't a next field to go to so exit
                                        return true;
                                    }                                                                        
                                }

                                if (next != null) {
                                    //if we programmatically focus, the blur event isn't triggered, so fire it here                                
                                    element.blur();
                                    next.focus();
                                }
                            }, 0);
                        }
                                          
                });

            }
        }
    });

    */


    // directive('validate', function () {
    //    return function(scope, el, attrs) {
    //        scope.$watch(attrs.name + '.$error', function(newVal, oldVal) {

    //            if (typeof newVal.required != 'undefined' && newVal.required != false) {
    //                    alert($('[name=' + newVal.required[0].$name + ']').data('msg'));
    //            }

    //            if (typeof newVal.maxlength != 'undefined' && newVal.maxlength != false) {
    //                    alert('You have exceed the max length specified.');
    //            }

    //            if (typeof newVal.minlength != 'undefined' && newVal.minlength != false) {
    //                    alert('You do not have enought characters in the input field');
    //            }


    //            //if (newVal.integer != true) {
    //            //    alert('The value must be an integer.');
    //            //}
    //        }, true);
    //    }
    //});
    //.directive('integer', function() {
    //    return {
    //        require: 'ngModel',
    //        link: function(scope, elm, attrs, ctrl) {
    //            ctrl.$validators.integer = function(modelValue, viewValue) {
    //                if (ctrl.$isEmpty(modelValue)) {
    //                    return true;
    //                }

    //                if (INTEGER_REGEXP.test(viewValue)) {
    //                    return true;
    //                }
    //                return false;
    //            };
    //        }
    //    };
    //});
})();