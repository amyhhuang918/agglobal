from agglobal.models import *
from django.contrib import admin


# Register your models here.
admin.site.register(Company)
admin.site.register(UserProfile)
admin.site.register(User_exp)
admin.site.register(InvestorProfile)
admin.site.register(Investment_experiences)
admin.site.register(User_rel)

