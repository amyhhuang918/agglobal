from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.db import models
from django.db.models.signals import post_save
import os

class OverwriteStorage(FileSystemStorage):
    def get_available_name(self, name):
        if self.exists(name):
            os.remove(os.path.join(settings.MEDIA_ROOT, name))
        return name
    
# Create your models here.

class UserProfile(models.Model):
    user_profile_id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=100,null=True, blank=True)
    last_name = models.CharField(max_length=100,null=True, blank=True)
    email = models.EmailField()
    company_name =  models.CharField(max_length=100,null=True, blank=True)
    domain =  models.CharField(max_length=100,null=True, blank=True)
    introduction = models.TextField(null=True, blank=True) 
    job_title = models.CharField(max_length=100,null=True, blank=True)
    personal_profile = models.TextField(null=True, blank=True) 
    city = models.CharField(max_length=100,null=True, blank=True)
    country = models.CharField(max_length=100,null=True, blank=True)
    industry = models.CharField(max_length=100,null=True, blank=True)
    industryofconcern = models.CharField(max_length=100,null=True, blank=True)
    profession = models.CharField(max_length=100,null=True, blank=True)
    profile_img = models.FileField(null=True, blank=True,upload_to='',storage=OverwriteStorage())
    activation_key = models.CharField(max_length=40)
    key_expires = models.DateTimeField()
    user = models.ForeignKey(User, db_column='userid')
    def __unicode__(self):
        return u'Profile: %s' % self.first_name

class Company(models.Model):
    company_id = models.AutoField(primary_key=True)
    project_name =  models.CharField(max_length=100,null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    location = models.CharField(max_length=100,null=True, blank=True)
    industry =  models.CharField(max_length=100,null=True, blank=True)
    invested_status =  models.CharField(max_length=100,null=True, blank=True)
    investment_budget =  models.CharField(max_length=100,null=True, blank=True)
    company_logo = models.FileField(null=True, blank=True,upload_to='',storage=OverwriteStorage())
    business_plan = models.FileField(null=True, blank=True,upload_to='',storage=OverwriteStorage())
    product_img1 = models.FileField(null=True, blank=True,upload_to='',storage=OverwriteStorage())
    product_img2 = models.FileField(null=True, blank=True,upload_to='',storage=OverwriteStorage())
    product_img3 = models.FileField(null=True, blank=True,upload_to='',storage=OverwriteStorage())
    product_img4 = models.FileField(null=True, blank=True,upload_to='',storage=OverwriteStorage())
    company_site = models.CharField(max_length=200,null=True, blank=True)
    introduction = models.TextField(null=True, blank=True)
    user= models.ForeignKey(User, db_column='userid')

class Businessplan_rating(models.Model):
    business_model = models.CharField(max_length=1,null=True, blank=True)
    expandibility = models.CharField(max_length=1,null=True, blank=True)
    innovativeness = models.CharField(max_length=1,null=True, blank=True)
    projectteam  = models.CharField(max_length=1,null=True, blank=True)
    usercomment = models.CharField(max_length=200,null=True, blank=True)
    company= models.ForeignKey(Company, db_column='company_id')
    user= models.ForeignKey(User, db_column='userid')

class Company_financing(models.Model):
    financing_title =  models.CharField(max_length=100,null=True, blank=True)
    start_year = models.CharField(max_length=50,null=True, blank=True)
    start_month = models.CharField(max_length=50,null=True, blank=True)
    end_year = models.CharField(max_length=50,null=True, blank=True)
    end_month = models.CharField(max_length=50,null=True, blank=True)
    introduction = models.TextField(null=True, blank=True)
    file_name =  models.CharField(max_length=100,null=True, blank=True) 
    company =  models.ForeignKey(Company)

class Company_investors(models.Model):
    name = models.CharField(max_length=100,null=True, blank=True)
    position = models.CharField(max_length=100,null=True, blank=True)
    introduction = models.TextField(null=True, blank=True)
    file_name =  models.CharField(max_length=100,null=True, blank=True)
    company =  models.ForeignKey(Company)

class Company_members(models.Model):
    name = models.CharField(max_length=100,null=True, blank=True)
    position = models.CharField(max_length=100,null=True, blank=True)
    introduction = models.TextField(null=True, blank=True)
    file_name =  models.CharField(max_length=100,null=True, blank=True)
    company =  models.ForeignKey(Company)


class User_company_rel(models.Model):
    user = models.ForeignKey(User,  db_column='userid')
    company = models.ForeignKey(Company)
    user_type = models.CharField(max_length=50,null=True, blank=True)
    like =  models.CharField(max_length=1,null=True, blank=True)
    follow =  models.CharField(max_length=1,null=True, blank=True)

class User_exp(models.Model):
    company_name = models.CharField(max_length=100,null=True, blank=True)
    job_title = models.CharField(max_length=100,null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    location = models.CharField(max_length=100,null=True, blank=True)
    start_year = models.CharField(max_length=50,null=True, blank=True)
    start_month = models.CharField(max_length=50,null=True, blank=True)
    end_year = models.CharField(max_length=50,null=True, blank=True)
    end_month = models.CharField(max_length=50,null=True, blank=True)
    attached_file = models.FileField(null=True, blank=True,upload_to='',storage=OverwriteStorage())
    user =  models.ForeignKey(UserProfile)

class InvestorProfile(models.Model):
    investor_id  = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=100,null=True, blank=True)
    last_name = models.CharField(max_length=100,null=True, blank=True)
    email = models.EmailField()
    country = models.CharField(max_length=100,null=True, blank=True)
    city = models.CharField(max_length=100,null=True, blank=True)
    industryofconcern = models.CharField(max_length=100,null=True, blank=True)
    company_name =  models.CharField(max_length=100,null=True, blank=True)
    job_title = models.CharField(max_length=100,null=True, blank=True)
    profession = models.CharField(max_length=100,null=True, blank=True)
    industry =  models.CharField(max_length=100,null=True, blank=True)
    introduction = models.TextField(null=True, blank=True)
    personal_profile = models.TextField(null=True, blank=True) 
    profile_img = models.FileField(null=True, blank=True,upload_to='',storage=OverwriteStorage())
    investment_field = models.CharField(max_length=100,null=True, blank=True)
    investment_location = models.CharField(max_length=100,null=True, blank=True)
    investment_budget  = models.CharField(max_length=100,null=True, blank=True)
    investment_angel_status =  models.CharField(max_length=100,null=True, blank=True, default="N")
    investment_pre_a_status =  models.CharField(max_length=100,null=True, blank=True, default="N")
    investment_a_status =  models.CharField(max_length=100,null=True, blank=True, default="N")
    investment_b_status =  models.CharField(max_length=100,null=True, blank=True, default="N")
    investment_b_after_status =  models.CharField(max_length=100,null=True, blank=True, default="N")
    activation_key = models.CharField(max_length=40)
    key_expires = models.DateTimeField()
    user = models.ForeignKey(User, db_column='userid')
    certified = models.CharField(max_length=1, null=True, blank=True)

class Investment_experiences(models.Model):
    project_name = models.CharField(max_length=100,null=True, blank=True)
    invest_year = models.CharField(max_length=50,null=True, blank=True)
    invest_month = models.CharField(max_length=50,null=True, blank=True)
    invested_status  = models.CharField(max_length=100,null=True, blank=True)
    project_detail  = models.TextField(null=True, blank=True)
    attached_project_file  = models.FileField(null=True, blank=True,upload_to='',storage=OverwriteStorage())
    investor =  models.ForeignKey(InvestorProfile)

class User_investor_ref(models.Model):
    user = models.ForeignKey(User,  db_column='userid')
    investor = models.ForeignKey(InvestorProfile)

class User_rel(models.Model):
    from_user = models.ForeignKey(User,  related_name = 'user_rel_from_user')
    to_user = models.ForeignKey(User, related_name = 'user_rel_to_user')
    from_user_type = models.CharField(max_length=50,null=True, blank=True)
    to_user_type = models.CharField(max_length=50,null=True, blank=True)
    like =  models.CharField(max_length=1,null=True, blank=True)
    follow =  models.CharField(max_length=1,null=True, blank=True)


class Investment_plan(models.Model):
    investment_field = models.CharField(max_length=100,null=True, blank=True)
    investment_location = models.CharField(max_length=100,null=True, blank=True)
    investment_budget  = models.CharField(max_length=100,null=True, blank=True)
    investment_status  = models.CharField(max_length=100,null=True, blank=True)
    investor =  models.ForeignKey(InvestorProfile)
 

class Invested_project(models.Model):
    project_name = models.CharField(max_length=100,null=True, blank=True)
    invest_date = models.DateField(null=True, blank=True)
    invest_status =  models.CharField(max_length=100,null=True, blank=True)
    project_detail  = models.TextField(null=True, blank=True) 
    attached_project_file  = models.FileField(null=True, blank=True,upload_to='',storage=OverwriteStorage())
    investor =  models.ForeignKey(InvestorProfile)


class Document(models.Model):
    docfile = models.FileField(upload_to='',storage=OverwriteStorage())
    email = models.EmailField(null=True, blank=True)
    filename = models.CharField(max_length=100,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    created = models.DateTimeField(null=True, blank=True)

class Message(models.Model):
    subject =  models.CharField(max_length=500,null=True, blank=True)
    body = models.TextField(null=True, blank=True)
    sender = models.ForeignKey(User, db_column='sender',  related_name = 'message_sender')
    receiver = models.ForeignKey(User, db_column='receiver', related_name = 'message_reciever')
    read =  models.CharField(max_length=1,null=True, blank=True)
    deleted = models.CharField(max_length=1,null=True, blank=True)
    senttime = models.DateTimeField()
    parentid = models.IntegerField(null=True, blank=True, db_index=True)




##### gets called after user has been created ####
"""
def create_user_profile(sender, instance, created, **kwargs):
    ###Get the temproray profile creating in the form class, then linked it to the user instance
    print "in create_user_profile"
    profile, created = User.objects.get_or_create(email=instance.email)
    profile.first_name=instance.first_name
    profile.last_name=instance.last_name
    profile.save()
post_save.connect(create_user_profile, sender=User)

def social_auth_to_profile(backend, details, response, user=None, is_new=False, *args, **kwargs):
    print "in social_auth_to_profile"
    if is_new:
        profile = UserProfile.objects.get_or_create(user=user)
    else:
        profile = UserProfile.objects.get(user=user)
    # Some of the default user details given in the pipeline
    profile.email = details['email']
    profile.name = details['fullname']
    # Now we also need the extra details, found in the `social_user` kwarg
    social_user = kwargs['social_user']
    profile.company_name = social_user.extra_data['headline']
    #profile.job_title = social_user.extra_data['positions']['position'][0]['title']
    profile.save()
"""
